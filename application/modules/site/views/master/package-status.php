<form id="form-package" action="<?=current_url()?>">
  <div class="form-group">
    <select class="form-control" name="<?=COL_PKGISACTIVE?>" style="width: 100%">
      <option value="1" <?=$rpackage[COL_PKGISACTIVE]==1?'checked':''?>>AKTIF</option>
      <option value="0" <?=$rpackage[COL_PKGISACTIVE]==0?'checked':''?>>INAKTIF</option>
    </select>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  var form = $('#form-package');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $(".uang", form).number(true, 0, '.', ',');
  form.validate({
    submitHandler: function(form) {
      var modal = $(form).closest('.modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit[0].innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
