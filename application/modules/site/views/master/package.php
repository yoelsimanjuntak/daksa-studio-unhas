<?php
$filterStatus = "";
if(!empty($_GET['filterStatus'])) $filterStatus = $_GET['filterStatus'];
else $filterStatus=1;
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6">
        <form class="bg-white" action="<?=current_url()?>" method="get">
          <div class="input-group ">
            <select class="form-control" name="filterStatus">\
              <option value="1" <?=$filterStatus==1?'selected':''?>>AKTIF</option>
              <option value="-1" <?=$filterStatus==-1?'selected':''?>>INAKTIF</option>
            </select>
            <span class="input-group-append">
              <button type="submit" class="btn btn-outline-success"><i class="far fa-filter"></i> FILTER</button>
              <a href="<?=site_url('site/master/package-add')?>" class="btn btn-outline-primary btn-popup-form" data-title="Tambah"><i class="far fa-plus-circle"></i> TAMBAH</a>
            </span>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row align-items-stretch">
      <?php
      if(!empty($data)) {
        foreach($data as $dat) {
          $classStatus = 'success';
          if($dat[COL_PKGISACTIVE]!=1) $classStatus = 'danger';

          $arrItems = array();
          if(!empty($dat[COL_PKGITEMS])) {
            $arrItems = json_decode($dat[COL_PKGITEMS]);
          }

          $rsess = $this->db
          ->where(COL_IDPACKAGE, $dat[COL_UNIQ])
          ->get(TBL_TSESSION)
          ->result_array();
          ?>
          <div class="col-12 col-sm-6 d-flex align-items-stretch">
            <div class="card w-100 card-<?=$classStatus?>">
              <div class="card-header">
                <h3 class="card-title font-weight-bold"><?=$dat[COL_PKGNAME]?></h3>
                <div class="card-tools">
                  <a href="<?=site_url('site/master/package-edit/'.$dat[COL_UNIQ])?>" class="btn-tool btn-popup-form " data-title="Perbarui Data"><i class="far fa-cog"></i> UBAH</a>
                  <a href="<?=site_url('site/master/package-status/'.$dat[COL_UNIQ])?>" class="btn-tool btn-popup-form-sm " data-title="Status"><i class="far fa-sliders-h"></i> STATUS</a>
                  <!--<a href="<?=site_url('site/sess/index-by-package/'.$dat[COL_UNIQ])?>" class="btn-tool " data-title="Peserta Ujian"><i class="far fa-user"></i> PESERTA</a>-->
                  <!--<a href="<?=site_url('site/master/package-formula/'.$dat[COL_UNIQ])?>" class="btn-tool text-primary btn-popup-form " data-title="Formula"><i class="far fa-badge-check"></i> PENILAIAN</a>-->
                </div>
              </div>
              <div class="card-body p-0">
                <ul class="nav flex-column">
                  <?php if(!empty($dat[COL_PKGDESC])) {
                    ?>
                    <li class="nav-item">
                      <span class="nav-link" style="padding: .25rem 1.5rem !important">
                        <span class="text-sm font-italic"><?=$dat[COL_PKGDESC]?></span>
                      </span>
                    </li>
                    <?php
                  }
                  ?>
                  <!--<li class="nav-item text-right">
                    <span class="nav-link" style="padding: .25rem 1.5rem !important">
                      <span class="float-left">Kategori</span> <strong><?=!empty($dat[COL_KATEGORI])?$dat[COL_KATEGORI]:'-'?></strong>
                    </span>
                  </li>-->
                  <li class="nav-item text-right">
                    <span class="nav-link" style="padding: .25rem 1.5rem !important">
                      <span class="float-left">Tanggal</span> <strong><?=date('d-m-Y', strtotime($dat[COL_PKGDATE]))?></strong>
                    </span>
                  </li>
                  <li class="nav-item text-right">
                    <span class="nav-link" style="padding: .25rem 1.5rem !important">
                      <span class="float-left">Waktu</span> <strong><?=$dat[COL_PKGAVAILABLEFROM]?></strong> s.d <strong><?=$dat[COL_PKGAVAILABLETO]?></strong>
                    </span>
                  </li>
                  <li class="nav-item text-right pb-2">
                    <span class="nav-link" style="padding: .25rem 1.5rem !important">
                      <span class="float-left">Peserta</span> <span class="badge bg-success"><?=number_format(count($rsess))?></span>
                    </span>
                  </li>
                </ul>
              </div>
              <div class="card-footer p-0 bg-white">
                <table class="table table-striped mb-0" width="100%">
                  <tbody>
                    <!--<tr>
                      <td class="nowrap" style="width: 10px">STATUS</td>
                      <td class="nowrap" style="width: 10px">:</td>
                      <td colspan="2">
                        <span class="badge badge-<?=$dat[COL_PKGISACTIVE]==1?'success':'danger'?>"><?=$dat[COL_PKGISACTIVE]==1?'AKTIF':'INAKTIF'?></span>
                      </td>
                    </tr>
                    <tr>
                      <td class="nowrap" style="width: 10px">HARGA</td>
                      <td class="nowrap" style="width: 10px">:</td>
                      <td colspan="2">Rp. <?=number_format($dat[COL_PKGPRICE])?></td>
                    </tr>-->
                    <?php
                    foreach($arrItems as $i) {
                      $rtest = $this->db
                      ->where(COL_UNIQ, $i->TestID)
                      ->get(TBL_MTEST)
                      ->row_array();
                      ?>
                      <tr>
                        <!--<td class="nowrap text-right" style="width: 10px"><?=$i->Seq?>.</td>-->
                        <td class="pl-4" colspan="2" style="vertical-align: middle">
                          <strong><?=!empty($rtest)?strtoupper($rtest[COL_TESTNAME]):strtoupper($i->TestName)?></strong> <small class="font-italic">(<?=number_format($rtest[COL_TESTQUESTNUM]).' Soal = '.number_format($rtest[COL_TESTDURATION]).' Menit'?>)</small>
                        </td>
                        <!--<td class="pr-4 nowrap text-center" style="width: 10px">

                        </td>-->
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td>
                        <a href="<?=site_url('site/sess/index-by-package/'.$dat[COL_UNIQ])?>" class="btn btn-sm btn-outline-primary" data-title="Daftar Peserta"><i class="far fa-users"></i> DAFTAR PESERTA</a>
                        <a href="<?=site_url('site/sess/report/'.$dat[COL_UNIQ])?>" class="btn btn-sm btn-outline-success" data-title="Laporan Hasil Ujian"><i class="far fa-chart-bar"></i> LIHAT HASIL</a>
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
          <?php
        }
      } else {
        ?>
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <p class="text-center mb-0 font-italic">
                BELUM ADA DATA TERSEDIA
              </p>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay d-none justify-content-center align-items-center">
        <i class="fas fa-2x fa-spinner fa-spin"></i>
      </div>
        <div class="modal-header">
          <h5 class="modal-title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-form-sm" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="overlay d-none justify-content-center align-items-center">
        <i class="fas fa-2x fa-spinner fa-spin"></i>
      </div>
        <div class="modal-header">
          <h5 class="modal-title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalForm = $('#modal-form');
var modalFormSm = $('#modal-form-sm');
$(document).ready(function() {
  modalForm.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalForm).empty();
    $('.modal-title', modalForm).html('');
  });

  $('.btn-popup-form').click(function() {
    var url = $(this).attr('href');
    var title = $(this).data('title');
    var optFooter = $(this).data('opt-footer');
    if(url) {
      if(title) {
        $('.modal-title', modalForm).html(title);
      }

      if(optFooter=='0') {
        $('.modal-footer', modalForm).removeClass('d-block').addClass('d-none');
      } else {
        $('.modal-footer', modalForm).removeClass('d-none').addClass('d-block');
      }

      modalForm.modal('show');
      $('.modal-body', modalForm).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
      $('.modal-body', modalForm).load(url, function(){
        $('.datepicker', modalForm).daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          maxYear: parseInt(moment().add(10, 'year').format('YYYY'),10),
          locale: {
              format: 'Y-MM-DD'
          }
        });
        $('.time-mask', modalForm).inputmask({
          alias: "datetime",
          inputFormat: "HH:MM"
        });
        $('button[type=submit]', modalForm).unbind('click').click(function(){
          $('form', modalForm).submit();
        });
      });
    }

    return false;
  });

  $('.btn-popup-form-sm').click(function() {
    var url = $(this).attr('href');
    var title = $(this).data('title');
    if(url) {
      if(title) {
        $('.modal-title', modalFormSm).html(title);
      }

      modalFormSm.modal('show');
      $('.modal-body', modalFormSm).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
      $('.modal-body', modalFormSm).load(url, function(){
        $('button[type=submit]', modalFormSm).unbind('click').click(function(){
          $('form', modalFormSm).submit();
        });
      });
    }

    return false;
  });
});
</script>
