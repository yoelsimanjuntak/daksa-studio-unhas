<form id="form-package" action="<?=current_url()?>">
  <div class="form-group">
    <div class="row">
      <div class="col-sm-7">
        <label>Nama Ujian</label>

        <!--<div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <input type="checkbox" name="<?=COL_PKGISACTIVE?>" value="1" <?=empty($data)?'checked':($data[COL_PKGISACTIVE]==1?'checked':'')?>>
            </span>
          </div>
          <input type="text" class="form-control" name="<?=COL_PKGNAME?>" placeholder="Nama Paket" value="<?=!empty($data)?$data[COL_PKGNAME]:''?>" required />
        </div>-->
        <input type="text" class="form-control" name="<?=COL_PKGNAME?>" placeholder="Nama Ujian" value="<?=!empty($data)?$data[COL_PKGNAME]:''?>" required />
      </div>
      <div class="col-sm-5">
        <div class="form-group">
          <label>Kategori</label>
          <select class="form-control" name="<?=COL_IDKATEGORI?>" style="width: 100%">
            <?=GetCombobox("select * from mkategori order by Kategori", COL_UNIQ, COL_KATEGORI, (!empty($data)?$data[COL_IDKATEGORI]:null))?>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-3">
        <label>Tanggal</label>
        <input type="text" class="form-control datepicker" name="<?=COL_PKGDATE?>" value="<?=!empty($data)?$data[COL_PKGDATE]:''?>" required />
      </div>
      <div class="col-sm-8">
        <div class="form-group">
          <label>Waktu</label>
          <div class="row">
            <div class="col-sm-3">
              <input type="text" class="form-control time-mask" name="<?=COL_PKGAVAILABLEFROM?>" value="<?=!empty($data)?$data[COL_PKGAVAILABLEFROM]:''?>" required />
            </div>
            <div class="col-sm-3">
              <input type="text" class="form-control time-mask" name="<?=COL_PKGAVAILABLETO?>" value="<?=!empty($data)?$data[COL_PKGAVAILABLETO]:''?>" required />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--<div class="form-group">
    <div class="row">
      <div class="col-sm-4">
        <label>Harga</label>
        <input type="text" class="form-control uang text-right" name="<?=COL_PKGPRICE?>" placeholder="$$,$$$" value="<?=!empty($data)?$data[COL_PKGPRICE]:''?>" required />
      </div>
    </div>
  </div>-->
  <div class="form-group">
    <label>Deskripsi</label>
    <textarea class="form-control" name="<?=COL_PKGDESC?>" placeholder="Deskripsi"><?=!empty($data)?$data[COL_PKGDESC]:''?></textarea>
  </div>
  <!--<div class="form-group">
    <label>Paket Soal</label>
    <select class="form-control" name="<?=COL_PKGITEMS?>[]" style="width: 100%" multiple>
      <?=GetCombobox("select * from mtest order by TestSeq", COL_UNIQ, COL_TESTNAME, $arrItems)?>
    </select>
  </div>-->
  <?php
if(/*!empty($data) && $data[COL_PKGISITEMEDITABLE]==1*/ true) {
    ?>
    <div class="form-group">
      <input type="hidden" name="<?=COL_PKGITEMS?>" />
      <table id="tbl-det" class="table table-bordered table-condensed">
        <thead>
          <tr>
            <th class="text-sm" style="width: 75px !important; white-space: nowrap">NO.</th>
            <th class="text-sm">MODEL SOAL</th>
            <th class="text-center" style="width: 10px !important">#</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
          <tr>
            <td><input type="text" name="det-no" class="form-control" /></td>
            <td>
              <select class="form-control" name="det-item" style="width: 100%">
                <?=GetCombobox("select * from mtest", COL_UNIQ, COL_TESTNAME)?>
              </select>
            </td>
            <td style="vertical-align: middle !important">
              <button type="button" id="btn-det-add" class="btn btn-outline-primary btn-sm"><i class="far fa-plus-circle"></i></button>
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
    <?php
  }
  ?>

</form>
<script type="text/javascript">
$(document).ready(function(){
  var form = $('#form-package');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $(".uang", form).number(true, 0, '.', ',');
  form.validate({
    submitHandler: function(form) {
      var modal = $(form).closest('.modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  $('[name=PkgItems]', form).change(function(){
    var det = $('[name=PkgItems]', form).val();
    var detArr = [];

    if(det) {
      detArr = JSON.parse(det);
    }

    if(detArr.length>0) {
      var detHtml = '';
      for(var i=0; i<detArr.length; i++) {
        detHtml += '<tr data-idx="'+detArr[i].TestID+'"><td>'+detArr[i].Seq+'</td><td>'+detArr[i].TestName+'</td><td><button type="button" class="btn btn-outline-danger btn-sm btn-det-del"><i class="far fa-times-circle"></i></button></td></tr>';
      }

      $('tbody', $('#tbl-det')).html(detHtml);
      $('.btn-det-del', $('#tbl-det')).unbind('click').click(function(){
        var idx = $(this).closest('tr').data('idx');
        var det = $('[name=PkgItems]', form).val();
        var detArr = [];

        if(det) {
          detArr = JSON.parse(det);
        }

        if(idx) {
          detArr = $.grep(detArr, function(n,i) {
            return n.TestID.toString() !== idx.toString();
          });

          detArr.sort(function(a, b) {
            return a.Seq-b.Seq;
          });
          $('[name=PkgItems]', form).val(JSON.stringify(detArr)).trigger('change');
        }
      });
    } else {
      $('tbody', $('#tbl-det')).html('<tr><td colspan="3" class="text-center"><small class="font-italic">(KOSONG)</small></td></tr>');
    }
  }).trigger('change');

  <?php
  if(!empty($data) && !empty($data[COL_PKGITEMS])) {
    echo @"$('[name=PkgItems]', form).val('".$data[COL_PKGITEMS]."').trigger('change');";
  }
  ?>

  $('#btn-det-add', form).click(function(){
    var no_=$('[name=det-no]', $('#tbl-det')).val();
    var item_=$('[name=det-item]', $('#tbl-det')).val();
    var itemname_=$('[name=det-item] option:selected', $('#tbl-det')).text();
    var det = $('[name=PkgItems]', form).val();
    var detArr = [];

    if(det) {
      detArr = JSON.parse(det);
    }

    if(item_) {
      detArr.push({Seq: no_, TestID: item_, TestName:itemname_});
      detArr.sort(function(a, b) {
        return a.Seq-b.Seq;
      });
      $('[name=PkgItems]', form).val(JSON.stringify(detArr)).trigger('change');
      $('[name=det-no]', $('#tbl-det')).val('');
    }
  });
});
</script>
