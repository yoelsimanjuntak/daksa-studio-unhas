<form id="form-formula" action="<?=current_url()?>" method="post">
  <div class="row mb-2">
    <div class="col-sm-3">
      <div class="form-group">
        <label>FAKTOR</label>
        <select class="form-control" name="<?=COL_SCOREFORMULA?>" style="width: 100%">
          <option value="SCORE">SKOR AKHIR</option>
          <option value="AVG">SKOR RATA-RATA</option>
          <option value="TRUE">JLH. BENAR</option>
          <option value="FALSE">JLH. SALAH</option>
        </select>
      </div>
    </div>
    <div class="col-sm-2">
      <label>MULAI</label>
      <input type="text" name="ValueFrom" class="form-control text-right uang" value="0" required />
    </div>
    <div class="col-sm-2">
      <label>HINGGA</label>
      <input type="text" name="ValueTo" class="form-control text-right uang" value="0" required />
    </div>
    <div class="col-sm-2">
      <label>POIN / NILAI</label>
      <input type="text" name="ValueFinal" class="form-control text-right uang" value="0" required />
    </div>
    <div class="col-sm-3 pt-3">
      <button type="button" id="btn-add-item" class="btn btn-primary btn-block mt-3"><i class="far fa-plus-circle"></i>&nbsp;&nbsp;TAMBAH</button>
    </div>
  </div>
</form>
<?php
if(!empty($data)) {
  ?>
  <table id="tbl-formula" class="table table-bordered table-sm text-sm">
    <tbody>
      <tr>
        <th>FAKTOR</th>
        <th>MULAI</th>
        <th>HINGGA</th>
        <th>POIN / NILAI</th>
        <th>AKSI</th>
      </tr>
      <?php
      foreach ($data as $f) {
        $arrScore = array();
        $desc = "";
        if ($f[COL_SCOREREF]=="SCORE") $desc = "Skor Akhir";
        else if ($f[COL_SCOREREF]=="AVG") $desc = "Skor Rata-Rata";
        else if ($f[COL_SCOREREF]=="TRUE") $desc = "Jlh. Benar";
        else if ($f[COL_SCOREREF]=="FALSE") $desc = "Jlh. Salah";

        if(!empty($f[COL_SCOREFORMULA])) {
          $arrScore = json_decode($f[COL_SCOREFORMULA]);
        }
        for($i=0;$i<count($arrScore); $i++) {
          $s = $arrScore[$i];
          ?>
          <tr style="vertical-align: middle">
            <?php
            if($i==0) {
              ?>
              <td <?=count($arrScore)>1?'rowspan="'.count($arrScore).'"':''?> style="vertical-align:middle">
                <?=$f[COL_SCOREREF]?><br />
                <small class="font-italic font-weight-bold"><?=$desc?></small>
              </td>
              <?php
            }
            ?>
            <td class="text-right nowrap" style="width: 100px;"><?=$s->ValueFrom?></td>
            <td class="text-right nowrap" style="width: 100px;"><?=$s->ValueTo?></td>
            <td class="text-right nowrap" style="width: 100px;"><?=$s->Point?></td>
            <td class="text-center nowrap" style="width: 10px;">
              <!--<a href="<?=site_url('site/master/package-formula-edit/'.$f[COL_UNIQ].'/'.$i)?>" class="btn btn-outline-primary btn-xs btn-edit-item" data-from="<?=$s->ValueFrom?>" data-to="<?=$s->ValueTo?>" data-point="<?=$s->Point?>"><i class="far fa-edit"></i>&nbsp;UBAH</a>-->
              <a href="<?=site_url('site/master/package-formula-delete/'.$f[COL_UNIQ].'/'.$i)?>" class="btn btn-outline-danger btn-xs btn-remove-item"><i class="far fa-times-circle"></i>&nbsp;HAPUS</a>
            </td>
            <?php
            if($i==0) {
              ?>
              <!--<td <?=count($arrScore)>1?'rowspan="'.count($arrScore).'"':''?>  class="text-center nowrap" style="width: 10px; vertical-align:middle">
                <a href="<?=site_url('site/master/package-formula-delete/'.$f[COL_UNIQ])?>" class="btn btn-outline-danger btn-xs btn-remove-item"><i class="far fa-times-circle"></i>&nbsp;HAPUS</a>
              </td>-->
              <?php
            }
            ?>
          </tr>
          <?php
        }
      }
      ?>
    </tbody>
  </table>
  <?php
} else {
  echo '<p class="font-italic">Belum ada penilaian yang diatur.</p>';
}
?>
<script type="text/javascript">
$(document).ready(function(){
  var form = $('#form-formula');
  var modal = form.closest('.modal');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $(".uang", form).number(true, 0, '.', ',');
  $("#btn-add-item", form).click(function(){
    form.submit();
  });

  $(".btn-remove-item", modal).click(function(){
    if(confirm('Apakah anda yakin ingin menghapus?')) {
      var url = $(this).attr('href');
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
        }
      }, "json").done(function() {
        $('.overlay', modal).removeClass('d-none').addClass('d-flex');
        $(form).closest('.modal-body').load($(form).attr('action'), function(){
          $('.overlay', modal).removeClass('d-flex').addClass('d-none');
        });
      }).fail(function() {
        toastr.error('SERVER ERROR');
      });
    }
    return false;
  });

  form.validate({
    submitHandler: function(form) {
      var modal = $(form).closest('.modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
          }
          $('.overlay', modal).removeClass('d-none').addClass('d-flex');
          $(form).closest('.modal-body').load($(form).attr('action'), function(){
            $('.overlay', modal).removeClass('d-flex').addClass('d-none');
          });
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
