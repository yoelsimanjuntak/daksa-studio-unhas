<?php
$arrItems = array();
if(!empty($data[COL_PKGITEMS])) {
  $arrItems=explode(",", $data[COL_PKGITEMS]);
}
$arrsym = array_diff(scandir(MY_IMAGEPATH.'symbols/'), array('.', '..'));
?>
<form id="form-package" action="<?=current_url()?>">
  <div class="form-group">
    <label>Nama Model / Paket Soal</label>
    <input type="text" class="form-control" name="<?=COL_TESTNAME?>" value="<?=!empty($data)?$data[COL_TESTNAME]:''?>" required />
  </div>
  <div class="form-group">
    <label>Instruksi</label>
    <textarea class="form-control" name="<?=COL_TESTINSTRUCTION?>"><?=!empty($data)?$data[COL_TESTINSTRUCTION]:''?></textarea>
  </div>
  <div class="form-group">
    <label>Pertanyaan <small>(DEFAULT)</small></label>
    <textarea class="form-control" name="<?=COL_TESTDEFAULT?>"><?=!empty($data)?$data[COL_TESTDEFAULT]:''?></textarea>
    <p class="text-muted text-sm font-italic">NB: Teks pertanyaan default diatas jika diisi akan muncul secara otomatis setiap kali menambah soal baru.</p>
  </div>
  <!--<div class="form-group">
    <label>Kelompok Soal <small>(OPSIONAL)</small></label>
    <select class="form-control no-select2 with-tags" name="<?=COL_TESTGROUP?>[]" style="width: 100% !important" multiple></select>
    <p class="text-muted text-sm font-italic">NB: Kelompok soal dapat digunakan untuk mengelompokkan soal berdasarkan jenis / kategori (misal: TWK, TIU, TKP).</p>
  </div>-->
  <div class="form-group row">
    <div class="col-sm-4">
      <label>Tipe</label>
      <select name="<?=COL_TESTTYPE?>" class="form-control" style="width: 100% !important" required>
        <option value="MUL" <?=!empty($data)&&$data[COL_TESTTYPE]=='MUL'?'selected':''?>>UMUM</option>
        <option value="ACR" <?=!empty($data)&&$data[COL_TESTTYPE]=='ACR'?'selected':''?>>KECERMATAN</option>
      </select>
    </div>
    <div class="col-sm-4">
      <label>Jlh. Soal</label>
      <input type="text" class="form-control uang text-right" name="<?=COL_TESTQUESTNUM?>" placeholder="0" value="<?=!empty($data)?$data[COL_TESTQUESTNUM]:''?>" required />
    </div>
    <div class="col-sm-4">
      <label>Durasi <small class="text-muted">(MENIT)</small></label>
      <input type="text" class="form-control uang text-right" name="<?=COL_TESTDURATION?>" placeholder="0" value="<?=!empty($data)?$data[COL_TESTDURATION]:''?>" required />
    </div>
  </div>
  <br />
  <div class="form-group div-opt">
    <input type="hidden" name="<?=COL_TESTOPTION?>" />
    <table id="tbl-option" class="table table-bordered table-condensed">
      <thead>
        <tr>
          <th class="text-sm" style="width: 100px !important; white-space: nowrap">OPSI</th>
          <th class="text-sm">TEKS (DEFAULT)</th>
          <th style="width: 10px !important">#</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
      <tfoot>
        <tr>
          <td><input type="text" name="det-opt" class="form-control form-control-sm" /></td>
          <td><input type="text" name="det-def" class="form-control form-control-sm" /></td>
          <td class="text-center">
            <button type="button" id="btn-option-edit" data-idx="" class="btn btn-xs btn-outline-success d-none"><i class="far fa-check-circle"></i></button>
            <button type="button" id="btn-option-edit-cancel" class="btn btn-xs btn-outline-secondary d-none"><i class="far fa-refresh"></i></button>
            <button type="button" id="btn-option-add" class="btn btn-xs btn-outline-primary btn-block"><i class="far fa-plus-circle"></i></button>
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
  <div class="form-group div-symbol">
    <h6 class="font-weight-bold">Simbol</h6>
    <select name="<?=COL_TESTSYMBOL?>[]" class="form-control no-select2" style="width: 100% !important" multiple="multiple" required>
      <?php
      foreach ($arrsym as $s) {
        $a = getimagesize(MY_IMAGEPATH.'symbols/'.$s);
        $image_type = $a[2];
        if (!in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
        {
          continue;
        }
        ?>
        <option value="<?=$s?>" <?=!empty($data)&&in_array($s, explode(",", $data[COL_TESTSYMBOL]))?'selected':''?>><?=strtolower($s)?></option>
        <?php
      }
      ?>
    </select>
    <!--<textarea id="symbol" rows="2" name="<?=COL_TESTSYMBOL?>"><?=!empty($data)?$data[COL_TESTSYMBOL]:''?></textarea>-->
  </div>
</form>
<script type="text/javascript">
/*CKEDITOR.config.toolbar = [['Paste','SpecialChar']];
CKEDITOR.config.height = 150;
CKEDITOR.replace('symbol');*/
$(document).ready(function(){
  var form = $('#form-package');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  /*$("select.with-tags").select2({
    width: 'resolve',
    theme: 'bootstrap4',
    tags: true
  });*/
  $("select[name^=TestSymbol]").select2({
    width: 'resolve',
    theme: 'bootstrap4',
    templateResult: function(state){
      if (!state.id) {
        return state.text;
      }
      var baseUrl = "<?=MY_IMAGEURL.'symbols/'?>";
      var $state = $(
        '<span><img src="'+baseUrl+'/'+state.element.value.toLowerCase()+'" style="height: 20px !important; width: 20px !important; margin-right: 10px" />'+state.text+'</span>'
      );
      return $state;
    }
  });
  $(".uang", form).number(true, 0, '.', ',');
  form.validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      for (instance in CKEDITOR.instances ) {
        CKEDITOR.instances[instance].updateElement();
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  $('[name=TestOption]', form).change(function(){
    var opt = $('[name=TestOption]', form).val();
    var optArr = [];

    if(opt) {
      optArr = JSON.parse(opt);
    }

    if(optArr.length>0) {
      var optHtml = '';
      for(var i=0; i<optArr.length; i++) {
        optHtml += '<tr data-idx="'+optArr[i].Opt+'"><td>'+optArr[i].Opt+'</td><td>'+optArr[i].Def+'</td><td style="white-space: nowrap"><button type="button" class="btn btn-xs btn-outline-success btn-option-edit"><i class="far fa-edit"></i></button>&nbsp;<button type="button" class="btn btn-xs btn-outline-danger btn-option-del"><i class="far fa-times-circle"></i></button></td></tr>';
      }

      $('tbody', $('#tbl-option')).html(optHtml);
      $('.btn-option-del', $('#tbl-option')).unbind('click').click(function(){
        var idx = $(this).closest('tr').data('idx');
        var opt = $('[name=TestOption]', form).val();
        var optArr = [];

        if(opt) {
          optArr = JSON.parse(opt);
        }

        if(idx) {
          optArr = $.grep(optArr, function(n,i) {
            return n.Opt !== idx;
          });

          $('[name=TestOption]', form).val(JSON.stringify(optArr)).trigger('change');
        }
      });

      $('.btn-option-edit', $('#tbl-option')).unbind('click').click(function(){
        var idx = $(this).closest('tr').data('idx');
        var opt = $('[name=TestOption]', form).val();
        var optArr = [];

        if(opt) {
          optArr = JSON.parse(opt);
        }

        if(idx) {
          var optEdited = $.grep(optArr, function(n,i) {
            return n.Opt == idx;
          });

          if(optEdited) {
            $('[name=det-opt]', $('#tbl-option')).val(optEdited[0].Opt);
            $('[name=det-def]', $('#tbl-option')).val(optEdited[0].Def);
            $('#btn-option-edit').removeClass('d-none').data('idx', optEdited[0].Opt);
            $('#btn-option-edit-cancel').removeClass('d-none');
            $('#btn-option-add').addClass('d-none');
          }
        }
      });
    } else {
      $('tbody', $('#tbl-option')).html('<tr><td colspan="3" class="text-center"><small class="font-italic">(KOSONG)</small></td></tr>');
    }
  }).trigger('change');

  <?php
  if(!empty($data) && !empty($data[COL_TESTOPTION])) {
    echo @"$('[name=TestOption]', form).val('".$data[COL_TESTOPTION]."').trigger('change');";
  }
  ?>

  $('[name=TestType]', form).change(function(){
    if($(this).val()=='ACR') {
      $('[name=TestOption]', form).attr('disabled', true);
      $('.div-opt').hide();

      $('[name=TestSymbol]', form).attr('disabled', false);
      $('.div-symbol').show();
    } else {
      $('[name=TestOption]', form).attr('disabled', false);
      $('.div-opt').show();

      $('[name=TestSymbol]', form).attr('disabled', true);
      $('.div-symbol').hide();
    }
  }).trigger('change');

  $('#btn-option-add', form).click(function(){
    var opt_=$('[name=det-opt]', $('#tbl-option')).val();
    var def_=$('[name=det-def]', $('#tbl-option')).val();
    var opt = $('[name=TestOption]', form).val();
    var optArr = [];

    if(opt) {
      optArr = JSON.parse(opt);
    }

    if(opt_) {
      var optExisted = $.grep(optArr, function(n,i) {
        return n.Opt == opt_;
      });
      if(optExisted.length>0) {
        alert('Opsi sudah ada!');
        return false;
      }

      optArr.push({Opt: opt_, Def: def_});
      $('[name=TestOption]', form).val(JSON.stringify(optArr)).trigger('change');
      $('[name=det-opt]', $('#tbl-option')).val('');
      $('[name=det-def]', $('#tbl-option')).val('');
    }
  });

  $('#btn-option-edit-cancel', form).click(function(){
    $('[name=det-opt]', $('#tbl-option')).val('');
    $('[name=det-def]', $('#tbl-option')).val('');

    $('#btn-option-edit').addClass('d-none').data('idx', '');
    $('#btn-option-edit-cancel').addClass('d-none');
    $('#btn-option-add').removeClass('d-none');
  });

  $('#btn-option-edit', form).click(function(){
    var idx = $(this).data('idx');
    var opt_=$('[name=det-opt]', $('#tbl-option')).val();
    var def_=$('[name=det-def]', $('#tbl-option')).val();
    var opt = $('[name=TestOption]', form).val();
    var optArr = [];

    if(opt) {
      optArr = JSON.parse(opt);
    }

    if(opt_) {
      optArr = optArr.map(function(n){
        return (n.Opt==idx?{Opt: opt_, Def: def_}:n);
      });

      $('[name=TestOption]', form).val(JSON.stringify(optArr)).trigger('change');
      $('[name=det-opt]', $('#tbl-option')).val('');
      $('[name=det-def]', $('#tbl-option')).val('');
    }
  });
});
</script>
