<?php
$numStatToday = $this->db->where('DATE_FORMAT(Timestamp, "%Y-%m-%d")=', date('Y-m-d'))->count_all_results(TBL_WEBLOGS);
$numStatMonthly = $this->db->where('DATE_FORMAT(Timestamp, "%Y-%m")=', date('Y-m'))->count_all_results(TBL_WEBLOGS);
$numStatTotal = $this->db->count_all_results(TBL_WEBLOGS);

$rwelcome = $this->db
->where(COL_CONTENTTYPE,'WelcomeText')
->get(TBL_WEBCONTENT)
->row_array();

$rtestimoni = $this->db
->where(COL_CONTENTTYPE,'Testimonial')
->order_by(COL_UNIQ, 'desc')
->limit(12)
->get(TBL_WEBCONTENT)
->result_array();

$qpkg = @"
select * from (
  select kat.Uniq, IFNULL(kat.Kategori,'LAINNYA') as Kategori, IFNULL(kat.Kategori,'Z') as Ordering, count(pkg.Uniq) as JlhPkg from mtestpackage pkg
  left join mkategori kat on kat.Uniq = pkg.IdKategori
  where pkg.PkgIsActive=1
  group by kat.Kategori
) tbl order by JlhPkg desc, Ordering asc
";
$rpkg = $this->db->query($qpkg)->result_array();

$rheader = $this->db
->where(COL_ISHEADER, 1)
->where(COL_POSTID, $data[COL_POSTID])
->get(TBL__POSTIMAGES)
->result_array();

$postContent = $data[COL_POSTCONTENT];
$rimg = $this->db
->where(COL_ISHEADER.' != ', 1)
->where(COL_POSTID, $data[COL_POSTID])
->get(TBL__POSTIMAGES)
->result_array();
foreach($rimg as $img) {
  if(!empty($img[COL_IMGSHORTCODE])) {
    $postContent = str_replace($img[COL_IMGSHORTCODE], '<img src="'.MY_UPLOADURL.$img[COL_IMGPATH].'" style="max-width: 100%" /><p style="margin-top: 10px !important; font-size: 10px; font-style:italic; line-height: 1.5 !important">'.$img[COL_IMGDESC].'</p>', $postContent);
  }
}

$arrTags = array();
if(!empty($data[COL_POSTMETATAGS])) {
  $arrTags = explode(",", $data[COL_POSTMETATAGS]);
}

$txtShareWA = 'Jangan lewatkan update berita dan informasi terbaru dari '.$this->setting_web_name.' | '.urlencode(current_url());

$rlainnya = $this->mpost->search(9,"",1,null,$data[COL_POSTID]);
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?=$data[COL_POSTTITLE]?>">
  <meta name="author" content="Partopi Tao">
  <meta name="keyword" content="daksa, studio, daksa studio, course, partopi tao, psikotest, bimbel, psikotest online, bimbel online, cat">
  <meta property="og:title" content="<?=$this->setting_web_name.' - '.$data[COL_POSTCATEGORYNAME]?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:image" content="<?=MY_IMAGEURL.'logo-secondary.png'?>" />
  <meta property="og:image:width" content="200" />
  <meta property="og:image:height" content="200" />

  <title><?=$this->setting_web_name.' - '.$data[COL_POSTCATEGORYNAME]?></title>

  <link href="<?=base_url()?>assets/themes/gotto/css/fonts.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/bootstrap-icons.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/owl.theme.default.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/tooplate-gotto-job.css" rel="stylesheet">

  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

  <script src="<?=base_url()?>assets/themes/gotto/js/jquery.min.js"></script>
  <script src="<?=base_url()?>assets/themes/gotto/js/bootstrap.min.js"></script>

  <link rel="icon" type="image/png" href=<?=base_url().$this->setting_web_icon?>>
  <style>
  .se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?=base_url().$this->setting_web_preloader?>') center no-repeat #fff;
  }
  .categories-block:hover {
    border-color: var(--secondary-color) !important;
  }
  .btn-float{
  	position:fixed;
  	width:60px;
  	height:60px;
  	bottom:100px;
  	right:40px;
  	background-color:#25d366;
  	color:#FFF;
  	border-radius:50px;
  	text-align:center;
    font-size:30px;
  	box-shadow: 2px 2px 3px #999;
    z-index:100;
  }

  .my-float{
  	margin-top:16px;
  }
  </style>
</head>
<body id="top">
  <div class="se-pre-con"></div>
  <nav class="navbar navbar-expand-lg">
    <div class="container">
      <a class="navbar-brand d-flex align-items-center" href="<?=site_url()?>">
        <img src="<?=base_url().$this->setting_web_logo2?>" class="img-fluid logo-image" style="width: 240px !important">
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav align-items-center ms-lg-5">
              <li class="nav-item ms-lg-auto">
                  <a class="nav-link" href="<?=site_url()?>">Beranda</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#article">Artikel</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#galeri">Galeri</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#testimonial">Testimoni</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link custom-btn btn" href="<?=site_url('site/user/register')?>"><i class="far fa-user-plus"></i> Daftar</a>
              </li>
          </ul>
      </div>
    </div>
  </nav>
  <main>
    <header class="site-header" style="background-image: url('<?=MY_IMAGEURL.'bg-home.png'?>') !important">
      <div class="section-overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-12 text-center">
            <h2 class="text-white"><?=$data[COL_POSTCATEGORYID]!=5?$data[COL_POSTCATEGORYNAME]:$data[COL_POSTTITLE]?></h2>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="<?=site_url()?>">Beranda</a></li>
                <li class="breadcrumb-item"><a href="<?=site_url('site/home/post/'.$data[COL_POSTCATEGORYID])?>"><?=$data[COL_POSTCATEGORYNAME]?></a></li>
                <li class="breadcrumb-item active"><?=(strlen($data[COL_POSTTITLE]) > 50 ? substr($data[COL_POSTTITLE], 0, 50) . "..." : $data[COL_POSTTITLE])?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </header>
    <section class="job-section section-padding pb-0">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-12 mb-3">
            <h3 class="job-title mb-0"><?=$data[COL_POSTTITLE]?></h3>
            <div class="job-thumb job-thumb-detail">
              <div class="d-flex flex-wrap align-items-center border-bottom pt-lg-3 pt-2 pb-3 mb-4">
                <p class="job-location mb-0"><i class="custom-icon me-1 far fa-calendar"></i> <?=date('d-m-Y', strtotime($data[COL_POSTDATE]))?></p>
                <p class="job-price mb-0"><i class="custom-icon me-1 far fa-user"></i> <?=$data[COL_FULLNAME]?></p>
              </div>
              <?php
              if($data[COL_POSTCATEGORYNAME]!='SKM') {
                ?>
                <div class="row mb-2 p-1">
                  <?php
                  foreach($rheader as $f) {
                    if(strpos(mime_content_type(MY_UPLOADPATH.$f[COL_IMGPATH]), 'image') !== false) {
                      ?>
                      <div class="col-12 col-sm-6 col-md-6 d-flex align-items-stretch p-2">
                        <div href="<?=MY_UPLOADURL.$f[COL_IMGPATH]?>"
                        data-toggle="lightbox"
                        data-title="<?=$data[COL_POSTTITLE]?>"
                        data-gallery="gallery"
                        style="background: url('<?=MY_UPLOADURL.$f[COL_IMGPATH]?>');
                        background-size: cover;
                        background-repeat: no-repeat;
                        background-position: center;
                        width: 100%;
                        min-height: 300px;
                        cursor: pointer;">
                        </div>
                      </div>
                      <?php
                    } else {
                      ?>
                      <div class="col-12 col-sm-12 col-md-12 mb-3 d-flex align-items-stretch">
                        <embed src="<?=MY_UPLOADURL.$f[COL_IMGPATH]?>" width="100%" height="600" />
                      </div>
                      <?php
                    }
                    ?>
                  <?php
                  }
                  ?>
                </div>
                <?=$data[COL_POSTCONTENT]?>
                <?php
              } else {
                ?>
                <div class="row mb-2 p-1">
                  <div class="col-lg-6 col-12 mt-5 mt-lg-0">
                    <?=$data[COL_POSTCONTENT]?>
                  </div>
                  <?php
                  if(!empty($rheader)) {
                    ?>
                    <div class="col-lg-6 col-12 mt-5 mt-lg-0">
                      <div class="job-thumb job-thumb-detail-box bg-white shadow-lg" style="height: 80vh !important; background-image: url('<?=MY_UPLOADURL.$rheader[0][COL_IMGPATH]?>') !important; background-size: cover">
                      </div>
                    </div>
                    <?php
                  }
                  ?>

                </div>
                <?php
              }
              ?>
              <div class="d-flex justify-content-center flex-wrap mt-5 border-top pt-4">
                <div class="job-detail-share d-flex align-items-center">
                  <p class="mb-0 me-lg-4 me-3">Bagikan:</p>
                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?=current_url()?>" class="bi-facebook"></a>
                  <a href="whatsapp://send?text=<?=$txtShareWA?>" data-action="share/whatsapp/share" class="bi-whatsapp mx-3"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="reviews-section section-padding">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-12">
            <h4 class="text-center mb-5">Berita / Artikel Lainnya</h4>
            <div class="owl-carousel owl-theme">
              <?php
              foreach($rlainnya as $b) {
                $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
                $img = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
                ?>
                <div class="col-12">
                  <div class="job-thumb job-thumb-box bg-white">
                    <div
                    class="job-image-box-wrap"
                    style="
                      height: 250px;
                      width: 100%;
                      background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_IMGPATH]:MY_IMAGEURL.'no-image.png'?>');
                      background-size: cover;
                      background-repeat: no-repeat;
                      background-position: center;
                    ">
                      <div class="job-image-box-wrap-info d-flex align-items-center">
                        <p class="mb-0">
                          <span class="badge badge-level"><?=$b[COL_POSTCATEGORYNAME]?></span>
                        </p>
                      </div>
                    </div>
                    <div class="job-body" style="min-height: 320px; max-height: 320px">
                      <h5 class="job-title">
                        <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>" class="job-title-link"><?=strlen($b[COL_POSTTITLE]) > 60 ? substr($b[COL_POSTTITLE], 0, 60) . "..." : $b[COL_POSTTITLE] ?></a>
                      </h5>
                      <div class="d-flex align-items-center">
                        <p class="job-location"><i class="custom-icon far fa-user-circle"></i>&nbsp;&nbsp;<?=$b[COL_FULLNAME]?></p>
                        <p class="job-date"><i class="custom-icon far fa-calendar"></i>&nbsp;&nbsp;<?=date('d-m-Y', strtotime($b[COL_POSTDATE]))?></p>
                      </div>
                      <div class="border-top pt-3">
                        <p class="job-price"><?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?></p>
                      </div>
                    </div>
                  </div>
                </div>
                <?php
              }
              ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <footer class="site-footer" id="kontak">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-6 col-12 mb-3">
          <div class="d-flex align-items-center mb-4">
            <img src="<?=MY_IMAGEURL.'logo-full.png'?>" class="img-fluid logo-image" style="width: 300px !important">
          </div>
          <p class="mb-2">
            <i class="custom-icon fas fa-map-marked-alt me-1"></i>
            <a href="#" class="site-footer-link"><?=$this->setting_org_address?></a>
          </p>

          <p class="mb-2">
            <i class="custom-icon fas fa-phone-rotary me-1"></i>
            <a href="#" class="site-footer-link"><?=$this->setting_org_phone?></a>
          </p>

          <p class="mb-2">
            <i class="custom-icon fas fa-envelope me-1"></i>
            <a href="#" class="site-footer-link"><?=$this->setting_org_mail?></a>
          </p>
        </div>
        <div class="col-lg-5 col-md-6 col-12 mt-3 mt-lg-0">
          <h6 class="site-footer-title">Statistik Pengunjung</h6>
          <div class="newsletter-form">
            <p class="mb-0"><small>HARI INI</small><strong style="float: right !important; font-size: .875em;"><?=number_format($numStatToday)?></strong></p>
            <p class="mb-0"><small>BULAN INI</small><strong style="float: right !important; font-size: .875em;"><?=number_format($numStatMonthly)?></strong></p>
            <p class="mb-0"><small>TOTAL</small><strong style="float: right !important; font-size: .875em;"><?=number_format($numStatTotal)?></strong></p>
          </div>
        </div>
      </div>
    </div>

    <div class="site-footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-12 d-flex align-items-center">
            <p class="copyright-text">Copyright &copy; <?=date('Y')?> <?=$this->setting_web_name?></p>
            <ul class="footer-menu d-flex">
              <li class="footer-menu-item"><a href="#" class="footer-menu-link">Privacy Policy</a></li>
              <li class="footer-menu-item"><a href="#" class="footer-menu-link">Terms</a></li>
            </ul>
          </div>
          <div class="col-lg-6 col-12 mt-2 mt-lg-0" style="text-align: right">
            <p style="padding-right: 15px">Developed by : <a class="sponsored-link" rel="sponsored" href="https://www.linkedin.com/in/yoelrolas/" target="_blank">Partopi Tao</a></p>
          </div>
          <!--<a class="back-top-icon bi-arrow-up smoothscroll d-flex justify-content-center align-items-center" href="#top"></a>-->
        </div>
      </div>
    </div>
  </footer>
  <a href="https://api.whatsapp.com/send?phone=<?=$this->setting_org_fax?>&text=Halo" class="btn-float" target="_blank">
    <i class="fab fa-whatsapp my-float"></i>
  </a>

  <!-- JAVASCRIPT FILES -->
  <script src="<?=base_url()?>assets/themes/gotto/js/owl.carousel.min.js"></script>
  <script src="<?=base_url()?>assets/themes/gotto/js/counter.js"></script>
  <script src="<?=base_url()?>assets/themes/gotto/js/custom.js"></script>
  <script type="text/javascript">
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");
  });
  $('.carousel-hero').owlCarousel({
    loop:true,
    margin:10,
    items: 1
  });
  $('.carousel-packages').owlCarousel({
    items:3,
    loop:true,
    autoplay: true,
    margin:30,
      responsive:{
        0:{
          items:1
        },
        600:{
          items:1
        },
        1000:{
          items:3
        }
      }
    });
    $('.owl-carousel').owlCarousel({
      loop:true,
      margin:10,
      /*nav:true,*/
      items: 3,
      responsive : {
        0: {
          items : 1,
        },
        480: {
          items : 2,
        },
        768: {
          items : 3,
        }
      }
    });
  </script>
</body>
</html>
