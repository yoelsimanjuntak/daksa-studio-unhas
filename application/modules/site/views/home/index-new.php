<?php
$numStatToday = $this->db->where('DATE_FORMAT(Timestamp, "%Y-%m-%d")=', date('Y-m-d'))->count_all_results(TBL_WEBLOGS);
$numStatMonthly = $this->db->where('DATE_FORMAT(Timestamp, "%Y-%m")=', date('Y-m'))->count_all_results(TBL_WEBLOGS);
$numStatTotal = $this->db->count_all_results(TBL_WEBLOGS);

$rwelcome = $this->db
->where(COL_CONTENTTYPE,'WelcomeText')
->get(TBL_WEBCONTENT)
->row_array();

$rtestimoni = $this->db
->where(COL_CONTENTTYPE,'Testimonial')
->order_by(COL_UNIQ, 'desc')
->limit(12)
->get(TBL_WEBCONTENT)
->result_array();

$rgaleri = $this->db
->where(COL_CONTENTTYPE,'Galeri')
->order_by(COL_UNIQ, 'desc')
->limit(12)
->get(TBL_WEBCONTENT)
->result_array();

$qpkg = @"
select * from (
  select kat.Uniq, IFNULL(kat.Kategori,'LAINNYA') as Kategori, IFNULL(kat.Kategori,'Z') as Ordering, count(pkg.Uniq) as JlhPkg from mtestpackage pkg
  left join mkategori kat on kat.Uniq = pkg.IdKategori
  where pkg.PkgIsActive=1
  group by kat.Kategori
) tbl order by JlhPkg desc, Ordering asc
";
$rpkg = $this->db->query($qpkg)->result_array();
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?=$this->setting_web_desc?>">
  <meta name="author" content="Partopi Tao">
  <meta name="keyword" content="daksa, studio, daksa studio, course, partopi tao, psikotest, bimbel, psikotest online, bimbel online, cat">
  <meta property="og:title" content="<?=$this->setting_web_name?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:image" content="<?=MY_IMAGEURL.'logo-secondary.png'?>" />
  <meta property="og:image:width" content="200" />
  <meta property="og:image:height" content="200" />

  <title><?=$this->setting_web_name.' - '.$this->setting_web_desc?></title>

  <link href="<?=base_url()?>assets/themes/gotto/css/fonts.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/bootstrap-icons.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/owl.theme.default.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/tooplate-gotto-job.css" rel="stylesheet">

  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

  <script src="<?=base_url()?>assets/themes/gotto/js/jquery.min.js"></script>
  <script src="<?=base_url()?>assets/themes/gotto/js/bootstrap.min.js"></script>

  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>-->
  <script src="<?=base_url()?>assets/themes/gotto/js/jquery.modal.js"></script>
  <link href="<?=base_url()?>assets/themes/gotto/css/jquery.modal.css" rel="stylesheet">
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />-->

  <link rel="icon" type="image/png" href=<?=base_url().$this->setting_web_icon?>>
  <style>
  .se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?=base_url().$this->setting_web_preloader?>') center no-repeat #fff;
  }
  .categories-block:hover {
    border-color: var(--secondary-color) !important;
  }
  .btn-float{
  	position:fixed;
  	width:60px;
  	height:60px;
  	bottom:100px;
  	right:40px;
  	background-color:#25d366;
  	color:#FFF;
  	border-radius:50px;
  	text-align:center;
    font-size:30px;
  	box-shadow: 2px 2px 3px #999;
    z-index:100;
  }

  .my-float{
  	margin-top:16px;
  }

  a {
    .job-image-box-wrap {
      position: relative;
      width:100px;
      height:100px;
      background-size:cover;
         &:before {
            opacity:0;
            content: '';
            position: absolute;
            top:0px;
            right:0px;
            left:0px;
            bottom:0px;
            background-color: #717275;
            mix-blend-mode:multiply;
            transition:opacity .25s ease;
         }
    } &:hover {
       .job-image-box-wrap {
          &:before {
             opacity:1;
          }
       }
    }
  }

  .modal {
    height: auto !important;
    overflow: visible !important;
  }
  .owl-carousel .owl-item .item-hero {
    min-height: 200px !important;
  }
  .owl-carousel .owl-item {
    /*max-width: 100% !important;*/
  }
  @media screen and (min-width: 992px) {
    .owl-carousel .owl-item .item-hero {
      height: 360px !important;
    }
  }
  </style>
</head>
<body id="top">
  <div class="se-pre-con"></div>
  <nav class="navbar navbar-expand-lg">
    <div class="container">
      <a class="navbar-brand d-flex align-items-center" href="<?=site_url()?>">
        <img src="<?=base_url().$this->setting_web_logo2?>" class="img-fluid logo-image" style="width: 240px !important">
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav align-items-center ms-lg-5">
              <li class="nav-item ms-lg-auto">
                  <a class="nav-link active" href="<?=site_url()?>">Beranda</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#article">Artikel</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#galeri">Galeri</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#testimonial">Testimoni</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link custom-btn btn" href="<?=site_url('site/user/register')?>"><i class="far fa-user-plus"></i> Daftar</a>
              </li>
          </ul>
      </div>
    </div>
  </nav>
  <main>
    <section class="hero-section d-flex justify-content-center align-items-center" style="background-image: url('<?=MY_IMAGEURL.'bg-home.png'?>'); background-position-y: bottom !important">
      <div class="section-overlay"></div>
      <div class="container">
          <div class="row">
            <div class="col-lg-6 col-12 mb-5 mb-lg-0">
              <div class="hero-section-text mt-5">
                <h4 class="text-white"><?=$this->setting_org_name?></h4>
                <p><?=!empty($rwelcome)?$rwelcome[COL_CONTENTDESC1]:''?></p>
                <a href="<?=site_url('site/user/login')?>" class="custom-btn custom-border-btn btn"><i class="far fa-sign-in"></i> Login</a>
                <a href="<?=site_url('site/user/register')?>" class="custom-btn custom-border-btn btn"><i class="far fa-user-plus"></i> Daftar</a>
              </div>
            </div>
            <div class="col-lg-6 col-12">
              <div class="owl-carousel owl-theme carousel-hero">
                <?php
                foreach(glob(MY_IMAGEPATH.'slide/*') as $filename) {
                  ?>
                  <div class="item item-hero" style="background-image: url('<?=MY_IMAGEURL.'slide/'.basename($filename)?>'); background-size: cover; background-position: center; border-radius: 2%"></div>
                  <?php
                }
                ?>
              </div>
            </div>

          </div>
      </div>
    </section>
    <section class="categories-section section-padding" id="categories-section">
      <div class="container">
          <div class="row justify-content-center align-items-center">
            <div class="col-lg-12 col-12 text-center">
              <h2 class="mb-5">Keuntungan</h2>
            </div>
            <div class="col-lg-2 col-md-4 col-6">
              <div class="categories-block">
                <a href="#" class="d-flex flex-column justify-content-center align-items-center h-100">
                  <i class="categories-icon far fa-globe"></i>
                  <small class="categories-block-title">MUDAH DIAKSES</small>
                </a>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-6">
              <div class="categories-block">
                <a href="#" class="d-flex flex-column justify-content-center align-items-center h-100">
                  <i class="categories-icon far fa-books"></i>
                  <small class="categories-block-title">SOAL LENGKAP</small>
                </a>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-6">
              <div class="categories-block">
                <a href="#" class="d-flex flex-column justify-content-center align-items-center h-100">
                  <i class="categories-icon far fa-lightbulb-on"></i>
                  <small class="categories-block-title">PEMBAHASAN</small>
                </a>
              </div>
            </div>
            <div class="col-lg-2 col-md-4 col-6">
              <div class="categories-block">
                <a href="#" class="d-flex flex-column justify-content-center align-items-center h-100">
                  <i class="categories-icon far fa-wallet"></i>
                  <small class="categories-block-title">TERJANGKAU</small>
                </a>
              </div>
            </div>
          </div>
      </div>
    </section>
    <section class="reviews-section section-padding" id="package">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-12">
            <h2 class="text-center mb-5">Paket CAT Online</h2>
            <div class="owl-carousel owl-theme carousel-packages">
              <?php
              foreach($rpkg as $pkg) {
                $rpkgs = $this->db->select('mtestpackage.*, mkategori.Kategori')
                ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_MTESTPACKAGE.".".COL_IDKATEGORI,"left")
                ->where((!empty($pkg[COL_UNIQ])?'mtestpackage.IdKategori='.$pkg[COL_UNIQ]:'mtestpackage.IdKategori is null'))
                ->where(COL_PKGISACTIVE, 1)
                ->order_by(COL_PKGNAME, 'asc')
                ->get(TBL_MTESTPACKAGE)
                ->result_array();
                $rpkg_ = $this->db
                ->where((!empty($pkg[COL_UNIQ])?'mtestpackage.IdKategori='.$pkg[COL_UNIQ]:'mtestpackage.IdKategori is null'))
                ->where(COL_PKGISACTIVE, 1)
                ->order_by(COL_PKGPRICE, 'asc')
                ->get(TBL_MTESTPACKAGE)
                ->row_array();
                ?>
                <div class="reviews-thumb">
                  <div class="reviews-info" style="padding-bottom: 5px !important">
                    <div class="d-flex align-items-center justify-content-between flex-wrap w-100 ms-3">
                      <p class="mb-0">
                        <strong><?=$pkg[COL_KATEGORI]?></strong>
                      </p>
                      <small class="pull-right">Mulai <strong>Rp. <?=number_format($rpkg_[COL_PKGPRICE])?></strong></small>
                    </div>
                  </div>
                  <div class="reviews-body" style="padding-top: 10px !important">
                    <div class="row">
                      <ul>
                        <?php
                        $n=0;
                        foreach($rpkgs as $p) {
                          if($n>=5) {
                            ?>
                            <li><small>dan lain-lain</small></li>
                            <?php
                            break;
                          }
                          ?>
                          <li><small><?=strtoupper($p[COL_PKGNAME])?></small></li>
                          <?php
                          $n++;
                        }
                        ?>
                      </ul>
                      <p style="text-align: right">
                        <a href="<?=site_url('site/home/package/'.$pkg[COL_UNIQ])?>" class="custom-btn btn ms-auto" style="padding: 10px 20px !important">DAFTAR <i class="far fa-arrow-circle-right"></i></a>
                      </p>
                    </div>
                  </div>
                </div>
                <?php
              }
              ?>

            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="reviews-section section-padding" id="article">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-12 col-12">
            <h2 class="text-center mb-5">Artikel</h2>
          </div>
          <div class="clearfix"></div>
          <?php
          foreach($berita as $b) {
            $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
            $tags = explode(",",$b[COL_POSTMETATAGS]);
            $img = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
            ?>
            <div class="col-lg-4 col-md-6 col-12">
              <div class="job-thumb job-thumb-box bg-white">
                <div
                class="job-image-box-wrap"
                style="
                  height: 250px;
                  width: 100%;
                  background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_IMGPATH]:MY_IMAGEURL.'no-image.png'?>');
                  background-size: cover;
                  background-repeat: no-repeat;
                  background-position: center;
                ">
                  <div class="job-image-box-wrap-info d-flex align-items-center">
                    <?php
                    if(!empty($tags)) {
                      ?>
                      <p class="mb-0">
                        <?php
                        $ct = 0;
                        foreach($tags as $t) {
                          if($ct>2) break;
                          ?>
                          <span class="badge badge-level"><?=(strlen($t) > 10 ? substr(strtoupper($t), 0, 10) . "..." : strtoupper($t))?></span>
                          <?php
                          $ct++;
                        }
                        ?>
                      </p>
                      <?php
                    }
                    ?>
                  </div>
                </div>
                <div class="job-body" style="min-height: 320px; max-height: 320px">
                  <h5 class="job-title">
                    <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>" class="job-title-link"><?=strlen($b[COL_POSTTITLE]) > 60 ? substr($b[COL_POSTTITLE], 0, 60) . "..." : $b[COL_POSTTITLE] ?></a>
                  </h5>
                  <div class="d-flex align-items-center">
                    <p class="job-location"><i class="custom-icon far fa-user-circle"></i>&nbsp;&nbsp;<?=$b[COL_FULLNAME]?></p>
                    <p class="job-date"><i class="custom-icon far fa-calendar"></i>&nbsp;&nbsp;<?=date('d-m-Y', strtotime($b[COL_CREATEDON]))?></p>
                  </div>
                  <div class="border-top pt-3">
                    <p class="job-price"><?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?></p>
                  </div>
                </div>
              </div>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </section>
    <section class="job-section recent-jobs-section section-padding" id="galeri">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-12 mb-4">
            <h2 class="text-center mb-5">Galeri</h2>
          </div>
          <div class="clearfix"></div>
          <?php
          foreach($rgaleri as $g) {
            if(!file_exists(MY_UPLOADPATH.$g[COL_CONTENTDESC2])) continue;
            ?>
            <div class="col-lg-4 col-md-6 col-12">
              <div class="job-thumb job-thumb-box">
                <a class="link-galeri" href="<?=MY_UPLOADURL.$g[COL_CONTENTDESC2]?>" data-title="<?=$g[COL_CONTENTTITLE]?>" data-desc="<?=$g[COL_CONTENTDESC1]?>">
                  <div class="job-image-box-wrap" style="
                  height: 400px;
                  width: 100%;
                  background-image: url('<?=MY_UPLOADURL.$g[COL_CONTENTDESC2]?>');
                  background-size: cover;
                  background-repeat: no-repeat;
                  background-position: center;
                  "></div>

                  <!--<div class="job-body">
                    <h6 class="job-title">
                      <a href="#" class="job-title-link"><?=$g[COL_CONTENTTITLE]?></a>
                    </h6>
                  </div>-->
                </a>
              </div>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </section>

    <section class="reviews-section section-padding" id="testimonial">
      <div class="container">
          <div class="row">
            <div class="col-lg-12 col-12">
              <h2 class="text-center mb-5">Testimoni</h2>
              <div class="owl-carousel owl-theme reviews-carousel">
                <?php
                foreach($rtestimoni as $t) {
                  ?>
                  <div class="reviews-thumb">
                    <div class="reviews-info d-flex align-items-center">
                      <img src="<?=base_url()?>assets/themes/gotto/images/testimonial-icon.png" class="avatar-image img-fluid" alt="" style="border: none !important; width: auto !important; height: auto !important">
                      <div class="d-flex align-items-center justify-content-between flex-wrap w-100 ms-3">
                          <p class="mb-0">
                              <strong><?=strtoupper($t[COL_CONTENTDESC1])?></strong>
                              <small><?=$t[COL_CONTENTTITLE]?></small>
                          </p>
                      </div>
                    </div>
                    <div class="reviews-body">
                      <img src="<?=base_url()?>assets/themes/gotto/images/left-quote.png" class="quote-icon img-fluid" alt="">
                      <p class="reviews-title"><?=$t[COL_CONTENTDESC2]?></p>
                    </div>
                  </div>
                  <?php
                }
                ?>
              </div>
            </div>
          </div>
      </div>
    </section>
  </main>

  <footer class="site-footer" id="kontak">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-6 col-12 mb-3">
          <div class="d-flex align-items-center mb-4">
            <img src="<?=MY_IMAGEURL.'logo-full.png'?>" class="img-fluid logo-image" style="width: 300px !important">
          </div>
          <p class="mb-2">
            <i class="custom-icon fas fa-map-marked-alt me-1"></i>
            <a href="#" class="site-footer-link"><?=$this->setting_org_address?></a>
          </p>

          <p class="mb-2">
            <i class="custom-icon fas fa-phone-rotary me-1"></i>
            <a href="#" class="site-footer-link"><?=$this->setting_org_phone?></a>
          </p>

          <p class="mb-2">
            <i class="custom-icon fas fa-envelope me-1"></i>
            <a href="#" class="site-footer-link"><?=$this->setting_org_mail?></a>
          </p>
        </div>
        <div class="col-lg-5 col-md-6 col-12 mt-3 mt-lg-0">
          <h6 class="site-footer-title">Statistik Pengunjung</h6>
          <div class="newsletter-form">
            <p class="mb-0"><small>HARI INI</small><strong style="float: right !important; font-size: .875em;"><?=number_format($numStatToday)?></strong></p>
            <p class="mb-0"><small>BULAN INI</small><strong style="float: right !important; font-size: .875em;"><?=number_format($numStatMonthly)?></strong></p>
            <p class="mb-0"><small>TOTAL</small><strong style="float: right !important; font-size: .875em;"><?=number_format($numStatTotal)?></strong></p>
          </div>
        </div>
      </div>
    </div>

    <div class="site-footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <p class="copyright-text" style="margin-right: 0 !important">&copy; <?=date('Y')?> <?=$this->setting_web_name?><span style="float: right">By : <a class="sponsored-link" rel="sponsored" href="https://www.linkedin.com/in/yoelrolas/" target="_blank">Partopi Tao</a></span></p>
          </div>
          <!--<a class="back-top-icon bi-arrow-up smoothscroll d-flex justify-content-center align-items-center" href="#top"></a>-->
        </div>
      </div>
    </div>
  </footer>
  <a href="https://api.whatsapp.com/send?phone=<?=$this->setting_org_fax?>&text=Halo" class="btn-float" target="_blank">
    <i class="fab fa-whatsapp my-float"></i>
  </a>
  <div id="modal-galeri" class="modal">
    <h5 style="color: var(--primary-color)">Loading...</h5>
    <img style="width: 100%;" src="" />
    <p class="mt-2 text-center">-</p>
  </div>

  <!-- JAVASCRIPT FILES -->
  <script src="<?=base_url()?>assets/themes/gotto/js/owl.carousel.min.js"></script>
  <script src="<?=base_url()?>assets/themes/gotto/js/counter.js"></script>
  <script src="<?=base_url()?>assets/themes/gotto/js/custom.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $(".se-pre-con").fadeOut("slow");
    $('.link-galeri').click(function(){
      var title = $(this).data('title');
      var img = $(this).attr('href');
      var desc = $(this).data('desc');

      $('h5',$('#modal-galeri')).html(title);
      $('p',$('#modal-galeri')).html(desc);
      $('img',$('#modal-galeri')).attr('src', img);
      $('#modal-galeri').jmodal();
      return false;
    });
  });
  $('.carousel-hero').owlCarousel({
    loop:true,
    margin:10,
    items: 1
  });
  $('.carousel-packages').owlCarousel({
    items:3,
    loop:true,
    autoplay: true,
    margin:30,
      responsive:{
        0:{
          items:1
        },
        600:{
          items:1
        },
        1000:{
          items:3
        }
      }
    });
  </script>
</body>
</html>
