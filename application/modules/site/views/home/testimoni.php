<?php
$rwelcome = $this->db
->where(COL_CONTENTTYPE,'WelcomeText')
->get(TBL_WEBCONTENT)
->row_array();

$rtestimoni = $this->db
->where(COL_CONTENTTYPE,'Testimonial')
->order_by(COL_UNIQ, 'desc')
->get(TBL_WEBCONTENT)
->result_array();

$rgaleri = $this->db
->where(COL_CONTENTTYPE,'Galeri')
->get(TBL_WEBCONTENT)
->result_array();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta property="og:title" content="<?=$this->setting_web_desc?> - Bimbel dan Psikotes Online Kedinasan" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:image" content="<?=MY_IMAGEURL.'logo-main.jpeg'?>" />

  <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
  <link rel="icon" type="image/png" href=<?=base_url().$this->setting_web_icon?>>

  <title><?=!empty($title) ? $title.' - '.$this->setting_web_name : $this->setting_web_name?></title>
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/softy/assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/softy/assets/css/font-awesome.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/softy/assets/css/templatemo-softy-pinko.css">

  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
  <style>
  .btn-float{
  	position:fixed;
  	width:60px;
  	height:60px;
  	bottom:100px;
  	right:40px;
  	background-color:#25d366;
  	color:#FFF;
  	border-radius:50px;
  	text-align:center;
    font-size:30px;
  	box-shadow: 2px 2px 3px #999;
    z-index:100;
  }

  .my-float{
  	margin-top:16px;
  }

  #form-testimoni input {
    height: 50px;
  }

  #form-testimoni input, #form-testimoni textarea {
    color: #777;
    font-size: 14px;
    border: 1px solid #eee;
    width: 100%;
    outline: none;
    padding-left: 20px;
    padding-right: 20px;
    border-radius: 25px;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin-bottom: 30px;
  }

  button.main-button-slider {
    font-size: 13px;
    border-radius: 20px;
    padding: 12px 20px;
    background-color: #ff589e;
    text-transform: uppercase;
    color: #fff;
    letter-spacing: 0.25px;
    -webkit-transition: all 0.3s ease 0s;
    -moz-transition: all 0.3s ease 0s;
    -o-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    text-decoration: none !important;
  }

  button.main-button-slider:hover {
    background-color: #8261ee;
  }
  </style>
</head>
<body>
  <!-- ***** Preloader Start ***** -->
  <div id="preloader">
      <div class="jumper">
          <div></div>
          <div></div>
          <div></div>
      </div>
  </div>
  <!-- ***** Preloader End ***** -->
  <!-- ***** Header Area Start ***** -->
  <header class="header-area header-sticky">
      <div class="container">
          <div class="row">
              <div class="col-12">
                  <nav class="main-nav">
                      <!-- ***** Logo Start ***** -->
                      <a href="<?=site_url()?>" class="logo">
                        <img src="<?=base_url().$this->setting_web_logo2?>" alt="Logo" style="height: 30px"/>
                      </a>
                      <!-- ***** Logo End ***** -->
                      <!-- ***** Menu Start ***** -->
                      <ul class="nav">
                          <li><a href="<?=site_url()?>" class="active">Beranda</a></li>
                          <li><a href="<?=site_url()?>#testimonials">Testimoni</a></li>
                          <li><a href="<?=site_url()?>#blog">Galeri</a></li>
                          <li><a href="<?=site_url()?>#contact-us">Kontak</a></li>
                      </ul>
                      <a class='menu-trigger'>
                          <span>Menu</span>
                      </a>
                      <!-- ***** Menu End ***** -->
                  </nav>
              </div>
          </div>
      </div>
  </header>
  <!-- ***** Header Area End ***** -->
  <!-- ***** Welcome Area Start ***** -->
  <div class="welcome-area" id="welcome" style="height: 50vh !important">
    <div class="header-text" style="top: 80% !important">
      <div class="container">
        <div class="row">
          <div class="offset-xl-3 col-xl-6 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
            <h1>Testimoni</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ***** Welcome Area End ***** -->

  <!-- ***** Testimonials Start ***** -->
  <section class="section" id="testimonials" style="padding-top: 50px !important">
    <div class="container">
      <div class="row">
        <div class="offset-lg-3 col-lg-6">
          <div class="center-text">
            <p style="margin-bottom: 0 !important">Berikan komentar dan kesan kamu setelah menikmati fasilitas dan layanan di <?=$this->setting_web_desc?></p>
          </div>
        </div>
        <div class="offset-lg-2 col-lg-8" style="margin-bottom: 50px">
          <div class="team-item">
            <div class="team-content" style="padding: 20px">
              <form id="form-testimoni" action="<?=site_url('site/home/testimoni-add')?>" method="post">
                <div class="row">
                  <div class="col-12">
                    <fieldset>
                      <input name="TestimoniName" type="text" class="form-control" placeholder="Nama" required />
                    </fieldset>
                    <fieldset>
                      <input name="TestimoniTitle" type="text" class="form-control" placeholder="Keterangan, misal: Lulus Seleksi IPDN Th. 2019" required />
                    </fieldset>
                    <fieldset>
                      <textarea name="TestimoniText" class="form-control" rows="3" placeholder="Testimoni" required></textarea>
                    </fieldset>
                  </div>
                  <div class="col-12 text-center">
                    <button type="submit" class="main-button-slider" style="border: 1px solid transparent">SUBMIT</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

        <?php
        foreach($rtestimoni as $t) {
          ?>
          <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="team-item">
              <div class="team-content">
                <i style="margin-left: 0 !important"><img src="<?=base_url()?>assets/themes/softy/assets/images/testimonial-icon.png" alt=""></i>
                <p><?=$t[COL_CONTENTDESC2]?></p>
                <div class="team-info">
                  <h3 class="user-name"><?=$t[COL_CONTENTDESC1]?></h3>
                  <span><?=$t[COL_CONTENTTITLE]?></span>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </section>
  <!-- ***** Testimonials End ***** -->

  <!-- ***** Footer Start ***** -->
  <footer style="padding-top: 0 !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <p class="copyright" style="border-top: none !important; margin-top: 0 !important">Copyright &copy; <?=date('Y')?> <?=$this->setting_web_name?></p>
        </div>
      </div>
    </div>
  </footer>
  <a href="https://api.whatsapp.com/send?phone=<?=$this->setting_org_phone?>&text=Halo" class="btn-float" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
  </a>

  <!-- jQuery -->
  <script src="<?=base_url()?>assets/themes/softy/assets/js/jquery-2.1.0.min.js"></script>

  <!-- Bootstrap -->
  <script src="<?=base_url()?>assets/themes/softy/assets/js/popper.js"></script>
  <script src="<?=base_url()?>assets/themes/softy/assets/js/bootstrap.min.js"></script>

  <!-- Plugins -->
  <script src="<?=base_url()?>assets/themes/softy/assets/js/scrollreveal.min.js"></script>
  <script src="<?=base_url()?>assets/themes/softy/assets/js/waypoints.min.js"></script>
  <script src="<?=base_url()?>assets/themes/softy/assets/js/jquery.counterup.min.js"></script>
  <script src="<?=base_url()?>assets/themes/softy/assets/js/imgfix.min.js"></script>

  <!-- Global Init -->
  <script src="<?=base_url()?>assets/themes/softy/assets/js/custom.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#form-testimoni').validate({
      ignore: "input[type='file']",
      submitHandler: function(form) {
        var modal = $(form).closest('.modal');
        var btnSubmit = null;
        var txtSubmit = '';
        if(modal) {
          var btnSubmit = $('button[type=submit]', modal);
          var txtSubmit = btnSubmit.html();
          btnSubmit.html('LOADING...');
          btnSubmit.attr('disabled', true);
        }

        $(form).ajaxSubmit({
          dataType: 'json',
          type : 'post',
          success: function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success('Berhasil!');
              setTimeout(function(){
                location.href = '<?=site_url()?>';
              }, 1000);
            }
          },
          error: function() {
            toastr.error('SERVER ERROR');
          },
          complete: function() {
            btnSubmit.html(txtSubmit);
            btnSubmit.attr('disabled', false);
          }
        });

        return false;
      }
    });
  });
  </script>
</body>
</html>
