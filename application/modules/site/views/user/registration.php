<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
div.filtering .select2-container {
  display: inline-block !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>NAMA</th>
                    <th>TEMPAT / TGL. LAHIR </th>
                    <th>NO. HP</th>
                    <th>PROGRAM PILIHAN</th>
                    <th>LOG</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-detail" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">PENDAFTARAN </span>
      </div>
      <div class="modal-body p-0">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;TUTUP</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var modal = $('#modal-detail');
  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/user/registration-load')?>",
      "type": 'POST',
      "data": function(data){

       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8 filtering'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "dom":"R<'row'<'col-sm-12 d-flex'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": [[ 5, "desc" ]],
    "columnDefs": [
      {"targets":[0,1], "className":'nowrap'},
      {"targets":[5], "className":'nowrap dt-body-right'}
    ],
    "columns": [
      {"orderable": false,"width": "10px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('[data-toggle="tooltip"]', $(row)).tooltip();
      $('.btn-detail', row).click(function() {
        var url = $(this).attr('href');
        modal.modal('show');
        $('.modal-body', modal).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
        $('.modal-body', modal).load(url, function(){

        });
        return false;
      });
      $('.btn-delete', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });
  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });

  modal.on('hidden.bs.modal', function (e) {
    $('.modal-body', modal).empty();
  });
});
</script>
