<form id="form-changepass" method="post" action="#">
  <div class="modal-header">
    <h5 class="modal-title">Ubah Password</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><i class="fa fa-close"></i></span>
    </button>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group row">
          <label class="control-label col-sm-5">PASSWORD LAMA</label>
          <div class="col-sm-7">
            <input type="password" class="form-control" name="OldPassword" required />
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-sm-5">PASSWORD BARU</label>
          <div class="col-sm-7">
            <input type="password" class="form-control" name="NewPassword" required />
          </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-sm-5">KONFIRMASI PASSWORD</label>
          <div class="col-sm-7">
            <input type="password" class="form-control" name="ConfirmPassword" required />
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">BATAL</button>
    <button type="submit" class="btn btn-outline-primary btn-ok">SIMPAN</button>
  </div>
</form>
