<?php
$isIncludeACR = false;
$isIncludeEPPS = false;
$arrItems = json_decode($data[COL_PKGITEMS]);
foreach($arrItems as $i) {
  $rtest = $this->db
  ->where(COL_UNIQ, $i->TestID)
  ->get(TBL_MTEST)
  ->row_array();

  if(!empty($rtest) && $rtest[COL_TESTTYPE]=='ACR') {
    $isIncludeACR = true;
  }
  if(!empty($rtest) && $rtest[COL_TESTREMARKS1]=='EPPS') {
    $isIncludeEPPS = true;
  }
}
?>
<form id="form-session" action="<?=current_url()?>" method="post">
  <p>Apakah anda yakin ingin mengikuti ujian simulasi CAT <strong><?=$data[COL_PKGNAME]?></strong>?</p>
  <input type="hidden" name="<?=COL_IDPACKAGE?>" value="<?=$data[COL_UNIQ]?>" />
  <?php
  if($isIncludeACR) {
    ?>
    <div class="form-group">
      <label class="control-label">OPSI (TEST KECERMATAN)</label>
      <select class="form-control" name="<?=COL_SESSREMARK2?>" style="width: 100%" required>
        <option value="ANGKA">ANGKA</option>
        <option value="HURUF">HURUF</option>
        <option value="SIMBOL">SIMBOL</option>
        <option value="MIXED">ANGKA & HURUF</option>
      </select>
    </div>
    <?php
  } else {
    ?>
    <div class="form-group">
      <label class="control-label"><input type="checkbox" name="IsRandom" value="1" />&nbsp;&nbsp;ACAK SOAL</label>
    </div>
    <?php
  }
  ?>

  <div class="form-group">
    <button type="submit" class="btn btn-sm btn-outline-primary btn-block">LANJUT&nbsp;<i class="far fa-arrow-circle-right"></i></button>
  </div>
</form>
