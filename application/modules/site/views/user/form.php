<form id="form-user" action="<?=current_url()?>" method="post">
  <div class="row mt-2 mb-3">
    <div class="col-sm-12 text-center">
      <img src="<?=!empty($data)&&!empty($data[COL_PROFILEPIC])?MY_UPLOADURL.$data[COL_PROFILEPIC]:MY_IMAGEURL.'icon-user.jpg'?>" class="profile-user-img img-fluid img-circle" style="border: 2px solid #adb5bd; height: 100px" alt="Avatar / Foto Profil">
    </div>
  </div>
  <div class="form-group">
    <label>NAMA LENGKAP</label>
    <input type="text" name="<?=COL_FULLNAME?>" class="form-control" value="<?=!empty($data)?$data[COL_FULLNAME]:''?>" required />
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        <label>USERNAME</label>
        <input type="text" name="<?=COL_EMAIL?>" class="form-control" value="<?=!empty($data)?$data[COL_EMAIL]:''?>" required />
      </div>
      <div class="col-sm-6">
        <label>NO. TELP</label>
        <input type="text" name="<?=COL_PHONE?>" class="form-control" value="<?=!empty($data)?$data[COL_PHONE]:''?>" required />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>PASSWORD <?=!empty($data)?'<small id="info-password" class="text-muted">(isi jika ingin mengubah password)</small>':''?></label>
    <input type="password" name="<?=COL_PASSWORD?>" class="form-control" />
  </div>
  <div class="form-group text-right mt-3">
    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
    <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  $('#form-user').validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
