<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta property="og:title" content="<?=$this->setting_web_desc?>" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="<?=base_url()?>" />
		<meta property="og:image" content="<?=base_url().$this->setting_web_logo?>" />
    <link rel="icon" type="image/png" href="<?=base_url().$this->setting_web_icon?>">

    <title><?= $this->setting_web_name ?> - Login</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
    <!-- jQuery -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="<?=base_url()?>assets/js/jquery.particleground.js"></script>

    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">

    <style>
        .adsbox{
            background: none repeat scroll 0 0 #f5f5f5;
            border-radius: 10px;
            box-shadow: 0 0 5px 1px rgba(50, 50, 50, 0.2);
            margin: 20px auto 0;
            padding: 15px;
            border: 1px solid #caced3;
            height: 145px;
        }
        html, body {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }
        #particles {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }
        #intro {
            position: absolute;
            left: 0;
            top: 50%;
            padding: 0 20px;
            width: 100%;
            #text-align: center;
        }
    </style>
</head>
<!-- Preloader Style -->
<style>
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?=base_url().$this->setting_web_preloader?>') center no-repeat #fff;
    }
    .login-page::after {
      /*background: url(http://localhost/hh-bmkg//assets/media/image/footer-map-bg.png) no-repeat scroll center center / 75% auto;*/
      background: url('<?=base_url()?>assets/media/image/bg-login.png');
      content: "";
      height: 100%;
      left: 0;
      opacity: 0.1;
      position: absolute;
      top: 0;
      width: 100%;
      z-index: -1;
      opacity: 0.1;
      background-color: #ffeb3b;
    }
    .user-image {
      height: 80px;
    }
    @media screen and (min-width: 992px) {
      .user-image {
        height: 120px;
      }
    }
</style>
<!-- /.preloader style -->

<!-- Preloader Script -->
<script>
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
</script>
<!-- /.preloader script -->
<body class="hold-transition login-page" style="background: #337357">

<!-- preloader -->
<div class="se-pre-con"></div>
<!-- /.preloader -->

<div id="particles">
  <div id="intro">
    <div class="login-box">
      <div class="card" style="background: #fff">
        <div class="card-body login-card-body" style="background: none">
          <div class="login-logo">
            <a href="<?=site_url()?>">
              <img class="user-image mb-2" src="<?=MY_IMAGEURL.'logo-unhas.png'?>" alt="Logo" style="height: 100px !important"><br  />
            </a>
          </div>

          <?= form_open(current_url(),array('id'=>'form-login')) ?>
          <p class="text-md text-muted">Login menggunakan akun anda:</p>
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="<?=COL_USERNAME?>" placeholder="Username / Email" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="far fa-user-circle"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="Password" required>
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="far fa-lock"></span>
              </div>
            </div>
          </div>
          <!--<div class="row mb-3">
            <div class="col-6">
              <div class="icheck-primary">
                <input type="checkbox" id="remember">
                <label for="remember">INGAT SAYA</label>
              </div>
            </div>
            <div class="col-6 pt-1 text-right">
              <a href="#">Lupa Password?</a>
            </div>
          </div>-->
          <div class="footer" style="text-align: right;">
            <button type="submit" class="btn btn-success btn-block"><i class="fad fa-sign-in"></i>&nbsp;MASUK</button>
            <!--<hr />
            <p class="text-md text-muted">Belum punya akun? <a href="<?=site_url('site/user/register')?>">Daftar Sekarang!</a></p>-->
          </div>
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.motio.min.js"></script>
<script>
$(document).ready(function() {
  $('#particles').particleground({
    dotColor: '#FFC436',
    lineColor: '#fff'
  });
  $('#intro').css({
      'margin-top': -($('#intro').height() / 2)
  });

  $('#form-login').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>').attr('disabled', true);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
            btnSubmit.html(txtSubmit).attr('disabled', false);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              setTimeout(function(){
                location.href = res.redirect;
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
          btnSubmit.html(txtSubmit).attr('disabled', false);
        },
        complete: function() {
          //btnSubmit.html(txtSubmit).attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
</body>
</html>
