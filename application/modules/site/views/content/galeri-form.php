<form id="form-doc" action="<?=current_url()?>" enctype="multipart/form-data">
  <div class="form-group">
    <label>Judul</label>
    <input type="text" class="form-control" name="<?=COL_CONTENTTITLE?>" value="<?=!empty($data)?$data[COL_CONTENTTITLE]:''?>" />
  </div>
  <div class="form-group">
    <label>Keterangan</label>
    <textarea class="form-control" rows="4" name="<?=COL_CONTENTDESC1?>"><?=!empty($data)?$data[COL_CONTENTDESC1]:''?></textarea>
  </div>
  <?php
  if(empty($data)) {
    ?>
    <div class="form-group">
      <label>Foto / Gambar</label>
      <div id="div-attachment">
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <span class="input-group-text"><i class="fad fa-paperclip"></i></span>
          </div>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="file" accept="image/*">
            <label class="custom-file-label" for="file">PILIH FILE</label>
          </div>
        </div>
        <p class="text-sm text-muted mb-0 font-italic">
          <strong>CATATAN:</strong><br />
          - Besar file / dokumen maksimum <strong>50 MB</strong><br />
          - Jenis file / dokumen yang diperbolehkan hanya dalam format <strong>JPG</strong> / <strong>PNG</strong> / <strong>PNG</strong>
        </p>
      </div>
    </div>
    <?php
  }
  ?>
</form>

<script type="text/javascript">
$(document).ready(function(){
  bsCustomFileInput.init();
  $("select", $('#form-doc')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('#form-doc').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var modal = $(form).closest('.modal');
      var btnSubmit = null;
      var txtSubmit = '';
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.html();
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            $('.btn-refresh-data').click();
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
          $(form).closest('.modal').modal('hide');
        }
      });

      return false;
    }
  });
});
</script>
