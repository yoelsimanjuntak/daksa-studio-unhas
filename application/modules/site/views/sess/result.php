<?php
$ruser = GetLoggedUser();
$rtest = $this->db
->where(COL_IDSESSION, $sess[COL_UNIQ])
->where(COL_TESTREMARKS1, null)
->order_by(COL_TESTSEQ, 'asc')
->get(TBL_TSESSIONTEST)
->result_array();

$repps = $this->db
->where(COL_IDSESSION, $sess[COL_UNIQ])
->where(COL_TESTREMARKS1, 'EPPS')
->order_by(COL_TESTSEQ, 'asc')
->get(TBL_TSESSIONTEST)
->result_array();

$rist = $this->db
->where(COL_IDSESSION, $sess[COL_UNIQ])
->order_by(COL_SEQ)->get(TBL_IST_RESULT)
->result_array();

$rpauli = $this->db
->where(COL_IDSESSION, $sess[COL_UNIQ])
->where(COL_TESTTYPE, 'PAULI')
->order_by(COL_TESTSEQ, 'asc')
->get(TBL_TSESSIONTEST)
->result_array();

$rkecermatan = $this->db
->where(COL_IDSESSION, $sess[COL_UNIQ])
->where(COL_TESTTYPE, 'ACR')
->order_by(COL_TESTSEQ, 'asc')
->get(TBL_TSESSIONTEST)
->result_array();

$arrChartIST = array();
$arrChartIST_ = array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?=site_url('site/user/dashboard')?>" class="btn btn-sm btn-primary"><i class="far fa-arrow-circle-left"></i>&nbsp;&nbsp;DASHBOARD</a>
        <div class="card card-default mt-2">
          <div class="card-header">
            <h5 class="card-title m-0 font-weight-bold">SUMMARY</h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-striped" style="max-width: 100%">
              <tbody>
                <tr>
                  <td style="width: 10px; white-space: nowrap">NAMA</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=$sess[COL_FULLNAME]?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">EMAIL</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=$sess[COL_EMAIL]?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">MULAI</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=date('d-m-Y H:i:s', strtotime($sess[COL_SESSTIMESTART]))?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">SELESAI</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=date('d-m-Y H:i:s', strtotime($sess[COL_SESSTIMEEND]))?></strong></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-12">
        <?php
        if($ruser[COL_ROLEID]==ROLEADMIN && !empty($rtest)) {
          ?>
          <div class="card card-default">
            <div class="card-body p-0">
              <table class="table table-hover" style="max-width: 100%">
                <thead>
                  <tr>
                    <th>NAMA TEST</th>
                    <th>MULAI</th>
                    <th>SELESAI</th>
                    <th class="text-center">NILAI / SKOR</th>
                    <?php
                    if($ruser[COL_ROLEID]==ROLEADMIN || true) {
                      ?>
                      <th class="text-center">OPSI</th>
                      <?php
                    }
                    ?>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sum = 0;
                  foreach($rtest as $t) {
                    $rgroup = $this->db
                    ->select('QuestGroup, sum(QuestScore) as QuestScore')
                    ->where(COL_IDTEST, $t[COL_UNIQ])
                    ->where('QuestGroup IS NOT NULL')
                    ->order_by(COL_UNIQ)
                    ->group_by(COL_QUESTGROUP)
                    ->get(TBL_TSESSIONSHEET)
                    ->result_array();

                    $rquest = $this->db
                    ->select_sum(COL_QUESTSCORE)
                    ->where(COL_IDTEST, $t[COL_UNIQ])
                    ->get(TBL_TSESSIONSHEET)
                    ->row_array();
                    $valscore = $rquest[COL_QUESTSCORE];
                    $txtscore = number_format($rquest[COL_QUESTSCORE]);
                    if ($t[COL_TESTREMARKS]=='WAKTU HABIS') {
                      $txtscore = '<span class="text-danger">'.$txtscore.'</span>';
                    } else if(isset($t[COL_TESTSCORE])) {
                      $txtscore = number_format($t[COL_TESTSCORE]);
                      $valscore = $t[COL_TESTSCORE];
                    }
                    ?>
                    <tr>
                      <td style="vertical-align: middle"><?=strtoupper($t[COL_TESTNAME])?></td>
                      <td style="vertical-align: middle; width: 10px; white-space: nowrap" class="text-right"><?=date('H:i:s', strtotime($t[COL_TESTSTART]))?></td>
                      <td style="vertical-align: middle; width: 10px; white-space: nowrap" class="text-right"><?=date('H:i:s', strtotime($t[COL_TESTEND]))?></td>
                      <td style="vertical-align: middle; white-space: nowrap" class="text-center"><strong><?=isset($t[COL_TESTSCORE])?number_format($t[COL_TESTSCORE]):number_format($rquest[COL_QUESTSCORE])?></strong></td>
                      <?php
                      if($ruser[COL_ROLEID]==ROLEADMIN || true) {
                        ?>
                        <td style="vertical-align: middle; width: 10px;  white-space: nowrap" class="text-center">
                          <a target="_blank" href="<?=site_url('site/sess/review/'.$t[COL_UNIQ])?>" class="btn btn-xs btn-success" title="Pembahasan"><i class="far fa-tasks"></i></a>&nbsp;
                          <a target="_blank" href="<?=site_url('site/sess/review-print/'.$t[COL_UNIQ])?>" class="btn btn-xs btn-primary" title="Pembahasan"><i class="far fa-print"></i></a>
                        </td>
                        <?php
                      }
                      ?>
                    </tr>
                    <?php
                    if(!empty($rgroup)) {
                      foreach($rgroup as $g) {
                        ?>
                        <tr>
                          <td style="vertical-align: middle; padding-left: 2.5rem !important" class="font-italic" colspan="3"><?=strtoupper($g[COL_QUESTGROUP])?></td>
                          <td style="vertical-align: middle; white-space: nowrap" class="text-center"><?=number_format($g[COL_QUESTSCORE])?></td>
                          <?php
                          if($ruser[COL_ROLEID]==ROLEADMIN || true) {
                            ?>
                            <td style="vertical-align: middle; width: 10px;  white-space: nowrap" class="text-center"></td>
                            <?php
                          }
                          ?>
                        </tr>
                        <?php
                      }
                    }
                    ?>
                    <?php
                    //$sum += isset($t[COL_TESTSCORE])?$t[COL_TESTSCORE]:0;
                    //$sum += $valscore;
                    if(!(strpos(strtolower($t[COL_TESTNAME]), 'pass hand') !== false)) {
                      $sum += isset($t[COL_TESTSCORE])?$t[COL_TESTSCORE]:$rquest[COL_QUESTSCORE];
                    }
                  }
                  ?>
                  <tr>
                    <th colspan="3" class="text-right">TOTAL</th>
                    <th class="text-center"><?=number_format($sum)?></th>
                    <?php
                    if($ruser[COL_ROLEID]==ROLEADMIN || true) {
                      ?>
                      <th></th>
                      <?php
                    }
                    ?>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <?php
        } else if($ruser[COL_ROLEID]!=ROLEADMIN) {
          ?>
          <div class="card card-default">
            <div class="card-body">
              <h3 class="text-center">SELESAI!</h3>
              <p class="text-center">Terimakasih atas partisipasi anda dalam mengikuti Ujian <strong><?=$sess[COL_PKGNAME]?></strong>.<p>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</section>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/chart.js/Chart.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  <?php
  if(!empty($rist)) {
    ?>
    var chartCanvasIST = $('#chartIST').get(0).getContext('2d');
    var chartIST = new Chart(chartCanvasIST, {
      type: 'line',
      data: {
        "datasets":[
          {
            "fill": false,
            "label":"SKOR",
            "tension":0,
            "animations":{"y":{"duration":2000,"delay":500}},
            "backgroundColor":"#6610f2",
            "data":<?=json_encode($arrChartIST)?>
          }
        ],
        "labels":<?=json_encode($arrChartIST_)?>
      },
      options: {
        responsive : true,
        scales: {
          yAxes: [{
            ticks: {
              min: 50,
              max: 160,
              stepSize: 10
            }
          }]
        }

      }
    });
    <?php
  }
  ?>
});
</script>
