<?php
$htmlTitle = $this->setting_web_name;
$htmlLogo = base_url().$this->setting_web_logo;
$mode = 'withkey';

$rsheet = $this->db
->where(COL_IDTEST, $rtest[COL_UNIQ])
->order_by(COL_UNIQ)
->get(TBL_TSESSIONSHEET)
->result_array();

$rskor = $this->db
->select_sum(COL_QUESTSCORE)
->where(COL_IDTEST, $rtest[COL_UNIQ])
->get(TBL_TSESSIONSHEET)
->row_array();

$repps = array();
if($rtest[COL_TESTREMARKS1]=='EPPS') {
  $repps = $this->db
  ->where(COL_IDSESSION, $rtest[COL_IDSESSION])
  ->where(COL_IDTEST, $rtest[COL_UNIQ])
  ->get(TBL_EPPS_SESSION)
  ->row_array();

  if(!empty($repps)) {
    $rskor = $this->db
    ->where(COL_EPPSSESSID, $repps[COL_UNIQ])
    ->get(TBL_EPPS_SESSIONDET)
    ->result_array();
  }
}
?>
<html>
<head>
  <title><?=$this->setting_web_name.' - '.$rtest[COL_TESTNAME]?></title>
  <style type="text/css">
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
    padding-top: 10px !important;
  }
  th, td {
    padding: 5px;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    /*border: 1px solid black !important;*/
  }

  td.is-invalid {
    color: red;
    font-weight: bold;
  }
  td.is-valid {
    color: green;
    font-weight: bold;
  }
  .table-head {
    border-bottom: 0.25px solid #000;
    margin-bottom: 15px !important;
  }
  td.cell-sm {
    width: 15px !important;
  }
  .font-weight-bold {
    font-weight: bold !important;
  }
  .text-right {
    text-align: right !important;
  }
  .v-align-top {
    vertical-align: top !important;
  }
  .nowrap {
    white-space: nowrap !important;
  }
  .w-100 {
    width: 100% !important;
  }
  .mt-5 {
    margin-top: .5rem !important;
  }
  .mt-b {
    margin-bottom: .5rem !important;
  }
  .text-green {
    color: green;
  }
  .text-red {
    color: red;
  }
  </style>
</head>
<body>
  <table class="table-head mt-5" width="100%">
    <tr>
      <td class="cell-sm nowrap">NAMA TEST</td>
      <td class="cell-sm nowrap">:</td>
      <td><strong><?=$rtest[COL_TESTNAME]?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap">PESERTA</td>
      <td class="cell-sm nowrap">:</td>
      <td><strong><?=$rsess[COL_FULLNAME]?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap">TANGGAL / WAKTU</td>
      <td class="cell-sm nowrap">:</td>
      <td><strong><?=date('Y-m-d', strtotime($rtest[COL_TESTSTART]))?></strong> / <strong><?=date('H:i', strtotime($rtest[COL_TESTSTART]))?></strong> s.d <strong><?=date('H:i', strtotime($rtest[COL_TESTEND]))?></strong></td>
    </tr>
    <?php
    if($rtest[COL_TESTREMARKS1] != 'EPPS') {
      ?>
      <tr>
        <td class="cell-sm nowrap">POIN</td>
        <td class="cell-sm nowrap">:</td>
        <td><strong><?=!empty($rskor)?number_format($rskor[COL_QUESTSCORE]):'-'?></strong></td>
      </tr>
      <!--<tr>
        <td style="width: 10px; white-space: nowrap">NILAI PSIKOLOGI</td>
        <td style="width: 10px; white-space: nowrap">:</td>
        <td><strong><?=isset($rtest[COL_TESTSCORE])?number_format($rtest[COL_TESTSCORE]):'-'?></strong></td>
      </tr>-->
      <?php
    } else {
      ?>
      <tr>
        <td class="cell-sm nowrap">KATEGORI</td>
        <td class="cell-sm nowrap">:</td>
        <td><strong><?=!empty($repps)?$repps[COL_EPPSKODE]:'-'?></strong></td>
      </tr>
      <?php
    }
    ?>
  </table>
  <div class="w-100">
    <?php
    $no=1;
    foreach($rsheet as $s) {
      $key='';
      $isTrue = false;
      $classCard = 'card-default';
      $classBg = 'bg-secondary';

      $optArr = json_decode($s[COL_QUESTOPTION]);
      $ptMax = 0;
      $ptMin = 0;
      foreach($optArr as $opt) {
        if($opt->Val>$ptMax) {
          $ptMax=$opt->Val;
          $key=$opt->Txt;
        }

        if($opt->Val<$ptMin) $ptMin=$opt->Val;
      }

      if ($rtest[COL_TESTREMARKS1]!='EPPS') {
        if($s[COL_QUESTSCORE]==$ptMax) {
          $isTrue = true;
          $classCard = 'card-success';
          $classBg = 'bg-success';
        }
        else if($s[COL_QUESTSCORE]==$ptMin) {
          $classCard = 'card-danger';
          $classBg = 'bg-danger';
        }
      }
      ?>
      <table style="margin-bottom: 10px">
        <tr>
          <td class="cell-sm font-weight-bold text-right v-align-top"><?=($no)?>.</td>
          <td>
            <?php
            if($rtest[COL_TESTTYPE] != 'ACR') {
              if(!empty($s[COL_QUESTIMAGE])) {
                $arrImg = explode(",", $s[COL_QUESTIMAGE]);
                if(count($arrImg)>0) {
                  foreach($arrImg as $img) {
                    ?>
                    <img class="p-2" src="<?=MY_UPLOADURL.$img?>" style="max-width: 50%" />
                    <?php
                  }
                }
              }
              ?>
              <?=$s[COL_QUESTTEXT]?>
              <?php
              if($s[COL_QUESTTYPE]=='MUL') {
                ?>
                <div class="mt-5">
                  <table class="w-100">
                    <?php
                    foreach($optArr as $opt) {
                      $classOpt = 'inherit';

                      if ($rtest[COL_TESTREMARKS1]!='EPPS') {
                        if($opt->Val == $ptMax) $classOpt = 'green';
                        else if(!$isTrue && $opt->Opt == $s[COL_QUESTRESPONSE]) {
                          $classOpt = 'red';
                        }
                      } else {
                        if($opt->Opt == $s[COL_QUESTRESPONSE]) {
                          $classOpt = 'green';
                        }
                      }

                      ?>
                      <tr>
                        <td class="cell-sm text-right v-align-top">
                          <span <?=toNum($opt->Val)==$ptMax?'class="font-weight-bold text-green"':($opt->Opt==$s[COL_QUESTRESPONSE]?'class="text-red"':'')?>>
                            <?=$opt->Opt.'.'?>
                          </span>
                        </td>
                        <td>
                          <span <?=toNum($opt->Val)==$ptMax?'class="font-weight-bold text-green"':($opt->Opt==$s[COL_QUESTRESPONSE]?'class="text-red"':'')?>>
                            <?=$opt->Txt?>
                          </span>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </table>
                </div>
                <?php
              } else if ($s[COL_QUESTTYPE]=='TEXT') {
                ?>
                <div class="mt-5">
                  <table class="w-100">
                    <tr>
                      <td class="cell-sm font-weight-bold text-right v-align-top">Jawaban</td>
                      <td class="cell-sm font-weight-bold text-right v-align-top">:</td>
                      <td class="<?=$isTrue?'is-valid':'is-invalid'?>">
                        <?=$s[COL_QUESTRESPONSE]?>
                      </td>
                    </tr>
                    <tr>
                      <td class="cell-sm font-weight-bold text-right v-align-top">Kunci</td>
                      <td class="cell-sm font-weight-bold text-right v-align-top">:</td>
                      <td>
                        <?=$key?>
                      </td>
                    </tr>
                  </table>
                </div>
                <?php
              }
              ?>
            <?php
            } else {

            }
            ?>
          </td>
        </tr>
      </table>
      <?php
      $no++;
    }
    ?>
  </div>
</body>
