<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=".$this->setting_web_name." - Rekapitulasi Hasil ".$pkg[COL_PKGNAME].".xls");
$rgroup = $this->db
->select('QuestGroup')
->join(TBL_TSESSIONTEST,TBL_TSESSIONTEST.'.'.COL_UNIQ." = ".TBL_TSESSIONSHEET.".".COL_IDTEST,"left")
->join(TBL_TSESSION,TBL_TSESSION.'.'.COL_UNIQ." = ".TBL_TSESSIONTEST.".".COL_IDSESSION,"left")
->where(TBL_TSESSION.'.'.COL_IDPACKAGE, $id)
->group_by(COL_QUESTGROUP)
->get(TBL_TSESSIONSHEET)
->result_array();
?>
<style>
td, th {
  background: transparent;
  border: 0.5px solid #000;
}
.text-center {
  text-align: center;
}
.text-right {
  text-align: right;
}
</style>
<table style="font-size: 11pt" width="100%">
  <thead>
    <tr>
      <th>ID</th>
      <th>NAMA PESERTA</th>
      <!--<th>MULAI</th>
      <th>SELESAI</th>-->
      <?php
      foreach($rgroup as $grp) {
        ?>
        <th><?=$grp['QuestGroup']?></th>
        <?php
      }
      ?>
      <th>SKOR AKHIR</th>
    </tr>
  </thead>
  <?php
  foreach($rec as $r) {
    $rgroup = $this->db
    ->select('QuestGroup, sum(QuestScore) as QuestScore')
    ->join(TBL_TSESSIONTEST,TBL_TSESSIONTEST.'.'.COL_UNIQ." = ".TBL_TSESSIONSHEET.".".COL_IDTEST,"left")
    ->join(TBL_TSESSION,TBL_TSESSION.'.'.COL_UNIQ." = ".TBL_TSESSIONTEST.".".COL_IDSESSION,"left")
    ->where(TBL_TSESSION.'.'.COL_UNIQ, $r[COL_UNIQ])
    ->group_by(COL_QUESTGROUP)
    ->get(TBL_TSESSIONSHEET)
    ->result_array();
    ?>
    <tr>
      <td style="vertical-align: middle" class="text-center"><?=strtoupper($r[COL_USERNAME])?></td>
      <td style="vertical-align: middle"><?=strtoupper($r[COL_FULLNAME])?></td>
      <!--<td style="vertical-align: middle; width: 100px; white-space: nowrap" class="text-right">'<?=date('H:i:s', strtotime($r[COL_SESSTIMESTART]))?></td>
      <td style="vertical-align: middle; width: 100px; white-space: nowrap" class="text-right">'<?=date('H:i:s', strtotime($r[COL_SESSTIMEEND]))?></td>-->
      <?php
      if(!empty($rgroup)) {
        foreach($rgroup as $g) {
          ?>
          <td style="vertical-align: middle; white-space: nowrap" class="text-center"><?=number_format($g[COL_QUESTSCORE])?></td>
          <?php
        }
      } else {
        ?>
        <td><?=$this->db->last_query()?></td>
        <?php
      }
      ?>
      <td style="vertical-align: middle; white-space: nowrap" class="text-center"><strong><?=number_format($r['SessScore'])?></strong></td>

    </tr>
    <?php
  }
  ?>
  <tbody>
  </tbody>
</table>
