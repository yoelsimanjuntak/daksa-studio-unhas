<?php
$isIncludeACR = false;
$isIncludeEPPS = false;
$arrItems = json_decode($rpkg[COL_PKGITEMS]);
foreach($arrItems as $i) {
  $rtest = $this->db
  ->where(COL_UNIQ, $i->TestID)
  ->get(TBL_MTEST)
  ->row_array();

  if(!empty($rtest) && $rtest[COL_TESTTYPE]=='ACR') {
    $isIncludeACR = true;
  }
  if(!empty($rtest) && $rtest[COL_TESTREMARKS1]=='EPPS') {
    $isIncludeEPPS = true;
  }
}
?>
<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
#datalist tbody th, #datalist tbody td {
  vertical-align: middle;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?> <small class="text-sm"><?=$rpkg[COL_PKGNAME]?></small></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <a href="<?=site_url('site/session/add')?>" type="button" class="btn btn-tool btn-add-data text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH</a>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <!--<th>PAKET</th>-->
                    <th>ID</th>
                    <th>NAMA PESERTA</th>
                    <th>MULAI</th>
                    <th>SISA WAKTU</th>
                    <th>KETERANGAN</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-add" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">TAMBAH PESERTA</span>
      </div>
      <form id="form-session" action="<?=site_url('site/sess/add-to-package/'.$rpkg[COL_UNIQ])?>" method="post">
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label">PENGGUNA / AKUN</label>
            <label class="control-label float-right"><input type="checkbox" name="IsAllChecked" value="0" />&nbsp;&nbsp;SEMUA</label>
            <select class="form-control" name="<?=COL_USERNAME?>[]" style="width: 100%" multiple required>
              <?=GetCombobox("select * from users where RoleID=".ROLEUSER." order by Fullname", COL_USERNAME, array(COL_FULLNAME, COL_EMAIL)/*, null, true, false, 'SEMUA AKUN'*/)?>
            </select>
          </div>
          <?php
          if($isIncludeACR) {
            ?>
            <div class="form-group">
              <label class="control-label">OPSI (TEST KECERMATAN)</label>
              <select class="form-control" name="<?=COL_SESSREMARK2?>" style="width: 100%" required>
                <option value="ANGKA">ANGKA</option>
                <option value="HURUF">HURUF</option>
                <option value="SIMBOL">SIMBOL</option>
                <option value="MIXED">ANGKA & HURUF</option>
              </select>
            </div>
            <?php
          }
          ?>
          <?php
          if($isIncludeEPPS) {
            ?>
            <div class="form-group">
              <label class="control-label">KODE / KATEGORI PESERTA</label>
              <select class="form-control" name="<?=COL_SESSREMARK2?>" style="width: 100%" required>
                <option value="1">PELAJAR (PRIA)</option>
                <option value="2">PELAJAR (WANITA)</option>
                <option value="3">UMUM (PRIA)</option>
                <option value="4">UMUM (WANITA)</option>
              </select>
            </div>
            <?php
          }
          ?>
          <div class="form-group">
            <label class="control-label"><input type="checkbox" name="IsRandom" value="1" checked />&nbsp;&nbsp;ACAK SOAL</label>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
          <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="dom-filter" class="d-none">
  <select class="form-control" name="filterStatus" style="width: 200px">
    <option value="">SEMUA</option>
    <option value="new">BELUM MULAI</option>
    <option value="active">SEDANG BERJALAN</option>
    <option value="complete">SELESAI</option>
  </select>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var orderdef = [];
  var coldefs = [];
  var cols = [];
  orderdef = [];
  coldefs = [
    {"targets":[0,5], "className":'nowrap text-center'},
    {"targets":[3,4], "className":'nowrap dt-body-right'}
  ];
  cols = [
    {"orderable": false,"width": "50px"},
    {"orderable": true},
    {"orderable": true},
    {"orderable": false},
    {"orderable": false},
    {"orderable": false,"width": "50px"}
  ];
  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/sess/index-by-package-load/'.$rpkg[COL_UNIQ])?>",
      "type": 'POST',
      "data": function(data){
        data.filterStatus = $('[name=filterStatus]', $('.filtering')).val();
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": orderdef,
    "columnDefs": coldefs,
    "columns": cols,
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('[data-toggle="tooltip"]', $(row)).tooltip();
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });
  $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add-data').click(function() {
    $('#modal-add').modal('show');
    return false;
  });

  $('#form-session').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            dt.DataTable().ajax.reload();
            $('#modal-add').modal('hide');
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  /*setInterval(function(){
    dt.DataTable().ajax.reload();
  }, 3000);*/

  $('[name=IsAllChecked]').change(function() {
    var opts = $('option', $('select[name^=Username]'));
    var arrval = [];

    if($(this).is(':checked')) {
      for(var i=0; i<opts.length; i++) {
        arrval.push($(opts[i]).attr('value'));
      }

    }

    $('[name^=Username]').val(arrval).trigger('change');
  });
});
</script>
