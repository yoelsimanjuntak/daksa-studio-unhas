<?php
$rpkg = $this->db
->where(COL_PKGISACTIVE, 1)
->order_by(COL_PKGNAME)
->get(TBL_MTESTPACKAGE)
->result_array();
?>
<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
#datalist tbody th, #datalist tbody td {
  vertical-align: middle;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <?php
          if(isset($pkg)) {
            ?>
            <li class="breadcrumb-item"><a href="<?=site_url('site/master/package')?>">Daftar Ujian</a></li>
            <?php
          }
          ?>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <?php
            if(isset($pkg)) {
              ?>
              <h3 class="card-title font-weight-bold"><?=$pkg[COL_PKGNAME]?></h3>
              <?php
            }
            ?>
            <div class="card-tools text-center">
              <?php
              if(isset($stat) && $stat=='new') {
                ?>
                <a href="<?=site_url('site/session/add/'.$stat)?>" type="button" class="btn btn-tool btn-add-data text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH</a>
                <?php
              }
              ?>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>PAKET</th>
                    <th>PESERTA</th>
                    <?php
                    if(isset($stat) && $stat=='new') {
                      ?>
                      <th>EMAIL</th>
                      <th>NO. TELP / HP</th>
                      <th>DIBUAT PADA</th>
                      <?php
                    } else if (isset($stat) && $stat=='active') {
                      ?>
                      <th>MULAI</th>
                      <th>TEST BERJALAN</th>
                      <th>SISA WAKTU</th>
                      <?php
                    }  else if (isset($stat) && $stat=='complete') {
                      ?>
                      <th>MULAI</th>
                      <th>SELESAI</th>
                      <th>POIN</th>
                      <?php
                    }
                    ?>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-add" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">TAMBAH SESI</span>
      </div>
      <form id="form-session" action="<?=site_url('site/sess/add')?>" method="post">
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label">PAKET</label>
            <select class="form-control" name="<?=COL_IDPACKAGE?>" style="width: 100%" required>
              <?php
              foreach($rpkg as $p) {
                $isIncludeACR = false;
                $isIncludeEPPS = false;
                $arrItems = json_decode($p[COL_PKGITEMS]);
                foreach($arrItems as $i) {
                  $rtest = $this->db
                  ->where(COL_UNIQ, $i->TestID)
                  ->get(TBL_MTEST)
                  ->row_array();

                  if(!empty($rtest) && $rtest[COL_TESTTYPE]=='ACR') {
                    $isIncludeACR = true;
                  }
                  if(!empty($rtest) && $rtest[COL_TESTREMARKS1]=='EPPS') {
                    $isIncludeEPPS = true;
                  }
                }
                echo '<option value="'.$p[COL_UNIQ].'" data-acr="'.($isIncludeACR?1:0).'" data-epps="'.($isIncludeEPPS?1:0).'">'.$p[COL_PKGNAME].'</option>';
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label class="control-label">PENGGUNA</label>
            <select class="form-control" name="<?=COL_USERNAME?>" style="width: 100%" required>
              <?=GetCombobox("select * from users where RoleID=".ROLEUSER." order by Fullname", COL_USERNAME, array(COL_FULLNAME, COL_EMAIL))?>
            </select>
          </div>
          <div class="form-group div-acropt">
            <label class="control-label">OPSI (TEST KECERMATAN)</label>
            <select class="form-control" name="<?=COL_SESSREMARK2?>" style="width: 100%" required>
              <option value="ANGKA">ANGKA</option>
              <option value="HURUF">HURUF</option>
              <option value="SIMBOL">SIMBOL</option>
              <option value="MIXED">ANGKA & HURUF</option>
            </select>
          </div>
          <div class="form-group div-eppskode">
            <label class="control-label">KODE / KATEGORI PESERTA</label>
            <select class="form-control" name="<?=COL_SESSREMARK2?>" style="width: 100%" required>
              <option value="1">PELAJAR (PRIA)</option>
              <option value="2">PELAJAR (WANITA)</option>
              <option value="3">UMUM (PRIA)</option>
              <option value="4">UMUM (WANITA)</option>
            </select>
          </div>
          <div class="form-group">
            <label class="control-label"><input type="checkbox" name="IsRandom" value="1" checked />&nbsp;&nbsp;ACAK SOAL</label>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
          <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="dom-filter" class="d-none">
  <?php
  if(!isset($pkg)) {
    ?>
    <select class="form-control" name="filterPackage" style="width: 200px">
      <?=GetCombobox("select * from mtestpackage order by PkgName", COL_UNIQ, COL_PKGNAME, null, true, false, '-- SEMUA PAKET --')?>
    </select>
    <?php
  } else {
    ?>
    <input type="hidden" name="filterPackage" value="<?=$pkg[COL_UNIQ]?>" />
    <?php
  }
  ?>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var orderdef = [];
  var coldefs = [];
  var cols = [];
  <?php
  if(isset($stat) && $stat=='new') {
    ?>
    orderdef = [[ 5, "desc" ]];
    coldefs = [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[5], "className":'nowrap dt-body-right'}
    ];
    cols = [
      {"orderable": false,"width": "50px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": false},
      {"orderable": true,"width": "10px"}
    ];
    <?php
  } else if (isset($stat) && $stat=='active') {
    ?>
    orderdef = [[ 3, "desc" ]];
    coldefs = [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[3, 5], "className":'nowrap dt-body-right'}
    ];
    cols = [
      {"orderable": false,"width": "50px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true,"width": "10px"}
    ];
    <?php
  } else if (isset($stat) && $stat=='complete') {
    ?>
    orderdef = [[ 4, "desc" ]];
    coldefs = [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[3,4,5], "className":'nowrap dt-body-right'}
    ];
    cols = [
      {"orderable": false,"width": "50px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": false,"width": "10px"}
    ];
    <?php
  }
  ?>
  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/sess/index-load/'.(isset($stat)?$stat:''))?>",
      "type": 'POST',
      "data": function(data){
        data.filterPackage = <?=isset($pkg)?$pkg[COL_UNIQ]:"$('[name=filterPackage]', $('.filtering')).val()"?>;
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": orderdef,
    "columnDefs": coldefs,
    "columns": cols,
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('[data-toggle="tooltip"]', $(row)).tooltip();
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });
  $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add-data').click(function() {
    $('#modal-add').modal('show');
    return false;
  });

  $('[name=IDPackage]').change(function(){
    var acr = $('option:selected', $(this)).data('acr');
    var epps = $('option:selected', $(this)).data('epps');
    if(acr==1) {
      $('[name=SessRemark2]', $('.div-acropt')).attr('disabled', false);
      $('.div-acropt').show();
    } else {
      $('[name=SessRemark2]', $('.div-acropt')).attr('disabled', true);
      $('.div-acropt').hide();
    }

    if(epps==1) {
      $('[name=SessRemark2]', $('.div-eppskode')).attr('disabled', false);
      $('.div-eppskode').show();
    } else {
      $('[name=SessRemark2]', $('.div-eppskode')).attr('disabled', true);
      $('.div-eppskode').hide();
    }
  }).trigger('change');

  $('#form-session').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            dt.DataTable().ajax.reload();
            $('#modal-add').modal('hide');
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  <?php
  if (isset($stat) && $stat=='active') {
    ?>
    setInterval(function(){
      dt.DataTable().ajax.reload();
    }, 3000);
    <?php
  }
  ?>
});
</script>
