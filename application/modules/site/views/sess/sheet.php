
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?=!empty($title) ? $title.' - '.$this->setting_web_name : $this->setting_web_name?></title>
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
  <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
  <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?=base_url()?>assets/js/moment.js"></script>
  <script src="<?=base_url()?>assets/js/countdown.js"></script>
  <script src="<?=base_url()?>assets/js/moment-countdown.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="icon" type="image/png" href=<?=base_url().$this->setting_web_icon?>>

  <style>
  .no-js #loader { display: none;  }
  .js #loader { display: block; position: absolute; left: 100px; top: 0; }
  .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url(<?=base_url().$this->setting_web_preloader?>) center no-repeat #fff;
  }

  @media (max-width: 767px) {
    .sidebar-toggle {
      font-size: 3vw !important;
    }
  }

  .form-group .control-label {
      text-align: right;
      line-height: 2;
  }
  .nowrap {
    white-space: nowrap;
  }
  .va-middle {
    vertical-align: middle !important;
  }
  .border-0 td {
    border: 0!important;
  }
  td.dt-body-right {
    text-align: right !important;
  }
  .sidebar-dark-orange .nav-sidebar>.nav-item>.nav-link.active, .sidebar-light-orange .nav-sidebar>.nav-item>.nav-link.active {
    color: #fff !important;
  }
  div.tooltip-inner {
    max-width: 350px !important;
    text-align: left !important;
  }
  .table-condensed td, .table-condensed th {
    padding: .5rem !important;
  }
  span.badge-opt {
    font-size: 16px !important;
    font-weight: bold !important;
    position: absolute !important;
    left: -10px !important;
    top: -7px !important;
  }

  .div-quest img {
    max-width: 100% !important;
  }
  </style>
  <script>
  // Wait for window load
  function startTime() {
    var today = new Date();
    $('#datetime').html(moment(today).format('DD')+' '+moment(today).format('MMM')+' '+moment(today).format('Y')+' <span class="text-muted font-weight-bold">'+moment(today).format('hh')+':'+moment(today).format('mm')+':'+moment(today).format('ss')+'</span>');
    var t = setTimeout(startTime, 1000);
  }
  $(document).ready(function() {
    startTime();
  });
  $(window).load(function() {
      $(".se-pre-con").fadeOut("slow");
  });
  </script>
</head>
<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_FULLNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'icon-user.jpg';

$rtest = $this->db
->where(COL_IDSESSION, $sess[COL_UNIQ])
->order_by(COL_TESTSEQ, 'asc')
->get(TBL_TSESSIONTEST)
->result_array();

$rquest = array();
$rtestCurrent = $this->db
->where('TestStart is not null')
->where('TestEnd is null')
->where(COL_IDSESSION, $sess[COL_UNIQ])
->order_by(COL_TESTSEQ, 'asc')
->get(TBL_TSESSIONTEST)
->row_array();
if(!empty($rtestCurrent)) {
  $rquest = $this->db
  ->where(COL_IDTEST, $rtestCurrent[COL_UNIQ])
  ->order_by(COL_UNIQ,'asc')
  ->get(TBL_TSESSIONSHEET)
  ->result_array();
}
?>
<body class="hold-transition layout-top-nav">
  <div class="se-pre-con"></div>
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-light navbar-white">
      <div class="container">
        <a href="<?=site_url()?>" class="navbar-brand">
          <img src="<?=base_url().$this->setting_web_logo?>" alt="Logo" class="brand-image img-circle">
          <span class="brand-text font-weight-light d-none d-sm-inline-block"><?=$this->setting_web_desc?></span>
        </a>
        <!--<ul class="navbar-nav">
          <li class="nav-item"></li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link">Home</a>
          </li>
        </ul>-->
        <ul class="navbar-nav ml-auto">
          <!--<li class="nav-item pr-2">
            <a class="btn btn-sm btn-success" href="<?=site_url('site/user/dashboard')?>"><i class="fas fa-home"></i><span class="d-none d-sm-inline-block">&nbsp;DASHBOARD</span></a>
          </li>-->
          <li class="nav-item pr-2">
            <button type="button" class="btn btn-sm btn-success" >SISA WAKTU <strong><span id="timer"></span></strong></a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="content-wrapper">
      <div class="content pt-3">
        <div class="container">
          <div class="row">
            <?php
            if(!empty($rtestCurrent)&&$rtestCurrent[COL_TESTTYPE]!='ACR'&&$rtestCurrent[COL_TESTTYPE]!='PAULI') {
              ?>
              <div class="col-lg-4 col-12">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title m-0 font-weight-bold"><?=$sess[COL_PKGNAME]?></h5>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                  </div>
                  <div class="card-body p-0">
                    <table class="table table-striped table-condensed" style="max-width: 100%; border-bottom: 1px solid #dedede">
                      <tbody class="text-sm">
                        <tr>
                          <td style="width: 10px; white-space: nowrap">NAMA</td>
                          <td style="width: 10px; white-space: nowrap">:</td>
                          <td><strong><?=$sess[COL_FULLNAME]?></strong></td>
                        </tr>
                        <tr>
                          <td style="width: 10px; white-space: nowrap">EMAIL</td>
                          <td style="width: 10px; white-space: nowrap">:</td>
                          <td><strong><?=$sess[COL_EMAIL]?></strong></td>
                        </tr>
                        <?php
                        if(!empty($rtestCurrent) && $rtestCurrent[COL_TESTTYPE] != 'ACR' && $rtestCurrent[COL_TESTTYPE] != 'PAULI') {
                          ?>
                          <!--<tr>
                            <td style="width: 10px; white-space: nowrap">SISA WAKTU</td>
                            <td style="width: 10px; white-space: nowrap">:</td>
                            <td><strong><span id="timer"></span></strong></td>
                          </tr>-->
                          <?php
                        }
                        ?>
                      </tbody>
                    </table>
                    <div class="row p-4">
                      <?php
                      $nq=1;
                      foreach($rquest as $q) {
                        ?>
                        <div class="col-lg-2 col-12 mb-2">
                          <button type="button" data-target="<?=$q[COL_UNIQ]?>" class="btn btn-sm btn-block btn-nav-quest btn-secondary"><?=$nq?></button>
                        </div>
                        <?php
                        $nq++;
                      }
                      ?>
                    </div>
                  </div>
                </div>
              </div>
              <?php
            }
            ?>

            <?php
            if(empty($rtestCurrent)) {
              ?>
              <div class="col-lg-8 col-12">
                <div class="card card-outline card-success">
                  <div class="card-header">
                    <h5 class="card-title m-0 font-weight-bold"><?=$sess[COL_PKGNAME]?></h5>
                  </div>
                  <div class="card-body p-0">
                    <table class="table table-hover">
                      <?php
                      $startTest = true;
                      foreach($rtest as $t) {
                        $prompt = $t[COL_TESTINSTRUCTION];
                        ?>
                        <tr>
                          <td style="vertical-align: middle"><?=strtoupper($t[COL_TESTNAME])?></td>
                          <td style="vertical-align: middle; width: 10px; white-space: nowrap" class="text-right"><strong><?=number_format($t[COL_TESTQUESTNUM]*(!empty($t[COL_TESTQUESTSUB])?$t[COL_TESTQUESTSUB]:1))?></strong> soal</td>
                          <td style="vertical-align: middle; width: 5px; white-space: nowrap" class="text-right">/</td>
                          <td style="vertical-align: middle; width: 10px; white-space: nowrap" class="text-right"><strong><?=number_format($t[COL_TESTDURATION]*(!empty($t[COL_TESTQUESTSUB])?$t[COL_TESTQUESTNUM]:1))?></strong> menit</td>
                          <td class="text-right">
                            <?php
                            if(empty($t[COL_TESTEND]) && $startTest) echo '<button type="button" class="btn btn-xs btn-outline-success btn-starttest" data-url="'.site_url('site/sess/start-test/'.$t[COL_UNIQ]).'" data-prompt="'.$prompt.'" data-name="'.$t[COL_TESTNAME].'" data-type="'.$t[COL_TESTTYPE].'" data-remarks1="'.$t[COL_TESTREMARKS1].'" data-remarks1url="'.site_url('site/sess/pre-test/'.$t[COL_UNIQ]).'">MULAI <i class="far fa-arrow-circle-right"></i></button>';
                            else if(!empty($t[COL_TESTEND])) echo '<i class="fas fa-check-circle text-primary"></i>';
                            ?>
                          </td>
                        </tr>
                        <?php
                        if(empty($t[COL_TESTEND]) && $startTest) $startTest = false;
                      }
                      ?>
                    </table>
                  </div>
                </div>
              </div>
              <?php
            } else {
              ?>
              <div class="col-lg-8 col-12">
                <div class="card card-outline card-success">
                  <div class="card-header">
                    <h5 class="card-title m-0 float-none text-center font-weight-bold">
                      <?=strtoupper($rtestCurrent[COL_TESTNAME])?>
                    </h5>
                  </div>
                  <div class="card-body">
                    <form id="form-sheet" action="<?=site_url('site/sess/submit/'.$rtestCurrent[COL_UNIQ])?>">
                      <input type="hidden" name="finish" value="0" />
                      <?php
                      $uniqlast = 0;
                      $txt_ = '';
                      $txtGrp_ = 0;
                      for($nq=0; $nq<count($rquest); $nq++) {
                        $q = $rquest[$nq];
                        $opts = json_decode($q[COL_QUESTOPTION]);

                        if($rtestCurrent[COL_TESTTYPE]=='ACR'||$rtestCurrent[COL_TESTTYPE]=='PAULI') {
                          if($txt_ != $q[COL_QUESTTEXT]) {
                            $txtGrp_++;
                            $txt_ = $q[COL_QUESTTEXT];
                          }
                        }
                        ?>
                        <div class="div-quest <?=$nq>0?'d-none':''?>" data-uniq="<?=$q[COL_UNIQ]?>" style="max-width: 100%" <?=$rtestCurrent[COL_TESTTYPE]=='ACR'||$rtestCurrent[COL_TESTTYPE]=='PAULI'?'data-acrno="'.$txtGrp_.'"':''?>>
                          <input type="hidden" name="QuestResponse__<?=$q[COL_UNIQ]?>" <?=!empty($q[COL_QUESTRESPONSE])&&$rtestCurrent[COL_TESTTYPE]!='ACR'&&$rtestCurrent[COL_TESTTYPE]!='PAULI'?'value="'.$q[COL_QUESTRESPONSE].'"':''?> />
                          <p class="mb-3 <?=$rtestCurrent[COL_TESTTYPE]!='ACR'&&$rtestCurrent[COL_TESTTYPE]!='PAULI'?'':'text-center'?>">
                            <?php
                            if($rtestCurrent[COL_TESTTYPE]!='PAULI') {
                              ?>
                              <span class="p-1 font-weight-bold" style="border: 1px solid #000"><?=$rtestCurrent[COL_TESTTYPE]!='ACR'?'PERTANYAAN NO. '.($nq+1):'KOLOM-'.$txtGrp_?></span>
                              <?php
                            }
                            ?>

                            <?php
                            if($rtestCurrent[COL_TESTTYPE] == 'ACR' || $rtestCurrent[COL_TESTTYPE] == 'PAULI') {
                              ?>
                              <span class="timer p-1 font-weight-bold float-right" style="border: 1px solid #000"></span>
                              <?php
                            }
                            ?>
                            <br />
                          </p>
                          <?php
                          if($rtestCurrent[COL_TESTTYPE] != 'ACR' && $rtestCurrent[COL_TESTTYPE] != 'PAULI') {
                            if(!empty($q[COL_QUESTIMAGE])) {
                              $arrImg = explode(",", $q[COL_QUESTIMAGE]);
                              if(count($arrImg)>0) {
                                foreach($arrImg as $img) {
                                  ?>
                                  <img class="p-2" src="<?=MY_UPLOADURL.$img?>" style="max-width: 50%" />
                                  <?php
                                }
                              }
                            }
                            ?>
                            <?=$q[COL_QUESTTEXT]?>
                            <br />
                            <div class="clearfix"></div>
                            <?php
                            if($q[COL_QUESTTYPE]=='MUL') {
                              ?>
                              <div class="row mt-4 pl-2 pr-2 div-opt d-flex align-items-stretch">
                                <?php
                                foreach($opts as $opt) {
                                  ?>
                                  <div class="<?=$rtestCurrent[COL_TESTTYPE]!='ACR'?'col-lg-6 col-12':'col-lg-2 col-2'?> mb-3">
                                    <button type="button" data-val="<?=$opt->Opt?>" class="btn btn-block btn-opt btn-outline-secondary <?=$rtestCurrent[COL_TESTTYPE]!='ACR'?'text-left':'text-center'?>" style="position: relative !important; padding: 10px 20px !important; height: 100%">
                                      <!--<span class="badge badge-opt bg-secondary"><?=$opt->Opt?></span>-->
                                      <strong><?=$opt->Opt?>.</strong> <?=$opt->Txt?>
                                    </button>
                                  </div>
                                  <?php
                                }
                                ?>
                              </div>
                              <?php
                            } else if($q[COL_QUESTTYPE]=='TEXT') {
                              ?>
                              <div class="form-group row">
                                <label class="control-label col-sm-2">Jawaban :</label>
                                <div class="col-sm-6">
                                  <input type="text" class="form-control txt-response"  />
                                </div>
                              </div>
                              <?php
                            }
                          } else if($rtestCurrent[COL_TESTTYPE] == 'ACR') {
                            ?>
                            <div class="row mt-4 pl-2 pr-2" style="justify-content: center;">
                              <?php
                              foreach($opts as $opt) {
                                ?>
                                <div class="mb-2 mr-2" style="min-width: 25px; padding: .5rem 2rem !important">
                                  <p class="text-center mb-0 font-weight-bold text-lg" style="font-size: 64pt !important;">
                                    <?php
                                    if($sess[COL_SESSREMARK2]=='SIMBOL') {
                                      ?>
                                      <img src="<?=MY_IMAGEURL.'symbols/'.$opt->Txt?>" style="width:25px; height: 25px" />
                                      <?php
                                    } else {
                                      echo $opt->Txt;
                                    }
                                    ?>
                                  </p>
                                  <p class="text-center mb-0 font-weight-bold text-lg">
                                    <?=$opt->Opt?>
                                  </p>
                                </div>
                                <?php
                              }
                              ?>
                            </div>

                            <p class="mt-4 font-weight-bold">Pertanyaan :</p>
                            <div class="row mt-1 pl-2 pr-2">
                              <?php
                              $arrTxtSub = explode(",",$q[COL_QUESTTEXTSUB]);
                              foreach($arrTxtSub as $txt) {
                                ?>
                                <div class="mb-3 mr-2" style="min-width: 25px">
                                  <button type="button" class="btn btn-lg btn-block btn-outline-secondary text-center text-lg" style="font-size: 36pt !important; padding: .5rem 2rem !important">
                                    <?php
                                    if($sess[COL_SESSREMARK2]=='SIMBOL') {
                                      ?>
                                      <img src="<?=MY_IMAGEURL.'symbols/'.$txt?>" style="width:25px; height: 25px" />
                                      <?php
                                    } else {
                                      echo $txt;
                                    }
                                    ?>
                                  </button>
                                </div>
                                <?php
                              }
                              ?>
                            </div>
                            <div class="row mt-3 pl-2 pr-2 div-opt">
                              <?php
                              foreach($opts as $opt) {
                                ?>
                                <div class="mb-3 mr-2 col-sm-2 p-0">
                                  <button type="button" data-val="<?=$opt->Opt?>" class="btn btn-opt btn-lg btn-block btn-outline-secondary text-center text-lg <?=$rtestCurrent[COL_TESTTYPE]!='ACR'?'text-left':'text-center'?>" style="font-size: 18pt !important;">
                                    <?=$opt->Opt?>
                                  </button>
                                </div>
                                <?php
                              }
                              ?>
                            </div>
                            <?php
                          } else if($rtestCurrent[COL_TESTTYPE] == 'PAULI') {
                            $opr = $rtestCurrent[COL_TESTSYMBOL];
                            $num = explode(",",$q[COL_QUESTTEXTSUB]);
                            $q_ = '';
                            if(!empty($num)) {
                              $q_ .= $num[0];
                            }
                            for($i=1;$i<count($num);$i++) {
                              $q_ .= ' '.$opr.' '.$num[$i];
                            }
                            $q_ .= ' = ';
                            ?>
                            <div class="row mt-4 p-2">
                              <div class="col-sm-12 p-4" style="text-align: center; border: 1px solid #000">
                                <p class="mb-0 font-weight-bold" style="font-size: 24pt">
                                  <?=$q_?><span class="label-pauli-ans">?</span>
                                </p>
                                <input type="hidden" class="form-control txt-response"  />
                              </div>
                            </div>
                            <div class="row mt-4">
                              <div class="col-4 mb-4" style="text-align: center;">
                                <button type="button" class="btn btn-block btn-outline-success btn-numpad" data-num="7" style="font-size: 24pt">7</button>
                              </div>
                              <div class="col-4 mb-4" style="text-align: center;">
                                <button type="button" class="btn btn-block btn-outline-success btn-numpad" data-num="8" style="font-size: 24pt">8</button>
                              </div>
                              <div class="col-4 mb-4" style="text-align: center;">
                                <button type="button" class="btn btn-block btn-outline-success btn-numpad" data-num="9" style="font-size: 24pt">9</button>
                              </div>
                              <div class="col-4 mb-4" style="text-align: center;">
                                <button type="button" class="btn btn-block btn-outline-success btn-numpad" data-num="4" style="font-size: 24pt">4</button>
                              </div>
                              <div class="col-4 mb-4" style="text-align: center;">
                                <button type="button" class="btn btn-block btn-outline-success btn-numpad" data-num="5" style="font-size: 24pt">5</button>
                              </div>
                              <div class="col-4 mb-4" style="text-align: center;">
                                <button type="button" class="btn btn-block btn-outline-success btn-numpad" data-num="6" style="font-size: 24pt">6</button>
                              </div>
                              <div class="col-4 mb-4" style="text-align: center;">
                                <button type="button" class="btn btn-block btn-outline-success btn-numpad" data-num="1" style="font-size: 24pt">1</button>
                              </div>
                              <div class="col-4 mb-4" style="text-align: center;">
                                <button type="button" class="btn btn-block btn-outline-success btn-numpad" data-num="2" style="font-size: 24pt">2</button>
                              </div>
                              <div class="col-4 mb-4" style="text-align: center;">
                                <button type="button" class="btn btn-block btn-outline-success btn-numpad" data-num="3" style="font-size: 24pt">3</button>
                              </div>
                              <div class="offset-4 col-4 mb-4" style="text-align: center;">
                                <button type="button" class="btn btn-block btn-outline-success btn-numpad" data-num="0" style="font-size: 24pt">0</button>
                              </div>
                              <div class="col-4 mb-4" style="text-align: center;">
                                <button type="button" class="btn btn-block btn-outline-success btn-numpad" data-num="-1" style="font-size: 24pt">
                                  <i class="far fa-arrow-right"></i>
                                </button>
                              </div>
                            </div>
                            <?php
                          }
                          ?>
                          <div class="row mt-3 <?=$rtestCurrent[COL_TESTTYPE]=='PAULI'?'d-none':''?>">
                            <div class="col-lg-6 col-12 text-right">
                              <button type="button" data-target="<?=$uniqlast?>" class="btn btn-block btn-nav btn-nav-prev btn-danger <?=$rtestCurrent[COL_TESTTYPE]=='ACR'?'d-none':''?>" <?=$nq>0?'':'disabled'?>>
                                <i class="far fa-arrow-circle-left"></i>&nbsp;&nbsp;SEBELUMNYA
                              </button>
                            </div>
                            <?php
                            if($nq<count($rquest)-1) {
                              ?>
                              <div class="col-lg-6 col-12 text-left">
                                <button type="button" data-target="<?=$rquest[$nq+1][COL_UNIQ]?>" class="btn btn-block btn-nav btn-nav-next btn-primary <?=$rtestCurrent[COL_TESTTYPE]=='ACR'?'d-none':''?>" <?=$nq<count($rquest)?'':'disabled'?>>
                                  SELANJUTNYA&nbsp;&nbsp;<i class="far fa-arrow-circle-right"></i>
                                </button>
                              </div>
                              <?php
                            } else {
                              ?>
                              <div class="col-lg-6 col-12 text-left">
                                <button type="submit" id="btn-submit" class="btn btn-block btn-success">
                                  SELESAI&nbsp;&nbsp;<i class="far fa-check-circle"></i>
                                </button>
                              </div>
                              <?php
                            }
                            ?>
                          </div>
                        </div>
                        <?php
                        $uniqlast = $q[COL_UNIQ];
                      }
                      ?>
                    </form>
                  </div>
                </div>
              </div>
              <?php
            }
            ?>

          </div>
        </div>
      </div>
    </div>
    <footer class="main-footer text-center">
      <strong>Copyright &copy; <?=$this->setting_org_name?>.</strong> Strongly developed by <strong>Partopi Tao</strong>.
    </footer>
  </div>
  <div class="modal fade" id="modal-starttest" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title font-weight-bold">MULAI TEST</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"><i class="fa fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body text-center"></div>
        <div class="modal-footer text-center">
          <div class="row">
            <div class="col-12 text-center">
              <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <a href="#" class="btn btn-sm btn-success btn-starttest">MULAI&nbsp;<i class="far fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal-pretest" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title font-weight-bold">HAPALAN&nbsp;<small>(SISA WAKTU <span id="timer-pre"></span>)</small></h6>
        </div>
        <div class="modal-body"></div>
      </div>
    </div>
  </div>
</body>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/fastclick/fastclick.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/sparklines/sparkline.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/icheck.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/js/select2.full.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/sweetalert.min.js"></script>
<script>
var noACR = 1;
var cd = null;
function responseChanged(doSubmit=true) {
  var respEl = $('input[name^=QuestResponse__]');
  for (i=0; i<respEl.length; i++) {
    var resp = $(respEl[i]).val();

    if(resp) {
      var questUniq = 0;
      var questEl = $(respEl[i]).closest('.div-quest');

      if(questEl) {
        questUniq = questEl.data('uniq');
        $('.btn-opt', questEl).addClass('btn-outline-secondary').removeClass('btn-outline-success');
        $('.btn-opt[data-val="'+resp+'"]', questEl).addClass('btn-outline-success').removeClass('btn-outline-secondary');

        $('.badge-opt', questEl).addClass('bg-secondary').removeClass('bg-success');
        $('.badge-opt', $('.btn-opt[data-val="'+resp+'"]', questEl)).addClass('bg-success').removeClass('bg-secondary');

        $('.btn-nav-quest[data-target="'+questUniq+'"]').addClass('btn-success').removeClass('btn-secondary');

        $('input.txt-response', questEl).addClass('is-valid');
        $('input.txt-response', questEl).val(resp);
      }
    }
  }

  if(doSubmit===true) {
    console.log('Submiting...');
    $('#form-sheet').ajaxSubmit({
      dataType: 'json',
      type : 'post',
      success: function(res) {
        // do nothing
      },
      error: function() {
        // do nothing
      },
      complete: function() {
        // do nothing
      }
    });
  }
}

function countdownACR(minutes, cbFinished) {
  //console.log('acrno', noACR);
  var elACR = $('div[data-acrno='+noACR+']:first');
  if(elACR.length>0) {
    var uniq = elACR.data('uniq');
    $('.btn-nav-quest[data-target='+uniq+']').click();

    noACR++;
    //var newdate = moment().add(minutes, 'minutes').toDate();
    var nowdate = new Date();
    var newdate = new Date();
    //newdate.setMinutes(newdate.getMinutes()+minutes);
    newdate.setSeconds(newdate.getSeconds()+(minutes*60));
    //console.log('added min', minutes);
    //console.log('nowdate', new Date());
    //console.log('newdate', newdate);
    cd = countdown(
      function(ts) {
        var dateStart = ts.start.getTime();
        var dateEnd = ts.end.getTime();
        //console.log('ts run', ts);
        //console.log('ts start', dateStart);
        //console.log('ts end', dateEnd);
        $('#timer').html('<strong>'+ts.hours.toString().padStart(2, '0')+'</strong> : <strong>'+ts.minutes.toString().padStart(2, '0')+'</strong> : <strong>'+ts.seconds.toString().padStart(2, '0')+'</strong>');
        $('.timer').html('<strong>'+ts.hours.toString().padStart(2, '0')+'</strong> : <strong>'+ts.minutes.toString().padStart(2, '0')+'</strong> : <strong>'+ts.seconds.toString().padStart(2, '0')+'</strong>');
        if(/*ts.hours==0 && ts.minutes==0 && ts.seconds==0*/ dateStart>=dateEnd) {
          window.clearInterval(cd);
          $('#timer').html('<strong class="text-danger">HABIS</strong>');
          $('.timer').html('<strong class="text-danger">HABIS</strong>');
          countdownACR(minutes, cbFinished);
        }
      },
      newdate,
      countdown.DEFAULTS
    );
    //console.log('cd init', cd);
  } else {
    //console.log('cb finished');
    cbFinished();
  }
}
$(document).ready(function(){
    $('.dropdown-menu textarea,.dropdown-menu input, .dropdown-menu label').click(function(e) {
        e.stopPropagation();
    });
    $(document).on('keypress','.angka',function(e){
      if((e.which <= 57 && e.which >= 48) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode==9 || e.which==43 || e.which==44 || e.which==45 || e.which==46 || e.keyCode==8){
          return true;
      }else{
          return false;
      }
    });
    $(document).on('blur','.uang',function(){
        $(this).val(desimal($(this).val(),0));
    }).on('focus','.uang',function(){
        $(this).val(toNum($(this).val()));
    });
    $(".uang").trigger("blur");
    $("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    $( ".alert-dismissible" ).fadeOut(3000, function() {
        // Animation complete.
    });
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    $(".money").number(true, 2, '.', ',');
    $(".uang").number(true, 0, '.', ',');

    $('#modal-starttest').on('hidden.bs.modal', function (e) {
      $('.modal-title',$('#modal-starttest')).html('MULAI TEST');
      $('.modal-body',$('#modal-starttest')).html('');
      $('.btn-starttest',$('#modal-starttest')).removeAttr('data-toggle').removeAttr('data-target').attr('href', '#').hide();
    });

    $('.btn-starttest').click(function(){
      var attrUrl = $(this).data('url');
      var attrPrompt = $(this).data('prompt');
      var attrName = $(this).data('name');
      var attrType = $(this).data('type');
      var attrRemarks1 = $(this).data('remarks1');
      var attrRemarks1URL = $(this).data('remarks1url');

      if(attrType=='IST' && attrRemarks1=='ME') {
        $('.modal-title',$('#modal-starttest')).html(attrName);
        $('.modal-body',$('#modal-starttest')).html(attrPrompt);
        $('.btn-starttest',$('#modal-starttest')).attr('data-toggle', "modal").attr('data-target', "#modal-pretest").attr('href', '#modal-pretest').show();

        $('.modal-body',$('#modal-pretest')).load(attrRemarks1URL, function(){

        });
        $('#modal-pretest').on('shown.bs.modal', function (e) {
          $('#modal-starttest').modal('hide');
          setTimeout(function(){
            location.href = attrUrl;
          }, 180000);
          cd = countdown(
            function(ts) {
              var dateStart = ts.start.getTime();
              var dateEnd = ts.end.getTime();

              $('#timer-pre').html('<strong>'+ts.minutes.toString().padStart(2, '0')+'</strong> : <strong>'+ts.seconds.toString().padStart(2, '0')+'</strong>');
              if(/*ts.hours==0 && ts.minutes==0 && ts.seconds==0*/ dateStart>=dateEnd) {
                window.clearInterval(cd);
                $('#timer-pre').html('<strong class="text-danger">SELESAI</strong>');
              }
            },
            moment().add(3, 'minutes').toDate(),
            countdown.HOURS|countdown.MINUTES|countdown.SECONDS
            );
        });

        $('#modal-starttest').modal('show');
      } else {
        $('.modal-title',$('#modal-starttest')).html(attrName);
        $('.modal-body',$('#modal-starttest')).html(attrPrompt);
        $('.btn-starttest',$('#modal-starttest')).attr('href', attrUrl).show();

        $('#modal-starttest').modal('show');
      }
    });

    <?php
    if(!empty($rtestCurrent)) {
      if($rtestCurrent[COL_TESTTYPE]=='ACR'||$rtestCurrent[COL_TESTTYPE]=='PAULI') {
        ?>
        countdownACR(<?=$rtestCurrent[COL_TESTDURATION]?>, function(){
          //alert('WAKTU TELAH HABIS.');
          $('#form-sheet').submit();
        });
        <?php
      } else {
        ?>
        cd = countdown(
          function(ts) {
            var dateStart = ts.start.getTime();
            var dateEnd = ts.end.getTime();

            $('#timer').html('<strong>'+ts.hours.toString().padStart(2, '0')+'</strong>:<strong>'+ts.minutes.toString().padStart(2, '0')+'</strong>:<strong>'+ts.seconds.toString().padStart(2, '0')+'</strong>');
            if(/*ts.hours==0 && ts.minutes==0 && ts.seconds==0*/ dateStart>=dateEnd) {
              window.clearInterval(cd);
              $('#timer').html('<strong class="text-danger">HABIS</strong>');

              alert('MAAF, WAKTU TELAH HABIS.');
              $('#form-sheet').submit();
            }
          },
          moment.unix('<?=strtotime($rtestCurrent[COL_TESTLIMIT])?>').toDate(),
          countdown.HOURS|countdown.MINUTES|countdown.SECONDS
        );
        <?php
      }
    } else {
      ?>
      $('#timer').html('<strong>HABIS</strong>');
      <?php
    }
    ?>

    $('input[name^=QuestResponse__]').change(function() {
      responseChanged();
    });

    responseChanged(false);

    $('.btn-nav, .btn-nav-quest').click(function() {
      var target = $(this).data('target');
      var targetEl = $('.div-quest[data-uniq='+target+']');
      if(targetEl) {
        $('.div-quest').addClass('d-none');
        targetEl.removeClass('d-none');
      }
    });

    $('.btn-opt').click(function() {
      var resp = $(this).data('val');
      var questUniq = 0;
      var questEl = $(this).closest('.div-quest');

      if (questEl) {
        questUniq = questEl.data('uniq');
        if (questUniq) {
          var acrno = questEl.data('acrno');
          $('input[name=QuestResponse__'+questUniq+']').val(resp).trigger('change');

          if(acrno) {
            var elACRLast = $('div[data-acrno='+acrno+']:last');
            if(elACRLast && elACRLast.data('uniq')==questUniq) {
              return false;
            }
          }
          $('.btn-nav-next', questEl).click();
        }
      }
    });

    $('input.txt-response').change(function(){
      var resp = $(this).val();
      var questUniq = 0;
      var questEl = $(this).closest('.div-quest');

      if (questEl) {
        questUniq = questEl.data('uniq');
        if (questUniq) {
          $('input[name=QuestResponse__'+questUniq+']').val(resp).trigger('change');
          $('.btn-nav-next', questEl).click();
        }
      }
    });

    $('button.btn-numpad').click(function() {
      var num = $(this).data('num');
      var questUniq = 0;
      var questEl = $(this).closest('.div-quest');
      var respEl = $('input.txt-response', questEl);

      if(questEl && respEl) {
        if(num>=0) {
          respEl.val(respEl.val()+num);
          if(respEl.val()) {
            $('.label-pauli-ans',questEl).html(respEl.val());
          } else {
            $('.label-pauli-ans',questEl).html('?');
          }
        }

        if(num<0 || respEl.val().length>=1) {
          setTimeout(function(){
            respEl.trigger('change');
          }, 200);
        }
      }
    });

    $(document).keyup(function(e){
      var numpadEl = $('button.btn-numpad[data-num='+e.key+']', $('.div-quest').not('.d-none'));
      if(numpadEl && numpadEl.length > 0) {
        numpadEl.click();
        numpadEl.addClass('active');
        setTimeout(function(){
          numpadEl.removeClass('active');
        }, 200);
      } else if(e.key=='Enter') {
        $('button.btn-numpad[data-num=-1]', $('.div-quest').not('.d-none')).click();
      }
      console.log(e.key);
    });

    $('#form-sheet').validate({
      submitHandler: function(form) {
        var emptyEl = $('input[name^=QuestResponse__]', form).filter(function() {
          return !this.value;
        });

        <?php
        if(!empty($rtestCurrent) && $rtestCurrent[COL_TESTTYPE]!='ACR') {
          ?>
          if (emptyEl.length > 0) {
            if(!confirm('Masih ada pertanyaan yang belum anda jawab. Apakah anda yakin ingin menyelesaikan test ini?')) {
              return false;
            }
          }
          <?php
        }
        ?>

        var btnSubmit = $('button[type=submit]', $('#form-sheet'));
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);

        $('input[name=finish]', form).val(1);
        $(form).ajaxSubmit({
          dataType: 'json',
          type : 'post',
          success: function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
              setTimeout(function(){
                location.reload();
              }, 1000);
            }
          },
          error: function() {
            toastr.error('SERVER ERROR');
          },
          complete: function() {
            btnSubmit.html(txtSubmit);
            btnSubmit.attr('disabled', false);
          }
        });
        return false;
      }
    });
});
</script>
</html>
