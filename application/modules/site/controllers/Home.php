<?php

class Home extends MY_Controller {

  public function index() {
    redirect('site/user/login');
    /*$this->load->model('mpost');

    $data['title'] = 'Beranda';
    $data['berita'] = $this->mpost->search(9,"",1);
    $this->load->view('site/home/index-new', $data);*/
  }

  public function page($slug) {
    $this->load->model('mpost');
    $data['title'] = 'Page';

    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where("(".TBL__POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL__POSTS.".".COL_POSTID." = '".$slug."')");
    $rpost = $this->db->get(TBL__POSTS)->row_array();
    if(!$rpost) {
        show_404();
        return false;
    }

    $this->db->where(COL_POSTID, $rpost[COL_POSTID]);
    $this->db->set(COL_TOTALVIEW, COL_TOTALVIEW."+1", FALSE);
    $this->db->update(TBL__POSTS);

    $data['title'] = $rpost[COL_POSTCATEGORYNAME];//strlen($rpost[COL_POSTTITLE]) > 50 ? substr($rpost[COL_POSTTITLE], 0, 50) . "..." : $rpost[COL_POSTTITLE];
    $data['data'] = $rpost;
    $data['berita'] = $this->mpost->search(5,"",1);
    $this->load->view('site/home/page', $data);
  }

  public function _404() {
    $data['title'] = 'Error';
    if(IsLogin()) {
      $this->template->load('adminlte' , 'home/_error', $data);
    } else {
      $this->template->load('adminlte' , 'home/_error', $data);
    }
  }

  public function testimoni() {
    $data['title'] = 'Testimoni';
    $this->load->view('site/home/testimoni', $data);
  }

  public function testimoni_add() {
    if(!empty($_POST)) {
      $this->db->trans_begin();
      try {
        $rec = array(
          COL_CONTENTTYPE=>'Testimonial',
          COL_CONTENTTITLE=>$this->input->post('TestimoniTitle'),
          COL_CONTENTDESC1=>$this->input->post('TestimoniName'),
          COL_CONTENTDESC2=>$this->input->post('TestimoniText'),
        );

        $res = $this->db->insert(TBL_WEBCONTENT, $rec);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('SELESAI');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    }
  }

  public function xendit_v1_inv_test() {
    // Response error : Array ( [error_code] => EMAIL_FORMAT_ERROR [message] => Customer email format is invalid )
    $options['secret_api_key'] = XENDIT_APIKEY;

    $xenditPHPClient = new XenditClient\XenditPHPClient($options);

    $external_id = 'chayra-001';
    $amount = 3000;
    $payer_email = 'rolassimanjuntak@gmail.com';
    $description = 'Testing';
    $opt = array(
      'locale' => 'id',
      'customer' => array(
        'given_names'=>'Rolas',
        'surname' => 'Doe',
        'email' => 'rolassimanjuntak@gmail.com',
        'mobile_number' => '+6287774441111'
      ),
      'items' => array(
        array('name'=>'Item 1', 'quantity'=>1, 'price'=>1000),
        array('name'=>'Item 2', 'quantity'=>1, 'price'=>2000)
      )
    );

    $response = $xenditPHPClient->createInvoice($external_id, $amount, $payer_email, $description, $opt);
    print_r($response);
  }

  public function xendit_v2_inv_test() {
     Xendit::setApiKey(XENDIT_APIKEY);
  }
}
 ?>
