<?php
class Master extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }
    if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
      redirect('site/user/dashboard');
    }
  }

  public function symbol() {
    $data['title'] = "Simbol";
    $this->template->load('adminlte', 'master/symbol', $data);
  }

  public function symbol_add() {
    $config['upload_path'] = MY_IMAGEPATH.'symbols/';
    $config['allowed_types'] = "png";
    $config['max_size']	= 512000;
    $config['max_width']  = 4000;
    $config['max_height']  = 4000;
    $config['overwrite'] = FALSE;
    $this->load->library('upload',$config);

    if($this->upload->do_upload('userfile')){
      ShowJsonSuccess('Berhasil');
      exit();
    } else {
      ShowJsonError(strip_tags($this->upload->display_errors()));
      exit();
    }
  }

  public function symbol_delete($file) {
    unlink(MY_IMAGEPATH.'/symbols/'.$file);
    redirect(site_url('site/master/symbol'));
  }

  public function kategori() {
    $data['title'] = "Kategori";
    $this->template->load('adminlte', 'master/kategori', $data);
  }

  public function kategori_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $questStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;
    //$dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    //$dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_KATEGORI,COL_CREATEDON,COL_CREATEDBY);
    $cols = array(COL_KATEGORI,COL_CREATEDON,COL_CREATEDBY);

    $queryAll = $this->db
    ->get(TBL_MKATEGORI);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->order_by(TBL_MKATEGORI.".".COL_KATEGORI, 'asc')
    ->get_compiled_select(TBL_MKATEGORI, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/master/kategori-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/master/kategori-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit" data-toggle="tooltip" data-placement="top" data-title="Perbarui" data-name="'.$r[COL_KATEGORI].'"><i class="fas fa-pencil-alt"></i></a>&nbsp;';

      $data[] = array(
        $htmlBtn,
        $r[COL_KATEGORI],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON])),
        $r[COL_CREATEDBY],
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function kategori_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_KATEGORI => $this->input->post(COL_KATEGORI),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MKATEGORI, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function kategori_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_KATEGORI => $this->input->post(COL_KATEGORI),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MKATEGORI, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function kategori_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_MKATEGORI);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function module() {
    $data['title'] = "Modul Belajar";
    $this->template->load('adminlte', 'master/module', $data);
  }

  public function module_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterKategori = !empty($_POST['filterKategori'])?$_POST['filterKategori']:null;
    $filterStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;
    //$dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    //$dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_KATEGORI,COL_MODTITLE,COL_MODDESC,null,COL_CREATEDON);
    $cols = array(COL_KATEGORI,COL_MODTITLE,COL_MODDESC);

    $queryAll = $this->db
    ->get(TBL_MMODUL);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($filterKategori)) {
      $this->db->where(COL_IDKATEGORI, $filterKategori);
    }
    if(!empty($filterStatus)) {
      if($filterStatus==1) {
        $this->db->where(COL_MODISAKTIF, 1);
      } else {
        $this->db->where(COL_MODISAKTIF, 0);
      }
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('mmodul.*, mkategori.Kategori')
    ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_MMODUL.".".COL_IDKATEGORI,"left")
    ->order_by(TBL_MMODUL.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_MMODUL, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/master/module-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/master/module-form/edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit" data-toggle="tooltip" data-placement="top" data-title="Perbarui"><i class="fas fa-pencil-alt"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.base_url().'assets/media/modules/'.$r[COL_MODFILENAME].'" class="btn btn-xs btn-outline-info" data-toggle="tooltip" data-placement="top" data-title="Download" target="_blank"><i class="fas fa-download"></i></a>&nbsp;';
      if($r[COL_MODISAKTIF]==1) {
        $htmlBtn .= '<a href="'.site_url('site/master/module-activation/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times-circle"></i></a>';
      } else {
        $htmlBtn .= '<a href="'.site_url('site/master/module-activation/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-success btn-action"><i class="fas fa-check"></i></a>';
      }

      $data[] = array(
        $htmlBtn,
        $r[COL_KATEGORI],
        $r[COL_MODTITLE],
        (strlen($r[COL_MODDESC]) > 100 ? substr($r[COL_MODDESC], 0, 100) . "..." : $r[COL_MODDESC]),
        ($r[COL_MODISAKTIF]==1?'<i class="far fa-check-circle text-success"></i>':'<i class="far fa-times-circle text-danger"></i>'),
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function module_form($mode, $id=null) {
    $ruser = GetLoggedUser();

    if(!empty($_POST)) {
      $config['upload_path'] = './assets/media/modules/';
      $config['allowed_types'] = "pdf";
      $config['max_size']	= 51200;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;
      $config['file_name'] = slugify(date('YmdHi').'-'.$this->input->post(COL_MODTITLE));

      $this->load->library('upload',$config);

      if($mode=='add') {
        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDKATEGORI=>$this->input->post(COL_IDKATEGORI),
            COL_MODTITLE=>$this->input->post(COL_MODTITLE),
            COL_MODDESC=>$this->input->post(COL_MODDESC),

            COL_CREATEDBY=>$ruser[COL_USERNAME],
            COL_CREATEDON=>date('Y-m-d H:i:s')
          );

          if(!empty($_FILES)) {
            $res = $this->upload->do_upload('file');
            if(!$res) {
              $err = $this->upload->display_errors('', '');
              throw new Exception($err);
            }
            $upl = $this->upload->data();
            $rec[COL_MODFILENAME] = $upl['file_name'];
          }

          $res = $this->db->insert(TBL_MMODUL, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('SELESAI');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }  else if($mode=='edit') {
        if(empty($id)) {
          ShowJsonError('Parameter tidak valid!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_IDKATEGORI=>$this->input->post(COL_IDKATEGORI),
            COL_MODTITLE=>$this->input->post(COL_MODTITLE),
            COL_MODDESC=>$this->input->post(COL_MODDESC)
          );

          $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MMODUL, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('SELESAI');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      $data=array();
      if($mode=='edit') {
        $data['data'] = $this->db->where(COL_UNIQ, $id)->get(TBL_MMODUL)->row_array();
      }
      $this->load->view('site/master/module-form', $data);
    }
  }

  public function module_delete($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MMODUL)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_MMODUL);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    if(file_exists('./assets/media/modules/'.$rdata[COL_MODFILENAME])) {
      unlink('./assets/media/modules/'.$rdata[COL_MODFILENAME]);
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function module_activation($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MMODUL)->row_array();
    if(!$rdata) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MMODUL, array(COL_MODISAKTIF=>($rdata[COL_MODISAKTIF]==1?0:1)));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('UBAH STATUS BERHASIL');
  }

  public function package($kat=null) {
    $data['title'] = "Daftar Ujian";

    /*if(!empty($kat)) {

    } else {
      $data['title'] = 'Daftar Kategori';
      $data['data'] = $this->db
      ->select('mtestpackage.*, mkategori.Kategori')
      ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_MTESTPACKAGE.".".COL_IDKATEGORI,"left")
      ->order_by('mkategori.Kategori', 'asc')
      ->group_by('mkategori.Kategori')
      ->get(TBL_MTESTPACKAGE)
      ->result_array();
      $this->template->load('adminlte', 'master/package-kategori', $data);
    }*/

    $filterStatus = "";
    if(!empty($_GET['filterStatus'])) $filterStatus = $_GET['filterStatus'];
    else $filterStatus=1;

    if ($filterStatus==1) $this->db->where(COL_PKGISACTIVE,1);
    else if ($filterStatus==-1) $this->db->where(COL_PKGISACTIVE,0);

    $data['data'] = $this->db
    ->select('mtestpackage.*, mkategori.Kategori')
    ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_MTESTPACKAGE.".".COL_IDKATEGORI,"left")
    //->order_by(COL_PKGISITEMEDITABLE, 'asc')
    ->order_by(COL_PKGDATE, 'desc')
    //->order_by(COL_PKGISACTIVE, 'desc')
    ->order_by('mkategori.Kategori', 'asc')
    ->order_by(COL_PKGNAME, 'asc')
    ->get(TBL_MTESTPACKAGE)
    ->result_array();

    $this->template->load('adminlte', 'master/package', $data);
  }

  public function package_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $arrItems = $this->input->post(COL_PKGITEMS);
      if(!empty($arrItems)) {
        $arrItems=json_decode($arrItems);
      }

      $isActive = $this->input->post(COL_PKGISACTIVE);
      $isActive = isset($isActive)&&$isActive==1?1:0;
      $dat = array(
        COL_IDKATEGORI=>$this->input->post(COL_IDKATEGORI),
        COL_PKGNAME=>$this->input->post(COL_PKGNAME),
        COL_PKGDESC=>$this->input->post(COL_PKGDESC),
        COL_PKGPRICE=>toNum($this->input->post(COL_PKGPRICE)),
        COL_PKGITEMS=>!empty($arrItems)?json_encode($arrItems):null,
        //COL_PKGISACTIVE=>$isActive,
        COL_PKGISACTIVE=>1,
        COL_PKGDATE=>!empty($this->input->post(COL_PKGDATE))?date('Y-m-d', strtotime($this->input->post(COL_PKGDATE))):null,
        COL_PKGAVAILABLEFROM=>$this->input->post(COL_PKGAVAILABLEFROM),
        COL_PKGAVAILABLETO=>$this->input->post(COL_PKGAVAILABLETO),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      $res = $this->db->insert(TBL_MTESTPACKAGE, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('SELESAI');
      exit();
    } else {
      $this->load->view('site/master/package-form');
    }
  }

  public function package_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MTESTPACKAGE)->row_array();
      if(empty($rdata)) {
        ShowJsonError('Parameter tidak valid!');
        exit();
      }

      $arrItems = $this->input->post(COL_PKGITEMS);
      if(!empty($arrItems)) {
        $arrItems=json_decode($arrItems);
      }
      $isActive = $this->input->post(COL_PKGISACTIVE);
      $isActive = isset($isActive)&&$isActive==1?1:0;
      $dat = array(
        COL_IDKATEGORI=>$this->input->post(COL_IDKATEGORI),
        COL_PKGNAME=>$this->input->post(COL_PKGNAME),
        COL_PKGDESC=>$this->input->post(COL_PKGDESC),
        COL_PKGPRICE=>toNum($this->input->post(COL_PKGPRICE)),
        //COL_PKGISACTIVE=>$isActive,
        COL_PKGDATE=>!empty($this->input->post(COL_PKGDATE))?date('Y-m-d', strtotime($this->input->post(COL_PKGDATE))):null,
        COL_PKGAVAILABLEFROM=>$this->input->post(COL_PKGAVAILABLEFROM),
        COL_PKGAVAILABLETO=>$this->input->post(COL_PKGAVAILABLETO),

        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );
      if($rdata[COL_PKGISITEMEDITABLE]==1) {
        $dat[COL_PKGITEMS] = !empty($arrItems)?json_encode($arrItems):null;
      }

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MTESTPACKAGE, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('SELESAI');
      exit();
    } else {
      $data['data'] = $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MTESTPACKAGE)->row_array();
      if(empty($rdata)) {
        ShowJsonError('Parameter tidak valid.');
        exit();
      }

      $this->load->view('site/master/package-form', $data);
    }
  }

  public function test() {
    $data['title'] = "Daftar Model Soal";
    $data['data'] = $this->db
    //->order_by(COL_CREATEDON, 'asc')
    ->get(TBL_MTEST)
    ->result_array();

    $this->template->load('adminlte', 'master/test', $data);
  }

  public function test_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $arrSym = $this->input->post(COL_TESTSYMBOL);
      $arrOpts = $this->input->post(COL_TESTOPTION);
      if(!empty($arrOpts)) {
        $arrOpts=json_decode($arrOpts);
      }

      if(toNum($this->input->post(COL_TESTQUESTNUM)) <= 0) {
        ShowJsonError('Jumlah soal tidak valid!');
        exit();
      }
      if(toNum($this->input->post(COL_TESTDURATION)) <= 0) {
        ShowJsonError('Durasi tidak valid!');
        exit();
      }

      $dat = array(
        COL_TESTTYPE=>$this->input->post(COL_TESTTYPE),
        COL_TESTNAME=>$this->input->post(COL_TESTNAME),
        COL_TESTQUESTNUM=>toNum($this->input->post(COL_TESTQUESTNUM)),
        COL_TESTDURATION=>toNum($this->input->post(COL_TESTDURATION)),
        COL_TESTOPTION=>!empty($arrOpts)?json_encode($arrOpts):null,
        COL_TESTINSTRUCTION=>$this->input->post(COL_TESTINSTRUCTION),
        COL_TESTDEFAULT=>$this->input->post(COL_TESTDEFAULT),
        COL_TESTSYMBOL=>!empty($arrSym)?implode(",",$arrSym):null,
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      $res = $this->db->insert(TBL_MTEST, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('SELESAI');
      exit();
    } else {
      $this->load->view('site/master/test-form');
    }
  }

  public function test_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $arrSym = $this->input->post(COL_TESTSYMBOL);
      $arrOpts = $this->input->post(COL_TESTOPTION);
      if(!empty($arrOpts)) {
        $arrOpts=json_decode($arrOpts);
      }

      if(toNum($this->input->post(COL_TESTQUESTNUM)) <= 0) {
        ShowJsonError('Jumlah soal tidak valid!');
        exit();
      }
      if(toNum($this->input->post(COL_TESTDURATION)) <= 0) {
        ShowJsonError('Durasi tidak valid!');
        exit();
      }

      $dat = array(
        COL_TESTTYPE=>$this->input->post(COL_TESTTYPE),
        COL_TESTNAME=>$this->input->post(COL_TESTNAME),
        COL_TESTQUESTNUM=>toNum($this->input->post(COL_TESTQUESTNUM)),
        COL_TESTDURATION=>toNum($this->input->post(COL_TESTDURATION)),
        COL_TESTOPTION=>!empty($arrOpts)?json_encode($arrOpts):null,
        COL_TESTINSTRUCTION=>$this->input->post(COL_TESTINSTRUCTION),
        COL_TESTDEFAULT=>$this->input->post(COL_TESTDEFAULT),
        COL_TESTSYMBOL=>!empty($arrSym)?implode(",",$arrSym):null,
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MTEST, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('SELESAI');
      exit();
    } else {
      $data['data'] = $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MTEST)->row_array();
      if(empty($rdata)) {
        ShowJsonError('Parameter tidak valid.');
        exit();
      }

      $this->load->view('site/master/test-form', $data);
    }
  }

  public function question($id) {
    $data['title'] = "Paket Soal";
    $rmodel = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_MTEST)
    ->row_array();

    if(empty($rmodel)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    $data['title'] = $rmodel[COL_TESTNAME];
    $data['idTest'] = $rmodel[COL_UNIQ];
    $this->template->load('adminlte', 'master/question', $data);
    //$this->load->view('site/master/question-test', $data);
  }

  public function question_load($id) {
    $rmodel = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_MTEST)
    ->row_array();

    if(empty($rmodel)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $questStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;
    $questStart = !empty($_POST['filterStart'])?$_POST['filterStart']:null;
    if(!empty($questStart)) {
      $start = $questStart;
    }
    //$dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    //$dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'asc');
    $orderables = array(null,COL_UNIQ,COL_QUESTTEXT,null,null,null,null,COL_CREATEDON);
    $cols = array(COL_QUESTTEXT,COL_QUESTGROUP);

    $queryAll = $this->db
    ->where(COL_IDTEST, $id)
    ->get(TBL_MTESTDETAIL);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    /*if(!empty($dateFrom)) {
      $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_DATEDISTRIBUTION.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_DATEDISTRIBUTION.' <= ', $dateTo);
    }*/

    if(!empty($questStatus)) {
      if($questStatus==1) {
        $this->db->where(COL_QUESTISACTIVE, 1);
      } else {
        $this->db->where(COL_QUESTISACTIVE, 0);
      }
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->where(COL_IDTEST, $id)
    ->order_by(TBL_MTESTDETAIL.".".COL_CREATEDON, 'asc')
    ->order_by(TBL_MTESTDETAIL.".".COL_UNIQ, 'asc')
    ->get_compiled_select(TBL_MTESTDETAIL, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    //echo $this->db->last_query();
    //exit();
    $data = [];

    $txt_ = '';
    $num = $start + 1;
    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/master/question-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/master/question-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit" data-toggle="tooltip" data-placement="top" data-title="Perbarui"><i class="fas fa-pencil-alt"></i></a>&nbsp;';
      if($r[COL_QUESTISACTIVE]==1) {
        $htmlBtn .= '<a href="'.site_url('site/master/question-activation/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Unpublish"><i class="fas fa-times-circle"></i></a>';
      } else {
        $htmlBtn .= '<a href="'.site_url('site/master/question-activation/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-success btn-action" data-toggle="tooltip" data-placement="top" data-title="Publish"><i class="fas fa-check"></i></a>';
      }
      $questStripped = htmlspecialchars_decode(strip_tags($r[COL_QUESTTEXT]));

      $optArr = array();
      $optHtml = '';
      $optMax = 0;
      if(!empty($r[COL_QUESTOPTION])) {
        $optArr = json_decode($r[COL_QUESTOPTION]);
        if(is_array($optArr)) {
          foreach($optArr as $opt) {
            if(toNum($opt->Val)>$optMax) $optMax = toNum($opt->Val);
          }
        }

      }

      foreach($optArr as $opt) {
        $optHtml .= '<btn type="button" class="btn btn-xs btn-'.(toNum($opt->Val)==$optMax?'info':'secondary').' m-1 pl-2 pr-2" data-toggle="tooltip" data-placement="top" title="'.$opt->Txt.'">'.$opt->Opt.'</btn>';
      }

      $htmlTooltip = '';
      if(strlen($questStripped) > 75) {
        $htmlTooltip = 'data-toggle="tooltip" data-placement="top" title="'.$questStripped.'"';
      }
      $data[] = array(
        '<input type="checkbox" class="checkbox-item" name="selection[]" value="'.$r[COL_UNIQ].'" />',
        $htmlBtn,
        '<input type="hidden" name="ROW__UNIQ" value="'.$r[COL_UNIQ].'" />&nbsp;'.$num,
        '<span '.$htmlTooltip.'>'.(strlen($questStripped) > 75 ? substr($questStripped, 0, 75) . "..." : $questStripped).'</span>',
        $optHtml,
        (!empty($r[COL_QUESTIMAGE])?'<a href="'.(site_url('site/master/question-img/'.$r[COL_UNIQ])).'" class="btn-popup-modal" data-target="#modal-media"><i class="far fa-info-circle text-primary"></i></a>':'<i class="far fa-times-circle text-danger"></i>'),
        $r[COL_QUESTGROUP],
        ($r[COL_QUESTISACTIVE]==1?'<i class="far fa-check-circle text-success"></i>':'<i class="far fa-times-circle text-danger"></i>'),
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
      $num++;
      $txt_ .= $questStripped.', ';
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    //echo $txt_;
    //exit();

    echo json_encode($result);
    exit();
  }

  public function question_img($id) {
    $rquest = $this->db->where(COL_UNIQ, $id)
    ->get(TBL_MTESTDETAIL)
    ->row_array();

    if(empty($rquest)) {
      echo '<strong class="text-danger">PARAMETER TIDAK VALID!</strong>';
      exit();
    }

    $respIndicators = '';
    $respInner = '';
    $arrImg = $rquest[COL_QUESTIMAGE];
    if(empty($arrImg) || empty(explode(',',$arrImg))) {
      exit();
    }

    $arrImg = explode(',',$arrImg);
    if(count($arrImg)>1) {
      $respIndicators = '';
      $respInner = '';
      for($i=0; $i<count($arrImg); $i++) {
        $respIndicators .= '<li data-target="#carouselExampleIndicators" data-slide-to="'.$i.'" class="'.($i==0?'active':'').'"></li>';
        $respInner .= '<div class="carousel-item '.($i==0?'active':'').'"><img class="d-block w-100" src="'.MY_UPLOADURL.$arrImg[$i].'"></div>';
      }
      echo @'
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          '.$respIndicators.'
        </ol>
        <div class="carousel-inner">
          '.$respInner.'
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      ';
      exit();
    } else {
      echo '<img src="'.MY_UPLOADURL.$arrImg[0].'" style="max-width: 100%" />';
      exit();
    }
  }

  public function question_group_load($id) {
    $html = "";
    $html .= '<option value="">-- KOSONG --</option>';

    if(!empty($_POST)) {
      $html .= '<option value="'.$_POST['Group'].'">'.$_POST['Group'].'</option>';
    }

    $rgroup = $this->db
    ->select(COL_QUESTGROUP)
    ->where(COL_IDTEST, $id)
    ->order_by(COL_QUESTGROUP)
    ->group_by(COL_QUESTGROUP)
    ->get(TBL_MTESTDETAIL)
    ->result_array();

    foreach($rgroup as $g) {
      if(!empty($g[COL_QUESTGROUP])) $html .= '<option value="'.$g[COL_QUESTGROUP].'">'.$g[COL_QUESTGROUP].'</option>';
    }

    echo $html;
    exit();
  }

  public function question_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $arrDat = array();
      $idTest = $this->input->post(COL_IDTEST);
      $arrText = $this->input->post(COL_QUESTTEXT);
      $arrImg = $this->input->post(COL_QUESTIMAGE);
      $arrType = $this->input->post(COL_QUESTTYPE);
      $arrGroup = $this->input->post(COL_QUESTGROUP);
      $arrKey = $this->input->post('QuestKey');
      $arrWeight = $this->input->post('QuestWeight');
      $arrOpt = $this->input->post('OPT');

      $n=0;
      foreach ($arrText as $r) {
        $arrImg_ = json_decode($arrImg[$n]);
        $arrMedia = array();
        $arrOpt_ = array();

        if($arrType[$n]=='MUL') {
          foreach($arrOpt[$n] as $opt) {
            $_text = $this->input->post('TXT__'.$opt.'['.$n.']');
            $_val = toNum($this->input->post('VAL__'.$opt.'['.$n.']'));

            if($_val != -1) {
              $arrOpt_[] = array(
                'Opt'=>$opt,
                'Txt'=>$_text,
                'Val'=>$_val
              );
            }
          }
        } else if ($arrType[$n]=='TEXT') {
          $arrOpt_[] = array(
            'Opt'=>$arrKey[$n],
            'Txt'=>$arrKey[$n],
            'Val'=>$arrWeight[$n]
          );
        }

        if(!empty($arrImg_)) {
          $i=1;
          foreach($arrImg_ as $img) {
            $fname = generateRandomString(15);
            $imgData = explode(',', $img->ImgPath);
            $imgData = base64_decode($imgData[1]);
            $imgFileName = $fname.'-'.$i.'.png';
            file_put_contents(MY_UPLOADPATH.$imgFileName, $imgData);

            $arrMedia[] = $imgFileName;
            $i++;
          }
        }

        $arrDat[] = array(
          COL_IDTEST=>$idTest,
          COL_QUESTTYPE=>$arrType[$n],
          COL_QUESTTEXT=>$arrText[$n],
          COL_QUESTGROUP=>$arrGroup[$n],
          COL_QUESTIMAGE=>implode(",", $arrMedia),
          COL_QUESTOPTION=>json_encode($arrOpt_),
          COL_CREATEDBY=>$ruser[COL_USERNAME],
          COL_CREATEDON=>date('Y-m-d H:i:s')
        );
        $n++;
      }

      if(!empty($arrDat)) {
        $res = $this->db->insert_batch(TBL_MTESTDETAIL, $arrDat);
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          exit();
        }
      }

      ShowJsonSuccess('BERHASIL', array('redirect'=>site_url('site/master/question/'.$idTest)));
      exit();
    } else {
      $idTest = $data['IDTest'] = $_GET['IDTest'];
      $numQuest = $data['num'] = $_GET['num'];
      $data['title'] = 'Tambah Soal';

      $rmodel = $data['rmodel'] = $this->db
      ->where(COL_UNIQ, $idTest)
      ->get(TBL_MTEST)
      ->row_array();

      if(empty($rmodel)) {
        show_error('PARAMETER TIDAK VALID!');
        exit();
      }

      $data['prevUrl']=site_url('site/master/question/'.$rmodel[COL_UNIQ]);
      $data['prevText']=$rmodel[COL_TESTNAME];

      $this->template->load('adminlte', 'master/question-form', $data);
    }
  }

  public function question_edit($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_MTESTDETAIL)
    ->row_array();

    if(empty($rdata)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    if(!empty($_POST)) {
      $type = $this->input->post(COL_QUESTTYPE);
      $group = $this->input->post(COL_QUESTGROUP);
      $arrOpt = $this->input->post('OPT');
      $arrImg = $this->input->post(COL_QUESTIMAGE);
      $arrOpt_ = array();
      $arrImg_ = json_decode($arrImg);
      $arrMedia = array();

      if($type=='MUL') {
        foreach($arrOpt as $opt) {
          $arrOpt_[] = array(
            'Opt'=>$opt,
            'Txt'=>$this->input->post('TXT__'.$opt),
            'Val'=>toNum($this->input->post('VAL__'.$opt))
          );
        }
      } else if($type=='TEXT') {
        $arrOpt_[] = array(
          'Opt'=>$this->input->post('QuestKey'),
          'Txt'=>$this->input->post('QuestKey'),
          'Val'=>toNum($this->input->post('QuestWeight'))
        );
      }

      if(!empty($arrImg_)) {
        $i=1;
        foreach($arrImg_ as $img) {
          $fname = generateRandomString(15);
          $imgData = explode(',', $img->ImgPath);
          $imgData = base64_decode($imgData[1]);
          $imgFileName = $fname.'-'.$i.'.png';
          file_put_contents(MY_UPLOADPATH.$imgFileName, $imgData);

          $arrMedia[] = $imgFileName;
          $i++;
        }
      }

      $dat = array(
        COL_QUESTTYPE=>$type,
        COL_QUESTGROUP=>$group,
        COL_QUESTTEXT=>$this->input->post(COL_QUESTTEXT),
        COL_QUESTIMAGE=>implode(",", $arrMedia),
        COL_QUESTOPTION=>json_encode($arrOpt_),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MTESTDETAIL, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('BERHASIL', array('redirect'=>site_url('site/master/question/'.$rdata[COL_IDTEST]).'?scrollTo='.$id));
      exit();
    } else {
      $idTest = $data['IDTest'] = $rdata[COL_IDTEST];
      $numQuest = $data['num'] = 1;
      $data['title'] = 'Ubah Soal';
      $data['data'] = $rdata;

      $rmodel = $data['rmodel'] = $this->db
      ->where(COL_UNIQ, $idTest)
      ->get(TBL_MTEST)
      ->row_array();

      if(empty($rmodel)) {
        show_error('PARAMETER TIDAK VALID!');
        exit();
      }

      $data['prevUrl']=site_url('site/master/question/'.$rmodel[COL_UNIQ]).'?scrollTo='.$id;
      $data['prevText']=$rmodel[COL_TESTNAME];

      $this->template->load('adminlte', 'master/question-form', $data);
    }
  }

  public function question_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_MTESTDETAIL);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function question_delete_bulk() {
    $selected = $this->input->post('selection');
    $this->db->trans_begin();
    try {
      $count=0;
      foreach ($selected as $datum) {
        $res = $this->db->where(COL_UNIQ, $datum)->delete(TBL_MTESTDETAIL);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
        $count++;
      }
      $this->db->trans_commit();
      ShowJsonSuccess($count.' data berhasil dihapus!');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
  }

  public function question_activation($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MTESTDETAIL)->row_array();
    if(!$rdata) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MTESTDETAIL, array(COL_QUESTISACTIVE=>($rdata[COL_QUESTISACTIVE]==1?0:1)));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('UBAH STATUS BERHASIL');
  }

  public function question_activation_bulk($opt) {
    $selected = $this->input->post('selection');
    $this->db->trans_begin();
    try {
      foreach ($selected as $datum) {
        $res = $this->db
        ->where(COL_UNIQ, $datum)
        ->update(TBL_MTESTDETAIL, array(
          COL_QUESTISACTIVE=>($opt==1?1:0),
          COL_UPDATEDBY=>$ruser[COL_USERNAME],
          COL_UPDATEDON=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      }
      $this->db->trans_commit();
      ShowJsonSuccess(($opt==1?'PUBLISH':'UNPUBLISH').' berhasil!');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
  }

  public function question_generate($id) {
    $ruser = GetLoggedUser();
    $t = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_MTEST)
    ->row_array();

    if(empty($t)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $num = $this->input->get('num');
    $mode = $this->input->get('mode');
    $stat = $this->input->get('stat');

    $arrQuest = array();
    if($t[COL_TESTTYPE]=='MUL' || $t[COL_TESTTYPE]=='IST') {
      if(!empty($stat)) {
        if($stat=='1') $this->db->where(COL_QUESTISACTIVE, 0);
        else if($stat=='2') $this->db->where(COL_QUESTISACTIVE, 1);
      }

      if($t[COL_TESTTYPE]=='MUL') {
        $rquest = $this->db
        ->where(COL_IDTEST, $id)
        //->where(COL_QUESTISACTIVE, 1)
        //->order_by('rand()')
        ->order_by((empty($this->input->get('IsRandom')) || $this->input->get('IsRandom') != 1?COL_UNIQ:'rand()'))
        ->get(TBL_MTESTDETAIL, $num)
        ->result_array();
      } else {
        $rquest = $this->db
        ->where(COL_IDTEST, $id)
        ->get(TBL_MTESTDETAIL, $num)
        ->result_array();
      }

      if(count($rquest)<$num) {
        ShowJsonError('Maaf soal tidak tersedia!');
        exit();
      }

      foreach($rquest as $q) {
        $arrQuest[] = $q[COL_UNIQ];
      }
    }

    if(!empty($arrQuest)) {
      $res = $this->db->insert(TBL_TPRINTS, array(
        COL_IDTEST=>$id,
        COL_IDTESTDETAIL=>implode(",", $arrQuest),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      ));
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      $id_ = $this->db->insert_id();
      ShowJsonSuccess('Berhasil!', array('URL1'=>site_url('site/master/question-print/'.$id.'/'.$id_).'?mode=nokey', 'URL2'=>site_url('site/master/question-print/'.$id.'/'.$id_).'?mode=withkey'));
      exit();
    } else {
      ShowJsonError('Gagal men-generate soal!');
      exit();
    }
  }

  public function question_print($idTest, $id) {
    $rtest = $this->db
    ->where(COL_UNIQ, $idTest)
    ->get(TBL_MTEST)
    ->row_array();

    $t = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TPRINTS)
    ->row_array();

    if(empty($t)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $quests = explode(",",$t[COL_IDTESTDETAIL]);
    $mode = $this->input->get('mode');
    $arrQuest = array();

    if(!empty($quests)) {
      foreach($quests as $q) {
        $rquest = $this->db
        ->where(COL_UNIQ, $q)
        ->get(TBL_MTESTDETAIL)
        ->row_array();

        if(!empty($rquest)) {
          $arrQuest[] = array(
            COL_IDTEST=>$rtest[COL_UNIQ],
            COL_QUESTTYPE=>$rquest[COL_QUESTTYPE],
            COL_QUESTTEXT=>$rquest[COL_QUESTTEXT],
            COL_QUESTTEXTSUB=>null,
            COL_QUESTIMAGE=>$rquest[COL_QUESTIMAGE],
            COL_QUESTOPTION=>$rquest[COL_QUESTOPTION]
          );
        }
      }
    }

    //echo "Sebanyak <strong>".count($arrQuest)."</strong> soal telah digenerate!";
    //exit();

    //$this->load->view('site/master/question-print', array('rtest'=>$t, 'rquest'=>$arrQuest));
    $this->load->library('Mypdf');
    $mpdf = new Mypdf('','A4',0,'',15,15,30,16);

    $html = $this->load->view('site/master/question-print', array('rtest'=>$rtest, 'rquest'=>$arrQuest, 'mode'=>$mode), TRUE);
    $htmlTitle = $this->setting_web_name;
    $htmlTitleSub = $this->setting_org_address.'<br />HP: +'.$this->setting_org_phone.' / +'.$this->setting_org_fax.'<br />website: www.chayrasmart.com / IG: @chayrasmartcourse';
    $htmlLogo = base_url().$this->setting_web_logo;
    $htmlHeader = @"
    <table style=\"border: none !important\">
      <tr>
        <td style=\"border: none !important; padding: 0 !important; width: 20%; white-space: nowrap; vertical-align: middle; text-align: right\"><img src=\"$htmlLogo\" style=\"width: 50px\" /></td>
        <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold; vertical-align: top; text-align: center\">$htmlTitle<br /><small style=\"font-weight: normal\">$htmlTitleSub</small></td>
        <td style=\"border: none !important; padding: 0 !important; width: 20%;\"></td>
      </tr>
    </table>
    <hr />
    ";

    $mpdf->pdf->SetTitle($htmlTitle);
    $mpdf->pdf->SetHTMLHeader($htmlHeader);
    $mpdf->pdf->SetWatermarkImage(base_url().$this->setting_web_logo);
    $mpdf->pdf->showWatermarkImage = true;
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->use_kwt = true;
    $mpdf->pdf->Output($this->setting_web_name.' - '.$htmlTitle.'.pdf', 'I');
  }

  public function package_formula($idpkg, $idtest) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $iFaktor = $this->input->post(COL_SCOREFORMULA);
      $iValFrom = $this->input->post('ValueFrom');
      $iValTo = $this->input->post('ValueTo');
      $iValue = $this->input->post('ValueFinal');

      $rcurrent = $this->db
      ->where(COL_IDPACKAGE, $idpkg)
      ->where(COL_IDTEST, $idtest)
      ->where(COL_SCOREREF, $iFaktor)
      ->get(TBL_MTESTFORMULA)
      ->row_array();

      if(!empty($rcurrent)) {
        $objFormulaCurr = json_decode($rcurrent[COL_SCOREFORMULA]);
        $objFormula = array();

        $isInserted = false;
        foreach($objFormulaCurr as $f) {
          if (toNum($iValFrom)>$f->ValueFrom && !$isInserted) {
            $objFormula[] = array('ValueFrom'=>toNum($iValFrom), 'ValueTo'=>toNum($iValTo), 'Point'=>toNum($iValue));
            $isInserted = true;
          }
          $objFormula[] = $f;
        }
        if(!$isInserted) {
          $objFormula[] = array('ValueFrom'=>toNum($iValFrom), 'ValueTo'=>toNum($iValTo), 'Point'=>toNum($iValue));
        }

        $res = $this->db
        ->where(array(COL_IDPACKAGE=>$idpkg, COL_IDTEST=>$idtest,COL_SCOREREF=>$iFaktor))
        ->update(TBL_MTESTFORMULA, array(COL_SCOREFORMULA=>json_encode($objFormula)));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          exit();
        }
      } else {
        $objFormula = array('ValueFrom'=>toNum($iValFrom), 'ValueTo'=>toNum($iValTo), 'Point'=>toNum($iValue));
        $res = $this->db->insert(TBL_MTESTFORMULA, array(
          COL_IDPACKAGE=>$idpkg,
          COL_IDTEST=>$idtest,
          COL_SCOREREF=>$iFaktor,
          COL_SCOREFORMULA=>json_encode(array($objFormula))
        ));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          exit();
        }
      }

      ShowJsonSuccess('Penilaian berhasil ditambahkan!');
      exit();
    } else {
      $data = $this->db
      ->where(COL_IDPACKAGE, $idpkg)
      ->where(COL_IDTEST, $idtest)
      ->order_by(COL_SCOREREF)
      ->get(TBL_MTESTFORMULA)
      ->result_array();

      $this->load->view('site/master/package-formula', array('data'=>$data));
    }
  }

  public function package_status($idpkg) {
    $ruser = GetLoggedUser();
    $rpackage = $this->db->where(COL_UNIQ, $idpkg)->get(TBL_MTESTPACKAGE)->row_array();
    if(empty($rpackage)) {
      echo 'Parameter tidak valid!';
      exit();
    }

    if(!empty($_POST)) {
      $isActive = $this->input->post(COL_PKGISACTIVE);
      $isActive = isset($isActive)&&$isActive==1?1:0;

      $dat = array(
        COL_PKGISACTIVE=>$isActive,
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );

      $res = $this->db->where(COL_UNIQ, $idpkg)->update(TBL_MTESTPACKAGE, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('Status berhasil diubah!');
      exit();
    } else {
      $data['rpackage'] = $rpackage;
      $this->load->view('site/master/package-status', $data);
    }
  }

  public function package_formula_delete($id, $idx) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MTESTFORMULA)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $arrFormula = json_decode($rdata[COL_SCOREFORMULA]);
    if(array_key_exists($idx, $arrFormula)) {
      array_splice($arrFormula, $idx, 1);
    }

    if(empty($arrFormula)) {
      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_MTESTFORMULA);
    } else {
      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MTESTFORMULA, array(COL_SCOREFORMULA=>json_encode($arrFormula)));
    }

    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Berhasil menghapus penilaian!');
  }
}
