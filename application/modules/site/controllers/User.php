<?php
//require_once APPPATH.'vendor/autoload.php';
class User extends MY_Controller {
  function __construct() {
      parent::__construct();
      //$this->load->library('encrypt');
      if(IsLogin() && GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        //redirect('site/home');
      }
  }

  function index() {
    if(!IsLogin()) {
      redirect('site/user/login');
    }
    $data['title'] = "Daftar Pengguna";
    $this->template->load('adminlte', 'user/index', $data);
  }

  public function index_load() {
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $userStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_USERNAME,COL_FULLNAME,COL_PHONE,null,COL_CREATEDON);
    $cols = array(COL_USERNAME, COL_FULLNAME, COL_PHONE);

    $queryAll = $this->db
    ->where(COL_ROLEID, ROLEUSER)
    ->get(TBL_USERS);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    /*if(!empty($dateFrom)) {
      $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_DATEDISTRIBUTION.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_DATEDISTRIBUTION.' <= ', $dateTo);
    }*/

    if(!empty($userStatus)) {
      if($userStatus==1) {
        $this->db->where(COL_ISSUSPEND, 0);
      } else {
        $this->db->where(COL_ISSUSPEND, 1);
      }
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->where(COL_ROLEID, ROLEUSER)
    ->get_compiled_select(TBL_USERS, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/user/edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit"><i class="fas fa-edit"></i>&nbsp;UBAH</a>&nbsp;';
      if($r[COL_ISSUSPEND]==0) {
        $htmlBtn .= '<a href="'.site_url('site/user/activation/'.$r[COL_UNIQ].'/0').'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times-circle"></i>&nbsp;SUSPEND</a>';
      } else {
        $htmlBtn .= '<a href="'.site_url('site/user/activation/'.$r[COL_UNIQ].'/1').'" class="btn btn-xs btn-outline-success btn-action"><i class="fas fa-check-circle"></i>&nbsp;AKTIFKAN</a>';
      }

      $data[] = array(
        $htmlBtn,
        $r[COL_USERNAME],
        $r[COL_FULLNAME],
        $r[COL_PHONE],
        ($r[COL_ISSUSPEND]==0?'<i class="far fa-check-circle text-success"></i>':'<i class="far fa-times-circle text-danger"></i>'),
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  function Login(){
    if(IsLogin()) {
      redirect('site/user/dashboard');
    }
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'Username',
          'label' => 'Username',
          'rules' => 'required'
        ),
        array(
          'field' => 'Password',
          'label' => 'Password',
          'rules' => 'required'
        )
      ));
      if($this->form_validation->run()) {
        $username = $this->input->post(COL_USERNAME);
        $password = $this->input->post(COL_PASSWORD);

        $ruser = $this->db
        ->where(COL_USERNAME,$username)
        ->where(COL_PASSWORD,md5($password))
        ->get(TBL_USERS)
        ->row_array();

        if(empty($ruser)) {
          ShowJsonError('Maaf, username / password yang anda masukkan tidak valid. Silakan coba kembali.');
          exit();
        }
        if($ruser[COL_ROLEID]!=ROLEADMIN) {
          if($ruser[COL_ISEMAILVERIFIED]!=1) {
            //ShowJsonError('Maaf, email anda belum terverifikasi. Silakan lakukan verifikasi melalui link yang dikirim ke email anda terlebih dahulu.');
            //exit();
          }

          if($ruser[COL_ISSUSPEND]==1) {
            ShowJsonError('Maaf, akun anda untuk sementara di SUSPEND. Silakan hubungi administrator.');
            exit();
          }
        }

        /*
        $this->db->where(COL_USERNAME, $username);
        $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));
        */

        SetLoginSession($ruser);
        ShowJsonSuccess('Selamat datang kembali, <strong>'.$ruser[COL_FULLNAME].'</strong>!', array('redirect'=>site_url('site/user/dashboard')));
        exit();

      } else {
        ShowJsonError('Maaf, username / password yang anda masukkan tidak valid. Silakan coba kembali.');
        exit();
      }
    } else {
      $this->load->view('site/user/login');
    }
  }
  function Logout(){
      UnsetLoginSession();
      redirect(site_url());
  }
  function Dashboard() {
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $data['title'] = 'Dashboard';
    $this->template->load('adminlte', 'site/user/dashboard', $data);
  }

  public function add() {
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError('ANDA TIDAK MEMILIKI HAK AKSES.');
        exit();
      }
      $rexist = $this->db
      ->where(COL_USERNAME, $this->input->post(COL_EMAIL))
      ->get(TBL_USERS)
      ->row_array();
      if(!empty($rexist)) {
        ShowJsonError('Maaf, alamat email <strong>'.$rexist[COL_EMAIL].'</strong> telah terdaftar.');
        exit();
      }

      $data = array(
        COL_ROLEID=>ROLEUSER,
        COL_FULLNAME=>$this->input->post(COL_FULLNAME),
        COL_USERNAME=>$this->input->post(COL_EMAIL),
        COL_EMAIL=>$this->input->post(COL_EMAIL),
        COL_PHONE=>$this->input->post(COL_PHONE),
        COL_PASSWORD=>md5($this->input->post(COL_PASSWORD)),
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $res = $this->db->insert(TBL_USERS, $data);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('BERHASIL');
      exit();
    } else {
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        show_error('ANDA TIDAK MEMILIKI HAK AKSES.');
        exit();
      }
      $this->load->view('site/user/form');
    }
  }

  public function edit($id) {
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $ruser = GetLoggedUser();

    $data['data'] = $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_USERS)
    ->row_array();

    if(empty($data)) {
      ShowJsonError('PARAMETER TIDAK VALID');
      exit();
    }

    if(!empty($_POST)) {
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError('ANDA TIDAK MEMILIKI HAK AKSES.');
        exit();
      }

      $data = array(
        COL_FULLNAME=>$this->input->post(COL_FULLNAME),
        COL_USERNAME=>$this->input->post(COL_EMAIL),
        COL_EMAIL=>$this->input->post(COL_EMAIL),
        COL_PHONE=>$this->input->post(COL_PHONE),
        COL_UPDATEDON=>date('Y-m-d H:i:s'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      );
      if(!empty($this->input->post(COL_PASSWORD))) {
        $data[COL_PASSWORD] = md5($this->input->post(COL_PASSWORD));
      }

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_USERS, $data);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('BERHASIL');
      exit();
    } else {
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        show_error('ANDA TIDAK MEMILIKI HAK AKSES.');
        exit();
      }

      $this->load->view('site/user/form', $data);
    }
  }

  public function activation($id, $stat=0) {
    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_USERS, array(COL_ISSUSPEND=>$stat==0?1:0));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL');
    exit();
  }

  public function changepassword() {
    $data['data'] = array();
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'NewPassword',
          'label' => 'NewPassword',
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[NewPassword]',
          'errors' => array('matches' => 'Kolom Konfirmasi Password wajib sama dengan Password Baru.')
        )
      ));

      if($ruser[COL_PASSWORD] != md5($this->input->post('OldPassword'))) {
        ShowJsonError('Password Lama tidak valid.');
        return false;
      }

      if($this->form_validation->run()) {
        $res = $this->db
        ->where(COL_USERNAME, $ruser[COL_USERNAME])
        ->update(TBL_USERS, array(COL_PASSWORD=>md5($this->input->post('NewPassword'))));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return false;
        }

        ShowJsonSuccess('Password berhasil diperbarui.', array('redirect'=>site_url('site/user/logout')));
        return false;
      } else {
        ShowJsonError(validation_errors());
        return false;
      }
    } else {
      $this->load->view('site/user/changepassword', $data);
    }
  }

  public function register() {
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'Password',
          'label' => 'Password',
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[Password]',
          'errors' => array('matches' => 'Kolom Konfirmasi Password wajib sama dengan Password Baru.')
        )
      ));

      $rexist = $this->db
      ->where(COL_USERNAME, $this->input->post(COL_EMAIL))
      ->get(TBL_USERS)
      ->row_array();
      if(!empty($rexist)) {
        ShowJsonError('Maaf, alamat email <strong>'.$rexist[COL_EMAIL].'</strong> telah terdaftar. Silakan daftar menggunakan email yang belum pernah terdaftar sebelumnya.');
        exit();
      }

      if(!$this->form_validation->run()) {
        $err = htmlspecialchars_decode(strip_tags(validation_errors()));
        ShowJsonError($err);
        return false;
      }

      $nmfile = null;
      if (!empty($_FILES['userfile']['name'])) {
        $config['upload_path'] = MY_UPLOADPATH;
        $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
        $config['max_size']	= 10240;
        $config['overwrite'] = FALSE;

        $this->load->library('upload',$config);
        $res = $this->upload->do_upload('userfile');
        if(!$res) {
          $err = $this->upload->display_errors('', '');
          ShowJsonError($err);
          exit();
        }
        $upl = $this->upload->data();
        $nmfile = $upl['file_name'];
      }

      $data = array(
        COL_ROLEID=>ROLEUSER,
        COL_FULLNAME=>$this->input->post(COL_FULLNAME),
        COL_USERNAME=>$this->input->post(COL_EMAIL),
        COL_EMAIL=>$this->input->post(COL_EMAIL),
        COL_PHONE=>$this->input->post(COL_PHONE),
        COL_PROFILEPIC=>$nmfile,
        COL_PASSWORD=>md5($this->input->post(COL_PASSWORD)),
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$this->input->post(COL_EMAIL)
      );

      $res = $this->db->insert(TBL_USERS, $data);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('Pendaftaran berhasil. Silakan login menggunakan akun yang sudah terdaftar.', array('redirect'=>site_url('site/user/login')));
      exit();
    } else {
      $this->load->view('site/user/register');
    }
  }

  public function profile() {
    if(!IsLogin()) {
      redirect('site/user/login');
    }
    $data['data'] = $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $nmfile = null;
      if (!empty($_FILES['userfile']['name'])) {
        $config['upload_path'] = MY_UPLOADPATH;
        $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
        $config['max_size']	= 10240;
        $config['overwrite'] = FALSE;

        $this->load->library('upload',$config);
        $res = $this->upload->do_upload('userfile');
        if(!$res) {
          $err = $this->upload->display_errors('', '');
          ShowJsonError($err);
          exit();
        }
        $upl = $this->upload->data();
        $nmfile = $upl['file_name'];
      }

      $data = array(
        COL_FULLNAME=>$this->input->post(COL_FULLNAME),
        COL_GENDER=>$this->input->post(COL_GENDER),
        COL_PHONE=>$this->input->post(COL_PHONE),
        COL_PROFILEPIC=>$nmfile,
        COL_DATEBIRTH=>date('Y-m-d', strtotime($this->input->post(COL_DATEBIRTH)))
      );
      $res = $this->db->where(COL_USERNAME, $ruser[COL_USERNAME])->update(TBL_USERS, $data);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      $ruser = $this->db
      ->where(COL_USERNAME,$ruser[COL_USERNAME])
      ->get(TBL_USERS)
      ->row_array();
      if(!empty($ruser)) {
        SetLoginSession($ruser);
      }

      ShowJsonSuccess('Pembaruan profil berhasil.');
      exit();
    } else {
      $this->load->view('site/user/profile', $data);
    }
  }

  public function daftar() {
    if(!empty($_POST)) {
      $nmfile = null;
      if (!empty($_FILES['userfile']['name'])) {
        $config['upload_path'] = MY_UPLOADPATH;
        $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
        $config['max_size']	= 10240;
        $config['overwrite'] = FALSE;

        $this->load->library('upload',$config);
        $res = $this->upload->do_upload('userfile');
        if(!$res) {
          $err = $this->upload->display_errors('', '');
          ShowJsonError($err);
          exit();
        }
        $upl = $this->upload->data();
        $nmfile = $upl['file_name'];
      }

      $rec = array(
        COL_FULLNAME=>$this->input->post(COL_FULLNAME),
        COL_BIRTH=>$this->input->post(COL_BIRTH),
        COL_NMSCHOOL=>$this->input->post(COL_NMSCHOOL),
        COL_NMPROGRAM=>$this->input->post(COL_NMPROGRAM),
        COL_PHONE=>$this->input->post(COL_PHONE),
        COL_NMSOCIALMEDIA=>$this->input->post(COL_NMSOCIALMEDIA),
        COL_NMFILE=>$nmfile,
        COL_TIMESTAMP=>date('Y-m-d H:i:s')
      );

      $res = $this->db->insert(TBL_TREGISTRATION, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err);
        exit();
      }

      ShowJsonSuccess('Hai, '.$rec[COL_FULLNAME].'! Pendaftaran kamu telah berhasil. Kamu akan dihubungi oleh administrator untuk petunjuk lebih lanjut. Terimakasih!');
      exit();
    } else {
      $this->load->view('site/user/daftar');
    }

  }

  public function registration() {
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $data['title'] = "Pendaftaran";
    $this->template->load('adminlte', 'user/registration', $data);
  }

  public function registration_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_FULLNAME,COL_BIRTH,COL_PHONE,COL_NMPROGRAM,COL_TIMESTAMP);
    $cols = array(COL_FULLNAME,COL_BIRTH,COL_PHONE,COL_NMPROGRAM);

    $queryAll = $this->db
    ->get(TBL_TREGISTRATION);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->order_by(TBL_TREGISTRATION.".".COL_TIMESTAMP, 'desc')
    ->get_compiled_select(TBL_TREGISTRATION, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('site/user/registration-detail/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-detail" data-toggle="tooltip" data-placement="top" data-title="Rincian"><i class="fas fa-eye"></i></a>'.'&nbsp;'.
        '<a href="'.site_url('site/user/registration-print/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-success" data-toggle="tooltip" data-placement="top" data-title="Cetak" target="_blank"><i class="fas fa-print"></i></a>'.'&nbsp;'.
        '<a href="'.site_url('site/user/registration-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-delete" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>',
        $r[COL_FULLNAME],
        $r[COL_BIRTH],
        $r[COL_PHONE],
        $r[COL_NMPROGRAM],
        date('Y-m-d H:i', strtotime($r[COL_TIMESTAMP]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function registration_delete($id) {
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TREGISTRATION)
    ->row_array();
    if(empty($rdata)) {
      ShowJsonError('Maaf, parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TREGISTRATION);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    if(!empty($rdata[COL_NMFILE])&&file_exists(MY_UPLOADPATH.$rdata[COL_NMFILE])) {
      unlink(MY_UPLOADPATH.$rdata[COL_NMFILE]);
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    exit();
  }

  public function registration_detail($id) {
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TREGISTRATION)
    ->row_array();
    if(empty($rdata)) {
      echo 'Maaf, parameter tidak valid!';
      exit();
    }

    $img = '';
    if(!empty($rdata[COL_NMFILE])&&file_exists(MY_UPLOADPATH.$rdata[COL_NMFILE])) {
      $img = '<p class="text-center mt-3"><img src="'.MY_UPLOADURL.$rdata[COL_NMFILE].'" style="max-height: 200px" /></p>';
    }
    $html = $img.@'
    <table class="table table-striped mb-0">
    <tbody>
    <tr>
    <td style="width: 100px; white-space: nowrap">Nama Lengkap</td><td style="width: 10px; white-space: nowrap">:</td><td class="font-weight-bold">'.$rdata[COL_FULLNAME].'</td>
    </tr><tr>
    <td style="width: 100px; white-space: nowrap">Tempat / Tgl. Lahir</td><td style="width: 10px; white-space: nowrap">:</td><td class="font-weight-bold">'.$rdata[COL_BIRTH].'</td>
    </tr>
    <tr>
    <td style="width: 100px; white-space: nowrap">Asal Sekolah</td><td style="width: 10px; white-space: nowrap">:</td><td class="font-weight-bold">'.$rdata[COL_NMSCHOOL].'</td>
    </tr>
    <tr>
    <td style="width: 100px; white-space: nowrap">Program</td><td style="width: 10px; white-space: nowrap">:</td><td class="font-weight-bold">'.$rdata[COL_NMPROGRAM].'</td>
    </tr>
    <tr>
    <td style="width: 100px; white-space: nowrap">No. HP</td><td style="width: 10px; white-space: nowrap">:</td><td class="font-weight-bold">'.$rdata[COL_PHONE].'</td>
    </tr>
    <tr>
    <td style="width: 100px; white-space: nowrap">Akun Instagram</td><td style="width: 10px; white-space: nowrap">:</td><td class="font-weight-bold">'.$rdata[COL_NMSOCIALMEDIA].'</td>
    </tr>
    </tbody>
    </table>
    ';

    echo $html;
  }

  public function registration_print($id) {
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TREGISTRATION)
    ->row_array();
    if(empty($rdata)) {
      show_error('Maaf, parameter tidak valid!');
      exit();
    }

    $this->load->library('Mypdf');
    $mpdf = new Mypdf('','A4',0,'',15,15,30,16);

    $html = $this->load->view('site/user/registration_print', array('rdata'=>$rdata), TRUE);
    $htmlTitle = $this->setting_web_name;
    $htmlTitleSub = $this->setting_org_address.'<br />HP: +'.$this->setting_org_phone.' / +'.$this->setting_org_fax.'<br />website: www.chayrasmart.com / IG: @chayrasmartcourse';
    $htmlLogo = base_url().$this->setting_web_logo;
    $htmlHeader = @"
    <table style=\"border: none !important\">
      <tr>
        <td style=\"border: none !important; padding: 0 !important; width: 20%; white-space: nowrap; vertical-align: middle; text-align: right\"><img src=\"$htmlLogo\" style=\"width: 50px\" /></td>
        <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold; vertical-align: top; text-align: center\">$htmlTitle<br /><small style=\"font-weight: normal\">$htmlTitleSub</small></td>
        <td style=\"border: none !important; padding: 0 !important; width: 20%;\"></td>
      </tr>
    </table>
    <hr />
    ";

    $mpdf->pdf->SetTitle($htmlTitle);
    $mpdf->pdf->SetHTMLHeader($htmlHeader);
    $mpdf->pdf->SetWatermarkImage(base_url().$this->setting_web_logo);
    $mpdf->pdf->showWatermarkImage = true;
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->use_kwt = true;
    $mpdf->pdf->Output($this->setting_web_name.' - '.$htmlTitle.'.pdf', 'I');
  }

  public function package() {
    if(!IsLogin()) {
      redirect('site/user/login');
    }
    $data['title'] = "Pilih Kategori";

    $q = @"
    select * from (
    	select kat.Uniq, IFNULL(kat.Kategori,'LAINNYA') as Kategori, IFNULL(kat.Kategori,'Z') as Ordering from mtestpackage pkg
    	left join mkategori kat on kat.Uniq = pkg.IdKategori
    	where pkg.PkgIsActive=1
    	group by kat.Kategori
    ) tbl order by Ordering asc
    ";
    $data['data'] = $this->db->query($q)->result_array();
    /*$data['data'] = $this->db
    ->select('mkategori.Uniq, IFNULL(mkategori.Kategori, \'LAINNYA\') as Kategori')
    ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_MTESTPACKAGE.".".COL_IDKATEGORI,"left")
    ->where(COL_PKGISACTIVE,1)
    ->order_by('mkategori.Kategori', 'asc')
    ->group_by('mkategori.Kategori')
    ->get(TBL_MTESTPACKAGE)
    ->result_array();*/

    $this->template->load('adminlte', 'user/package', $data);
  }
}
 ?>
